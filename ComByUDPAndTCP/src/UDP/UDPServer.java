package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UDPServer {
    private int serverport = 9090;

    private DatagramSocket socket = new DatagramSocket(serverport);


    public UDPServer() throws SocketException{
    }

    public void start() throws IOException {
        System.out.println("服务器启动！");

        while (true){
            DatagramPacket requestPacket = new DatagramPacket(new byte[4096], 4096);
            socket.receive(requestPacket);
            String request = new String(requestPacket.getData(), 0, requestPacket.getLength());
            String response = process(request);
            DatagramPacket responsePacket = new DatagramPacket(response.getBytes(),response.getBytes().length, requestPacket.getSocketAddress());
            socket.send(responsePacket);
            System.out.printf("[%s:%d req: %s, resp = %s\n", responsePacket.getAddress().toString(),
                    requestPacket.getPort(), request, response);
        }


    }

    public String process(String request){
        return request;
    }

    public static void main(String[] args) throws IOException {
        UDPServer server = new UDPServer();
        server.start();
    }
}
