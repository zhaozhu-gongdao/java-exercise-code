package com.example.sp20231226.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/cookie")
public class CookieController {
    @RequestMapping("add")
    public void addCookie(HttpServletResponse httpResponse){
        httpResponse.addCookie(new Cookie("testCookie", "123456"));
    }
}
