package com.example.sp20231226.model;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class User {
    private String userName;
    private int age;
}
