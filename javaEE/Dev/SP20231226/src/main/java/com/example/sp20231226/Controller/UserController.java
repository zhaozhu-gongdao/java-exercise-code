package com.example.sp20231226.Controller;

import com.example.sp20231226.model.Student;
import com.example.sp20231226.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @RequestMapping("testFun")
    public String testFun(@RequestBody User user){
        return user.toString();
    }

    @Value("${string.hello}")
    private String hello;

    @RequestMapping("/ymlKey")
    public String key(){
        return "读取到值" + hello;
    }

    @Autowired
    private Student student;

    @RequestMapping("/readStudent")
    public String readStudent(){
        return student.toString();
    }
}
