package com.example.sp20231219.Controller;

import com.example.sp20231219.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {
    @RequestMapping("/getProducts")
    public String getProducts(@RequestParam("proIds") List<String> proIds){
        String stringBuffer = new String();
        for (String proId : proIds){
            stringBuffer += "获取到了id为" + proId + "的商品";
        }

        return stringBuffer;
    }


    @Autowired
    @RequestMapping(value = "/m7")
    public Object method7(@RequestBody User user) {
        return user.toString();
    }
}
