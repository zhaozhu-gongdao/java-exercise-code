package com.example.sp20231219.Controller;

import com.example.sp20231219.Service.UserService;
import jakarta.servlet.http.HttpSession;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//把这个类交给spring进行管理
//@RestController
@RestController
public class UserController {

    //@Autowired
    private UserService service;

//    public UserController(UserService service){
//        this.service = service;
//    }


    @Autowired
    public void setService(UserService service) {
        this.service = service;
    }

    public void sayHi(){
        System.out.println("这是userController");
        service.sayHi();
    }

    @RequestMapping(value = {"/addUser", "/deleteUser"})
    public String valueTest(@RequestParam(value="tourist", defaultValue = "吴肖悦") String name){
        return name + "是神里绫人的狗";
    }

    @RequestMapping("/sumUser/{name}")
    public String valueTest1(@PathVariable(required = false) String name){
        return name + "是巴巴托斯的狗";
    }

    @RequestMapping("session")
    public void lookSession(HttpSession session){
        System.out.println(session);
    }
}
