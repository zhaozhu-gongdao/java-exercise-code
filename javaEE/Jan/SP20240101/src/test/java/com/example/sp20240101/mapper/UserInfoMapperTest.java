package com.example.sp20240101.mapper;

import com.example.sp20240101.model.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
class UserInfoMapperTest {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Test
    void queryAllUser() {
        List<UserInfo> userInfos = userInfoMapper.queryAllUser();
        System.out.println(userInfos);
    }
    @Test
    void selectOne(){
        UserInfo userInfo = userInfoMapper.selectOne(4);
        System.out.println(userInfo);
    }

    @Test
    void insert(){
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("jide");
        userInfo.setPassword("1412");
        userInfo.setGender(2);
        userInfo.setAge(17);
        userInfo.setPhone("1845856109");
        userInfoMapper.insert(userInfo);
    }

    @Test
    void insert2(){
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("wendi");
        userInfo.setPassword("1321");
        userInfo.setGender(1);
        userInfo.setAge(26);
        userInfo.setPhone("1845856109");
        Integer count = userInfoMapper.insert(userInfo);
        System.out.println("添加数据条数：" + count + ", 数据ID：" + userInfo.getId());
    }
}