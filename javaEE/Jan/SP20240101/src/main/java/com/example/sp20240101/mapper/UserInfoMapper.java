package com.example.sp20240101.mapper;

import com.example.sp20240101.model.UserInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserInfoMapper {
    @Select("select username, `password`, age, gender, phone from userinfo")
    public List<UserInfo> queryAllUser();

    @Select("select username, `password`, age, gender, phone from userinfo where id= #{id}")
    UserInfo selectOne(Integer id);

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into userinfo (username, password, age, gender, phone)" +
            "values(#{username}, #{password}, #{age}, #{gender}, #{phone})")
    Integer insert(UserInfo userInfo);

    @Delete("delete from userinfo where id = #{id}")
    void delete(Integer id);

}

