import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Solution {
    //游游的you
    public void youTim(){
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        for (int i = 0; i < q; i++) {
            int ret = 0;
            int yNum = in.nextInt();
            int oNum = in.nextInt();
            int uNum = in.nextInt();

            int minNum = Math.min(Math.min(yNum, oNum), uNum);
            yNum -= minNum;
            oNum -= minNum;
            uNum -= minNum;
            ret += minNum * 2;
            ret += oNum - 1;
            System.out.println(ret);
        }
    }

    //牛牛的快递
    public void cowPacket(){
        Scanner in = new Scanner(System.in);
        float a = in.nextFloat();
        char b = in.next().charAt(0);
        int money = 20;

        //处理加急的问题
        if (a > 1){
            float currenKg = a - 1;
            if (currenKg - (int)currenKg > 0){
                money += currenKg + 1;
            }
        }

        if (b == 'y'){
            money += 5;
        }

        System.out.println(money);
    }

    public void twoNumesLeng1(){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String str1 = in.next();
        String str2 = in.next();
        String[] strs = new String[n];

        for (int i = 0; i < n; i++){
            strs[i] = in.next();
        }

        int ret = 0;
        int prev1 = -1;
        int prev2 = -1;
        for (int i = 0; i < n; i++){
            if (strs[i].equals(str1)){
                if (prev2 != -1){
                    ret  = i - prev2;
                }
                prev1 = i;
            }else if (strs[i].equals(str2)){
                if (prev1 != -1){
                    ret = i - prev1;
                }

                prev2 = i;
            }
        }

        System.out.println(ret);
    }

    public void twoNumLength() throws Throwable {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        String[] str = reader.readLine().split(" ");
        String s1 = str[0], s2 = str[1];
        int prev1 = -1, prev2 = -1, ret = 0x3f3f3f3f;
        for (int i = 0; i < n; i++) {
            String s = reader.readLine();
            if (s.equals(s1)) // 去前⾯找最近的 s2
            {
                if (prev2 != -1) {
                    ret = Math.min(ret, i - prev2);
                }
                prev1 = i;
            } else if (s.equals(s2)) // 去前⾯找最近的 s1
            {
                if (prev1 != -1) {
                    ret = Math.min(ret, i - prev1);
                }
                prev2 = i;
            }
        }
        System.out.println(ret == 0x3f3f3f3f ? -1 : ret);
    }

    public void duplicateZeros(int[] arr) {
//        int cur = 0;
//        int dest = -1;
//        for (; dest < arr.length - 1; cur++){
//            if (arr[cur] == 0){
//                dest += 2;
//            }else {
//                dest++;
//            }
//        }
//        if (dest == arr.length){
//            arr[dest - 1] = 0;
//            dest -= 2;
//            cur -= 1;
//        }
//
//        for (int i = cur; i < arr.length; i--){
//            if (arr[0] == 0){
//                arr[dest--] = 0;
//                arr[dest--] = 0;
//            }else {
//                arr[dest--] = arr[i];
//            }
//        }

        int n = arr.length;
        int[] newArr = new int[n];
        int cur = 0;
        int dest = 0;

        while (dest < n){
            if (arr[cur] == 0){
                newArr[dest++] = 0;
                newArr[dest] = 0;
            }else{
                newArr[dest] = arr[cur];
            }

            dest++;
            cur++;
        }
    }
    public String solve (String s, String t) {
        int num1 = Integer.parseInt(s);
        int num2 = Integer.parseInt(t);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(num1 + num2);
        String ret = new String(stringBuilder);
        return ret;
    }
    class ListNode {
        int val;
        ListNode next = null;
        public ListNode(int val) {
            this.val = val;
        }
    }
    public ListNode addInList (ListNode head1, ListNode head2) {
        // write code here
        ListNode ret = null;
        ListNode head = ret;

        ListNode cur1 = head1, cur2 = head2;
        int bit = 0;
        //遍历两个链表
        while (cur1 != null && cur2 != null){
            int sum = cur1.val + cur2.val;
            bit = 0;
            if (sum >= 10){
                bit = 1;
            }
            ListNode node = new ListNode(sum % 10 + bit);
            head.next = node;
            head = node;
            cur1 = cur1.next;
            cur2 = cur2.next;

        }

        //一方无了
        if (cur1 == null){
            //第二行还有值
            while (cur2 != null){
                int sum = bit + cur2.val;
                if (sum > 10){
                    bit = 1;
                }
                ListNode node = new ListNode(sum % 10 + bit);
                head.next = node;
                head = node;
                cur2 = cur2.next;
            }
        }else{
            while (cur1 != null){
                int sum = bit + cur1.val;
                if (sum > 10){
                    bit = 1;
                }
                ListNode node = new ListNode(sum % 10 + bit);
                head.next = node;
                head = node;
                cur1 = cur1.next;
            }
        }

        return ret;
    }
    public static void main(String[] args) {
        Solution test = new Solution();
        test.duplicateZeros(new int[]{1, 0, 2, 3, 0, 4, 5, 0});
        int num = 100;
        int num2 = 9;
        int sum = num2 + num;
        //String s = new String(sum);

    }
}
