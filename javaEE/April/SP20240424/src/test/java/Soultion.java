import sun.java2d.pipe.SolidTextRenderer;

import java.util.Scanner;

public class Soultion {
    public int getLongestPalindrome (String A) {
        StringBuilder stringBuilder = new StringBuilder(A);
        char[] ch = A.toCharArray();
        stringBuilder.append(ch[0]);
        int i = 1;
        int ret = 0;
        int n = stringBuilder.length();
        while (stringBuilder.length() != 0){
            if (stringBuilder.charAt(n - 1) == ch[i]){
                stringBuilder.deleteCharAt(n - 1);
                ret++;
            }
            i++;
        }

        if (ret == 0){
            return 1;
        }else{
            return ret * 2 - 1;
        }

        Scanner scanner = new Scanner(System.in);
    }

    public static void main(String[] args) {
        Soultion test = new Soultion();
        test.getLongestPalindrome("abccba");
    }
}
