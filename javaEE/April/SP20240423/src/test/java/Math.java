import java.util.Scanner;

public class Math {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String str = in.next();
        char[] ch = str.toCharArray();
        int ret = -1;
        int head = -1;
        for (int i = 0; i < ch.length;){
            if (ch[i] >= '0' && ch[i] <= '9'){
                int left = i, right = i;
                while (right < ch.length&&ch[right] >= '0' && ch[right] <= '9'){
                    right++;
                }

                if (java.lang.Math.max(ret, right - left) != ret){
                    head = left;
                }
                ret = java.lang.Math.max(ret, right - left);
                i = right;
            }else{
                i++;
            }
        }

        //循环打印
        for (int i = head; i < ret; i++){
            System.out.print(ch[i]);
        }

    }
}
