package com.example.sp20240515.controller;

import com.example.sp20240515.model.MessageInfo;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/message")
public class MessageController {
    private List<MessageInfo> messageInfoList = new ArrayList<>();
    @RequestMapping("/publish")
    public Boolean publish(MessageInfo messageInfo){
        if (!StringUtils.hasLength(messageInfo.getMessage())
                || !StringUtils.hasLength(messageInfo.getFrom())
                || !StringUtils.hasLength(messageInfo.getTo())){
            return false;
        }

        messageInfoList.add(messageInfo);
        return true;
    }

    @RequestMapping("/getList")
    public List<MessageInfo> getMessageInfoList(){
        return messageInfoList;
    }
}
