package com.example.sp20240515.model;

import lombok.Data;

@Data
public class MessageInfo {
    private String from;
    private String to;
    private String message;

}
