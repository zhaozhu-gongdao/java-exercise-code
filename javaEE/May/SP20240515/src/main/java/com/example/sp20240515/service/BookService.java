package com.example.sp20240515.service;

import com.example.sp20240515.dao.BookDao;
import com.example.sp20240515.model.BookInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookService {
    @Autowired
    private BookDao bookDao;

    public List<BookInfo> getList(){
        List<BookInfo> bookInfos = bookDao.mockData();
        for (BookInfo book : bookInfos){
            if (book.getStatus() == 1){
                book.setStatusCN("已借阅");
            }else{
                book.setStatusCN("不可借阅");
            }
        }
        return bookInfos;
    }
}
