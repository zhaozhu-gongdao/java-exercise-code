package com.example.sp20240515.service;

import com.example.sp20240515.dao.UserDao;
import jakarta.servlet.http.HttpSession;
import org.springframework.util.StringUtils;

public class UserService {
    private UserDao userDao = new UserDao();

    public boolean login(String userName, String password, HttpSession session) {
        if (!StringUtils.hasLength(userName) || !StringUtils.hasLength(password)){
            return false;
        }

        if ("zhangsan".equals(userName) && "123456".equals(password)){
            session.setAttribute("userName", "zhangsan");
            return true;
        }

        return false;
    }
}
