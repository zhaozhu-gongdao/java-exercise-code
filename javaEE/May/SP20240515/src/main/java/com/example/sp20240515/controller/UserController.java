package com.example.sp20240515.controller;

import com.example.sp20240515.service.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    private UserService service= new UserService();

    @RequestMapping("/login")
    public boolean login(String userName, String password, HttpSession session){
        return service.login(userName,password, session);
    }

}
