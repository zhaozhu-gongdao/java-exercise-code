package com.example.sp20240515.dao;

import com.example.sp20240515.model.BookInfo;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class BookDao {
    //优化tip：如果知道要创建list的具体长度，创建时直接写上，这样后面就不需要再扩容了
    private List<BookInfo> bookInfos = new ArrayList<>(10);
    public List<BookInfo> mockData(){
        for (int i = 0; i < 10; i++){
            BookInfo book = new BookInfo();
            book.setId(i);
            book.setBookName("图书" + i);
            book.setAuthor("作者" + i);
            //随机生成一个200以下的整数
            book.setCount(new Random().nextInt(200));
            //随机生成一个100以下的小数
            book.setPrice(new BigDecimal(new Random().nextInt(100)));
            book.setPublish("出版社" + i);
            book.setStatus(i % 2 == 0?2:1);

            bookInfos.add(book);
        }

        return bookInfos;
    }
}
