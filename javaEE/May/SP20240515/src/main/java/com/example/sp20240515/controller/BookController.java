package com.example.sp20240515.controller;

import com.example.sp20240515.model.BookInfo;
import com.example.sp20240515.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {
    @Autowired
    private BookService bookService;

    @RequestMapping("getList")
    public List<BookInfo> getList(){
        return bookService.getList();
    }
}
