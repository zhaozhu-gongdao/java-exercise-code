package com.example.sp20240514.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/calc/calc")
public class CalcController {

    @RequestMapping("/sum")
    public String sum(Integer num1, Integer num2){
        return "计算的结果是：" + (num1 + num2);
    }
}
