package com.example.sp20240514.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @RequestMapping("/login")
    public boolean login(String userName, String password, HttpSession session){
        if (!StringUtils.hasLength(userName) || !StringUtils.hasLength(password)){
            return false;
        }
        if ("zhangsan".equals(userName) && "123456".equals((password))){
            session.setAttribute("username", "zhangsan");
            return true;
        }

        return false;
    }

    @RequestMapping("/getUserInfo")
    public String getUserInfo(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        String res = null;
        if (session != null) {
            res = (String) session.getAttribute("username");
        }

        if (session == null){
            System.out.println("null");
        }
        return res;
    }
}
