package com.example.sp20240514;

import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
//    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//
//
//        char[] ch = in.next().toCharArray();
//        //记录次数
//        int[] hash = new int[26];
//
//        for (int i = 0; i < ch.length; i++){
//            hash[ch[i] - 'a']++;
//        }
//
//        //遍历寻找hash里面数最大和最小的
//        int max = 0;
//        int min = 1200;
//        for (int x : hash){
//            if (x > max){
//                max = x;
//            }else if (x < min){
//                min = x;
//            }
//        }
//
//
//        //进行判断
//        if (isNum(max - min)){
//            //是质数
//            System.out.printf("Lucky Word\n" + (max-min));
//        }else{
//            //不是质数
//            System.out.printf("No Answer\n" + 0);
//        }
//    }
//
//    //判断传过来的数是不是质数
//    public static boolean isNum(int num){
//        if (num < 2){
//            return true;
//        }
//
//        for(int i = 2; i < Math.sqrt(num); i++){
//            if (num % i == 0){
//                return false;
//            }
//        }
//
//        return true;
//    }


    public static int[] searchLastNum(int[] arr) {
        int dest = -1;
        int cur = 0;

        while (cur < arr.length) {
            if (arr[cur] == 0) {
                dest += 2;
            } else {
                dest += 1;
            }

            if (dest >= arr.length - 1) {
                break;
            }

            cur++;
        }
        return new int[]{cur, dest};
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1, 0, 2, 3, 0, 4, 5, 0};
        Main.searchLastNum(arr);
    }
}