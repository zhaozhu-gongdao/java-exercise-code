package com.example.sp20240526.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
public class LoggerController {
    private static Logger log = LoggerFactory.getLogger(LoggerController.class);

    @PostConstruct
    public void print(){
        System.out.println("打印日志");
        log.info("=======我是日志框架打印的日志===================");
        log.error("我是error日志");
        log.warn("我是warn日志");
        log.info("我是info日志");
        log.debug("我是debug日志");
        log.trace("我是trace日志");
    }
}
