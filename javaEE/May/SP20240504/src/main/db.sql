create database if not exists blog_system charset utf8;

use blog_system;

drop table if exists user;
drop table if exists blog;

create table blog (
    blogId int primary key auto_increment,
    title varchar(256),
    content varchar(4096),
    userId int,
    postTime datetime
);

create table user (
    userId int primary key auto_increment,
    username varchar(64) unique,
    password varchar(64)
);

insert into user values(1, '蛋蛋', '123456'), (2, '砂金', '123456'),(3, '拉帝奥', '123456');
insert into blog values (1, '蛋蛋的当狗宣言', '我就要当wxy的狗，除了你，别人的狗我都不要当', 1, '2024-05-04 19:00:00');
insert into blog values (2, '是的我们有一个孩子（砂金篇)', '教授，又要仰仗你的智慧了', 2, '2024-05-04 19:00:00');
insert into blog values (3, '是的我们有一个孩子（教授篇)', '该死的赌徒', 3, '2024-05-04 19:00:00');

