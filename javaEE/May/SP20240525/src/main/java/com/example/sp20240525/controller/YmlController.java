package com.example.sp20240525.controller;

import com.example.sp20240525.model.Person;
import com.example.sp20240525.model.Student;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class YmlController {
    @Value("${demo.key2}")
    private String key2;

    @Value("${demo.key3}")
    private int key3;

    @Value("${string.str1}")
    private String str1;
    @Value("${string.str2}")
    private String str2;
    @Value("${string.str3}")
    private String str3;

    @Autowired
    private Student student;

    @Autowired
    private Person person;
    @PostConstruct
    public void  init(){
//        System.out.println(str1);
//        System.out.println(str2);
//        System.out.println(str3);
//        System.out.println(student);
        System.out.println(person.getName().toString());
        System.out.println(person.getMap().toString());
    }
}
