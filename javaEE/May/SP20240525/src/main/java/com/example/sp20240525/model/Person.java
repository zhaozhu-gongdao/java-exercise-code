package com.example.sp20240525.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "person")
public class Person {
    private List<String> name;

//    private String[] name;   还可以使用数组去接受

    private HashMap<String, String> map;
}
