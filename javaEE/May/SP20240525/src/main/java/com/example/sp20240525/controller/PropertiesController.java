package com.example.sp20240525.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PropertiesController {
    @Value("${demo.key1}")
    private String key1;

    @Value("${demo.key2}")
    private String key2;

    @RequestMapping("/readkey")
    public String readKey(){
        return key1;
    }

    @RequestMapping("/readKey2")
    public String readKey2(){
        System.out.println("hhh");
        return key2;
    }
}
