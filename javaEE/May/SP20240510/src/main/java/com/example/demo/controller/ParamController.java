package com.example.demo.controller;

import com.example.demo.model.Person;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/param")
public class ParamController {
    @RequestMapping("/m1")
    public String m1(String name){
        return name;
    }

    @RequestMapping("/m2")
    public String m2(String name, Integer age){
        return "name:" + name + ", age:" + age;
    }

    @RequestMapping("/m3")
    public String m3(Person person){
        return person.toString();
    }

    @RequestMapping("/m4")
    public String m4(@RequestParam("name") String username){
        return username;
    }
}
