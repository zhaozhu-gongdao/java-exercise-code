package com.example.sp20240512.controller;

import com.example.sp20240512.model.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@RequestMapping("/param")
@RestController
public class ParamController {
    private ObjectMapper objectMapper = new ObjectMapper();
    @RequestMapping("/m1")
    public String m1(@RequestBody Person person){
        return "收到的对象是：" + person.toString();
    }

    @RequestMapping("/m2/{id}/{userName}")
    public String m2(@PathVariable("id") Integer userId, @PathVariable String userName){
        return "接受到的id为：" + userId + "， userName为：" + userName;
    }

    @RequestMapping("/m3")
    public String m3(@RequestPart MultipartFile file) throws IOException {
        System.out.println(file.getOriginalFilename());
        file.transferTo(new File("D:/apply/" + file.getOriginalFilename()));
        return "success";
    }

    @RequestMapping("/getCookie1")
    public String getCookie1(HttpServletRequest request, HttpServletResponse response){
        Cookie[] cookies = request.getCookies();

        //使用lambda表达式进行循环
        Arrays.stream(cookies).forEach(cookie -> {
            System.out.println(cookie.getName() + ":" + cookie.getValue());
        });

        return "success";
    }

    @RequestMapping("/getCookie")
    public String getCookie(@CookieValue String name){
        return "Cookie存储的name为" + name;
    }

    @RequestMapping("/getSession1")
    public String getSession1(HttpServletRequest request){
        HttpSession session = request.getSession(false);  //默认值为true
        if (session != null){
            String username = (String)session.getAttribute("username");
            return "登录用户：" + username;
        }

        return "session 为空";
    }

    @RequestMapping("/setSession")
    public String setSession(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.setAttribute("username", "zhangsan");
        return "success";
    }

    @RequestMapping("/getSession2")
    public String getSession2(@SessionAttribute String username){
        return "session中存储的username为：" + username;
    }

    @RequestMapping("/getSession")
    public String getSession(HttpSession session){
        String username = (String)session.getAttribute("username");
        return "username为：" + username;
    }

    @RequestMapping("/getHeader1")
    public String getHeader(HttpServletRequest request){
        //获取Header中某个key的值
        String userAgent = request.getHeader("User-Agent");
        return "userAgent：" + userAgent;
    }

    @RequestMapping("/getHeader")
    public String getHeader(@RequestHeader("User-Agent") String userAgent){
        return "userAgent：" + userAgent;
    }
}
