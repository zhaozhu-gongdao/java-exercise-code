package com.example.sp20240520.model;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        int ret = 0;

        for (int i = 0; i < t; i++){
            int personNum = in.nextInt();
            int a = in.nextInt();
            int b = in.nextInt();
            ret = 0;

            if (b >= a){
                //优先使用三人船
                while (personNum > 0){
                    ret += b;
                    personNum -= 3;
                }
            }else{
                while (personNum > 0){
                    ret += a;
                    personNum -= 2;
                }
            }
        }

        System.out.println(ret);
    }
}


class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);

        List<List<Integer>> ret = new ArrayList<>();
        int n = nums.length;
        for (int i = 0; i < n;){
            //优化操作
            if (nums[i] > 0){
                break;
            }

            int left = i + 1;
            int right = n - 1;
            int target = -nums[i];
            while (left < right){
                if (nums[left] + nums[right] > target){
                    right--;
                }else if (nums[left] + nums[right] < target){
                    left++;
                }else if (nums[left] + nums[right] == target){
                    ret.add(new ArrayList<Integer>(Arrays.asList(nums[i],
                            nums[left], nums[right])));
                    right--;
                    left++;

                    while (left < right && nums[left] == nums[left - 1]){
                        left++;
                    }

                    while (left < right && nums[right] == nums[right + 1]){
                        right--;
                    }
                }
            }

            i++;
            while (i < n && nums[i] == nums[i - 1]){
                i++;
            }
        }

        return ret;
    }
}
