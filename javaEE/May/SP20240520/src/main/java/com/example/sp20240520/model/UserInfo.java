package com.example.sp20240520.model;

import lombok.Data;

@Data
public class UserInfo {
    private String username;
    private String password;
}
