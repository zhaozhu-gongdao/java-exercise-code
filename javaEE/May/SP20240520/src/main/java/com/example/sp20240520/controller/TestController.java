package com.example.sp20240520.controller;

import com.example.sp20240520.model.UserInfo;
import com.example.sp20240520.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
    private UserService userService;

    @Autowired
    private UserInfo u1;


    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }



    public void doController(){
        userService.doService();
        System.out.println(u1);
        System.out.println("这是Controoler注解");
    }
}
