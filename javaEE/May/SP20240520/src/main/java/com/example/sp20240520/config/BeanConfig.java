package com.example.sp20240520.config;

import com.example.sp20240520.model.UserInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class BeanConfig {
    //使用@Bean定义多个对象
    @Bean("u1")
    public UserInfo userInfoZ(){
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("zhangsan");
        userInfo.setPassword("123456");
        return userInfo;
    }

    @Bean
    public UserInfo userInfoL(){
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("lisi");
        userInfo.setPassword("123456");
        return userInfo;
    }

    @Bean
    public UserInfo userInfoW(String nameW){
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername(nameW);
        userInfo.setPassword("123456");
        return userInfo;
    }

    @Bean
    public String nameW(){
        return "wangwu";
    }

    @Bean
    public String nameZ(){
        return "zhaoliu";
    }

}
