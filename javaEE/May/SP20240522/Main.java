package com.example.sp20240522;

public class Main {
    public static int longestOnes(int[] nums, int k) {
        int zeroCount = 0;
        int ret = 0;
        for (int left = 0, right = 0; right < nums.length; right++){
            if (nums[right] == 0){
                zeroCount++;
            }

            while (zeroCount > k){
                left++;
            }

            ret = Math.max(ret, right - left + 1);
        }

        return ret;
    }

    public static void main(String[] args) {
        longestOnes(new int[]{1,1,1,0,0,0,1,1,1,1,0}, 2);
    }
}
