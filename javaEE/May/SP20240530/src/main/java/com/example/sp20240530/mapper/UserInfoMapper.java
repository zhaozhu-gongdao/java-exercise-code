package com.example.sp20240530.mapper;

import com.example.sp20240530.model.UserInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserInfoMapper {
    @Select("select id, username, password, age, gender," +
            "phone, delete_flag as deleteFlag, create_time as createTime," +
            "update_time as updateTime from userInfo ")
    List<UserInfo> selectAll();

//    @Result({
//        @Result(column = "delete_flag", property = "deleteFlag"),
//        @Result(column = "create_time", property = "createTime")
//    })
    @Results(id = "BaseMap", value = {
        @Result(column = "delete_flag", property = "deleteFlag"),
        @Result(column = "create_time", property = "createTime"),
        @Result(column = "update_time", property = "updateTime")
    })
    @Select("select * from userinfo")
    List<UserInfo> selectAll1();

    @ResultMap(value = "BaseMap")
    @Select("select * from userinfo where id = #{id}")
    UserInfo selectUser2(Integer id);

    @Select("select * from userinfo where username = #{username}")
    UserInfo selectUser(String username);

    //@Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into userinfo (username, password, age, gender, phone)" +
            "values(#{user.username}, #{user.password}, #{user.age}, " +
            "#{user.gender}, #{user.phone})")
    Integer insert(@Param("user") UserInfo userInfo);

    @Delete("delete from userinfo where id = #{id}")
    Integer delete(Integer id);

    @Update("update userinfo set age = #{age} where id = #{id}")
    Integer update(UserInfo userInfo);


}
