package com.example.sp20240530.mapper;

import com.example.sp20240530.model.BookInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BookInfoMapper {
    @Select("select ta.*, tb.username, tb.age from bookinfo ta " +
            "LEFT JOIN userinfo tb on ta.uid = tb.id where uid = #{uid}")
    List<BookInfo> selectByUid(Integer uid);

    @Select("select * from bookinfo where id = #{id}")
    List<BookInfo> selectById(Integer id);

    @Select("select * from bookinfo where id = ${id}")
    List<BookInfo> selectById2(Integer id);

    @Select("select * from bookinfo where bookName = '${bookName}'")
    BookInfo selectByName(String bookName);

    @Select("select * from bookinfo order by id #{sort}")
    BookInfo selectBookSort(String sort);

    @Select("select * from bookinfo where bookName like '#{bookName}'")
    List<BookInfo> selectBookByLike(String bookName);
}
