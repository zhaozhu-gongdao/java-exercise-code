package com.example.sp20240530.mapper;

import com.example.sp20240530.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Mapper
public interface UserInfoXMLMapper {
    List<UserInfo> selectAll();
    List<UserInfo> selectAll2();

    Integer insert(@Param("user") UserInfo userInfo);

    Integer delete(Integer id);

    Integer update(UserInfo userInfo);
}
