package com.example.sp20240530.mapper;

import com.example.sp20240530.model.BookInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest
class BookInfoMapperTest {
    @Autowired
    private BookInfoMapper bookInfoMapper;

    @Test
    void selectByUid() {
        List<BookInfo> bookInfoList = bookInfoMapper.selectByUid(1);
        log.info(bookInfoList.toString());
    }

    @Test
    void selectById() {
        List<BookInfo> bookInfoList = bookInfoMapper.selectById(4);
        log.info(bookInfoList.toString());
    }

    @Test
    void selectById2() {
        List<BookInfo> bookInfoList = bookInfoMapper.selectById2(4);
        log.info(bookInfoList.toString());
    }

    @Test
    void selectByName() {
        BookInfo bookInfo = bookInfoMapper.selectByName("Mybatis使用");
        log.info(bookInfo.toString());
    }

    @Test
    void selectBookByLike() {
        List<BookInfo> bookInfos = bookInfoMapper.selectBookByLike("%使用%");
        log.info(bookInfos.toString());
    }
}