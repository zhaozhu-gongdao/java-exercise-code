package com.example.sp20240530.mapper;

import com.example.sp20240530.model.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class UserInfoXMLMapperTest {
    @Autowired
    private UserInfoXMLMapper userInfoXMLMapper;
    @Test
    void selectAll() {
        List<UserInfo> userInfoList = userInfoXMLMapper.selectAll();
        log.info(userInfoList.toString());
    }

    @Test
    void insert() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("蛋蛋超人");
        userInfo.setGender(1);
        userInfo.setAge(18);
        userInfo.setPassword("123456");
        userInfoXMLMapper.insert(userInfo);
        log.info(""+ userInfo.getId() + userInfo.getUsername());
        log.error(userInfo.getId().toString());
    }

    @Test
    void delete() {
        userInfoXMLMapper.delete(1);
    }

    @Test
    void update() {
        UserInfo userInfo = new UserInfo();
        userInfo.setAge(12);
        userInfo.setId(2);
        Integer res = userInfoXMLMapper.update(userInfo);
        if (res > 0){
            log.info("数据更新成功");
        }
    }

    @Test
    void selectAll2() {
        List<UserInfo> userInfoList = userInfoXMLMapper.selectAll();
        log.info(userInfoList.toString());
    }
}