package com.example.sp20240530.mapper;

import com.example.sp20240530.model.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@Slf4j
@SpringBootTest
class UserInfoMapperTest {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Test
    void selectAll() {
        List<UserInfo> userInfos = userInfoMapper.selectAll();
        log.info(userInfos.toString());
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void selectUser() {
        UserInfo userInfo = userInfoMapper.selectUser2(8);
        log.info(userInfo.toString());
    }

    @Test
    void insert() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("ladiao");
        userInfo.setGender(1);
        userInfo.setAge(20);
        userInfo.setPassword("123456");
        userInfoMapper.insert(userInfo);
        log.info(""+ userInfo.getId() + userInfo.getUsername());
    }

    @Test
    void delete() {
        userInfoMapper.delete(10);
    }

    @Test
    void update() {
        UserInfo userInfo = new UserInfo();
        userInfo.setAge(22);
        userInfo.setId(11);
        Integer res = userInfoMapper.update(userInfo);
        if (res > 0){
            log.info("数据更新成功");
        }
    }

    @Test
    void selectAll3() {
        List<UserInfo> userInfos = userInfoMapper.selectAll();
        log.info(userInfos.toString());
    }

    @Test
    void selectUser2() {
        UserInfo userInfo = userInfoMapper.selectUser2(7);
        log.info(userInfo.toString());
    }
}