import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/index")
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //检查用户是否登录
        HttpSession session = req.getSession(false);
        if (session == null){
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("请先执行登录操作");
            return;
        }

        String username = (String)session.getAttribute("username");
        Long time = (Long)session.getAttribute("time");
        System.out.println("username=" + username + ", time=" + time);

        resp.setContentType("text/html; charset=utf8");
        resp.getWriter().write("欢迎您， " + username + ", 上次登录时间：" + time);
    }
}
