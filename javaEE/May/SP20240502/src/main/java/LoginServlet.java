import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username == null || password == null || username.equals("") || password.equals("")){
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("请求的参数不完整");
            return;
        }

        //这里直接指定用户名一定要是zhangsan，密码一定要是123
        if (!username.equals("zhangsan")){
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("用户名错误！");
            return;
        }

        if (!password.equals("123")){
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("密码错误！");
            return;
        }

        //登录成功，给该用户创建会话
        HttpSession session = req.getSession(true);
        session.setAttribute("username", username);
        session.setAttribute("time", System.currentTimeMillis());

        resp.sendRedirect("index");
    }
}
