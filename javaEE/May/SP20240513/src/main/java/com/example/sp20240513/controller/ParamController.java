package com.example.sp20240513.controller;

import com.example.sp20240513.model.Person;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/param")
public class ParamController {
    @RequestMapping("/index")
    public String index(){
        return "/index.html";
    }

    @ResponseBody
    @RequestMapping("/responseData")
    public String responseData(){
        return "这是返回数据";
    }

    @RequestMapping("/returnHTML")
    public String returnHTML(){
        return "<h1>返回了h1标签</h1>";
    }

    @RequestMapping("/returnJSON")
    public Person returnJSON(){
        Person person = new Person();
        person.setUsername("wxy");
        person.setAge(19);
        return person;
    }

    @RequestMapping("/setStatus")
    public String setStatus(HttpServletResponse response){
        response.setStatus(404);
        return "ok";
    }

    @RequestMapping(value = "/setHeader", produces = "application/json;charset=utf-8")
    public String setHeader(){
        return "{‘username': 'zhangsan'}";
    }
}
