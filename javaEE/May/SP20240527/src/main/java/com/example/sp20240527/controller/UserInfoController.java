package com.example.sp20240527.controller;

import com.example.sp20240527.mapper.UserInfoMapper;
import com.example.sp20240527.model.UserInfo;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserInfoController {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @PostConstruct
    public void init(){
        List<UserInfo> userInfos = userInfoMapper.selectAll();
        System.out.println(userInfos.toString());
    }
}
