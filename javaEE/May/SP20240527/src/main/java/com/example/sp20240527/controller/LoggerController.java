package com.example.sp20240527.controller;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class LoggerController {
    @PostConstruct
    public void print(){
        log.error("我是error日志");
        log.warn("我是warn日志");
        log.info("我是info日志");
        log.debug("我是debug日志");
        log.trace("我是trace日志");
    }
}
