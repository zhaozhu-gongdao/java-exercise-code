package com.example.sp20240527.model;

import lombok.Data;

import java.util.Date;

@Data
public class UserInfo {
    private Integer id;
    private String userName;
    private String password;
    private Integer age;
    private Integer gender;
    private String phone;
    private int deleteFlag;
    private Date creatTime;
    private Date updateTime;
}
