package com.example.sp20240511.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/param")
public class ParamController {

    @RequestMapping("/m1")
    public String m1(String[] name){
        //使用lambda表达式进行循环
        Arrays.stream(name).forEach(s -> {
            System.out.print(s + " ");
        });
        return "已经正常接受数组";
    }

    @RequestMapping("/m2")
    public String m2(@RequestParam List<String> name){
        return "接收到的list对象有：" + name.toString();
    }
}
