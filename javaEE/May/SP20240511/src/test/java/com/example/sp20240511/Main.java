package com.example.sp20240511;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int t = in.nextInt();
        //一共进行t组
        for (int i = 0; i < t; i++){
            int personH = in.nextInt();
            int personA = in.nextInt();
            int anH = in.nextInt();
            int newAnH = anH;
            int anA = in.nextInt();
            if (anA == 0){
                System.out.println(-1);
                continue;
            }
            int num = 0;
            while (personH > 0){
                anH -= personA;
                personH -= anA;
                if (anH <= 0){
                    num++;
                    anH = newAnH;
                }
            }
            System.out.println(num);
        }
    }
}
