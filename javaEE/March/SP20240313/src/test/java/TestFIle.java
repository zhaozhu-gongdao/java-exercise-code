import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TestFIle {
    public static void main(String[] args) throws IOException {
        String srcPath = "d:/else/src.txt";
        String destPath = "d:/else/dest.txt";

        FileInputStream fileInputStream = new FileInputStream(srcPath);
        FileOutputStream fileOutputStream = new FileOutputStream(destPath);

        while (true){
            int ch = fileInputStream.read();

            if (ch == -1){
                break;
            }

            fileOutputStream.write(ch);
        }

        fileInputStream.close();
        fileOutputStream.close();
    }
}
