import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Testexec {
    public static void main(String[] args) throws IOException, InterruptedException {
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec("javac");

        //获取标准输入
        InputStream stdoutFrom = process.getInputStream();
        FileOutputStream stdoutTo = new FileOutputStream("stdout.txt");
        while (true){
            int ch = stdoutFrom.read();
            if (ch == -1){
                break;
            }

            stdoutTo.write(ch);
        }
        stdoutTo.close();
        stdoutFrom.close();

        //获取标准错误
        InputStream stderrFrom = process.getErrorStream();
        FileOutputStream stderrTo = new FileOutputStream("stderr.txt");
        while (true){
            int ch = stderrFrom.read();
            if (ch == -1){
                break;
            }

            stderrTo.write(ch);
        }
        stderrTo.close();
        stderrFrom.close();

        int exitCode = process.waitFor();
        System.out.println(exitCode);
    }
}
