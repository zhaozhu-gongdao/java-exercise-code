public class Solution {

    public int lengthOfLongestSubstring(String ss) {
        char[] s = ss.toCharArray();
        int[] hash = new int[128];
        int ret = 0;
        int n = ss.length();

        for (int left = 0, right = 0; right < n; ){
            char ch = s[right];
            hash[ch]++;

            while (hash[ch] > 1){
                hash[s[left++]]--;
            }

            ret = Math.max(ret, right - left + 1);
            right++;
        }

        return ret;
    }
    public int minOperations(int[] nums, int x) {
        int n = nums.length;
        int tend = 0;
        int ret = -1;
        //遍历求和
        for (int f : nums){
            tend += f;
        }

        int target = tend - x;

        if (target < 0){
            return -1;
        }
        int len = 0;

        for (int left = 0, right = 0, sum = 0; right < n; right++){
            sum += nums[right];

            while (sum > target){
                sum -= nums[left++];
            }

            if (sum == target){
                ret = Math.max(ret, right - left + 1);
            }
        }

        if(ret == -1) return ret;
        else return nums.length - ret;
    }

    public int longestOnes(int[] nums, int k) {
        int ret = 0;
        int zero = 0;

        for (int left = 0, right = 0; right < nums.length; right++){
            if (nums[right] == 0){
                zero++;
            }

            while (zero > k){
                if (nums[left] == 0){
                    zero--;
                }
                left++;
            }

            ret = Math.max(ret, right - left + 1);
        }

        return ret;

    }
}
