import java.io.*;
import java.util.Scanner;

//扫描指定目录，并找到包含指定字符的所有普通文件，并且后续询问是否删除该文件
public class IOTest {
//    public static void main(String[] args) throws IOException {
//        System.out.println("请输入要查找的目录：");
//        Scanner scanner = new Scanner(System.in);
//        File rootPath = new File(scanner.next());
//        System.out.println("请输入要删除的文件名：");
//        String deleteName = scanner.next();
//
//        //判断是不是目录
//        if (!rootPath.isDirectory()){
//            System.out.println("输入的不是目录，无法查询");
//            return;
//        }
//        scanDir(rootPath, deleteName);
//
//    }
//
//    public static void scanDir(File currentFile, String deleteName){
//        File[] files  = currentFile.listFiles();
//        if (files == null || files.length == 0){
//            return;
//        }
//
//
//
//        for (File f : files){
//            System.out.println(f.getAbsolutePath());
//            if (f.isDirectory()){
//                scanDir(f, deleteName);
//            }else {
//                dealFile(f, deleteName);
//            }
//        }
//    }
//
//    public static void dealFile(File file, String deleteName){
//        if (!file.getName().contains(deleteName)){
//            return;
//        }
//
//        System.out.println("您要删除的文件是" + file.getAbsolutePath() + "请问是否要删除（Y/N）");
//        Scanner scanner = new Scanner(System.in);
//        String res = scanner.next();
//
//        if (res.equals("y") || res.equals("Y")){
//            file.delete();
//            System.out.println("删除成功");
//        }else{
//            System.out.println("已取消删除");
//        }
//
//    }

    //把test1的文件复制到test2中    public static void main(String[] args) throws IOException {
    public static void main(String[] args) throws IOException {
//        Writer writer = new FileWriter("./src/test/java/test2.txt",true);
//        try(Reader reader = new FileReader("./src/test/java/test1.txt")){
//            while (true){
//                int ch = reader.read();
//                if (ch == -1){
//                    break;
//                }
//
//                writer.write(ch);
//            }
//        }
//
//        writer.close();


        System.out.println("请输入要复制的文件：");
        Scanner scanner = new Scanner(System.in);
        File srcFile = new File(scanner.next());
        if (!srcFile.isFile()){
            System.out.println("您输入的源文件路径非法");
            return;
        }

        System.out.println("请输入要复制的地址");
        String dest = scanner.next();
        File destFile = new File(dest);
        if (!destFile.getParentFile().isDirectory()){
            System.out.println("您输入的目标文件路径非法");
            return;
        }


        //复制文件
        try(InputStream inputStream = new FileInputStream(srcFile);
            OutputStream outputStream = new FileOutputStream(destFile)){
//            while (true){
//                byte[] buffer = new byte[5];
//                int n = inputStream.read(buffer);
//                System.out.println("n = " + n);
//
//                if (n == -1){
//                    break;
//                }
//                outputStream.write(buffer,0, n);
//            }
        }
    }
}
