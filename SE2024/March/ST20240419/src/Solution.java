import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class Solution {
    //数字统计
    public void sizeStats(){
        Scanner in = new Scanner(System.in);

        int L = in.nextInt();
        int R = in.nextInt();

        int size = 0;
        for (int i = L; i <= R; i++){
            int tmp = i;
            //数字拆分
            while (tmp > 0){
                if (tmp % 10 == 2){
                    size++;
                }

                tmp /= 10;
            }
        }

        System.out.println(size);

    }


    //两个数组的交集
    public ArrayList<Integer> intersection (ArrayList<Integer> nums1, ArrayList<Integer> nums2) {
        ArrayList<Integer> ret = new ArrayList<>();

        boolean[] hash = new boolean[1010];
        for (int i = 0; i < nums1.size(); i++){
            hash[nums1.get(i)] = true;
        }

        for (int i = 0; i < nums2.size(); i++){
            if (hash[nums2.get(i)]){
                ret.add(nums2.get(i));
                hash[nums2.get(i)] = false;
            }
        }
        return ret;
    }

    //点击消除
    public void
}
