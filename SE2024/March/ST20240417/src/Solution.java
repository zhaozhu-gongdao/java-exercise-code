import java.util.ArrayList;
import java.util.Scanner;

public class Solution {
    //数字统计
    public void numsStats(){
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        int size = 0;
        for (int i = a; i <= b; i++){
            int tmp = i;
            while (tmp > 0){
                if (tmp % 10 == 2){
                    size++;
                }

                tmp /= 10;
            }
        }

        System.out.println(size);
    }

    //两个数组的交集
    public ArrayList<Integer> intersection (ArrayList<Integer> nums1, ArrayList<Integer> nums2) {
        ArrayList<Integer> ret = new ArrayList<>();
        boolean[] hash = new boolean[1010];

        for (int i : nums1){
            hash[i] = true;
        }

        for (int i : nums2){
            if (hash[i] == true){
                ret.add(i);
                hash[i] = false;
            }
        }
        return ret;
    }

    //两个数组的交集
    public ArrayList<Integer> intersection2 (ArrayList<Integer> nums1, ArrayList<Integer> nums2) {
        ArrayList<Integer> ret = new ArrayList<>();
        for (int i : nums2){
            if (nums1.contains(i) && !ret.contains(i)){
                ret.add(i);
            }
        }
        return ret;
    }

    public static void main1(String[] args) {
        StringBuilder ret = new StringBuilder();
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();

        for (int i = 0; i < s.length(); i++){
            char ch = s.charAt(i);
            if (ret.length() != 0 && ret.charAt(ret.length() - 1) == ch){
                ret.deleteCharAt(s.length() - 1);
            }else {
                ret.append(ch);
            }
        }

        if (ret.length() != 0){
            System.out.println(ret.toString());
        }else {
            System.out.println(0);
        }


    }


    public static void main2(String[] args) {
        //输入操作
        Scanner scanner = new Scanner(System.in);
        int strsLength = scanner.nextInt();

        String str1 = scanner.next();
        String str2 = scanner.next();
        String[] strs = new String[strsLength];
        scanner.nextLine();

        for (int i = 0; i < strsLength; i++ ){
            strs[i] = scanner.nextLine();
        }

        StringBuilder str1Sign = new StringBuilder();
        StringBuilder str2Sign = new StringBuilder();

        for (int i = 0; i < strsLength; i++){
            if (strs[i].equals(str1)){
                str1Sign.append(i);
            }

            if (strs[i].equals(str2)){
                str2Sign.append(i);

            }
        }

        int ret = 0;
        for (int i = 0; i < str1Sign.length(); i++){
            for (int j = 0; i < str2Sign.length(); j++){
                ret = Math.min(str2Sign.charAt(j) - str1Sign.charAt(i), ret);
            }
        }

        System.out.println(ret);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int[] floor = new int[num];
        for (int i = 0; i < num; i++){
            floor[i] = scanner.nextInt();
        }
    }
}
