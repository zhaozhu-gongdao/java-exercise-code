import java.util.Arrays;

public class Solution {
    //和为target的数
    public int[] twoSum(int[] price, int target) {
        int left = 0;
        int right = price.length - 1;
        int[] ret = null;

        while (left < right){
            if (price[right] + price[left] > target){
                right--;
            }else if (price[right] + price[left] < target){
                left++;
            }else{
                return new int[]{price[left], price[right]};
            }
        }

        return ret;
    }

    //和为s
    public int triangleNumber(int[] nums) {
        Arrays.sort(nums);
        int ret = 0;

        //定下c
        int n = nums.length - 1;
        for (int i = n; i >= 2; i--){
            int c = nums[i];
            int left = 0;
            int right = i - 1;

            while (left < right){
                if (nums[left] + nums[right] > c){
                    ret += right - left;
                    right--;
                }else{
                    left++;
                }
            }

        }

        return ret;
    }
}
