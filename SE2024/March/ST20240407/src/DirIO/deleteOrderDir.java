package DirIO;

import java.io.File;
import java.util.Scanner;

public class deleteOrderDir {
    //扫描指定目录，并找到名称中包含指定字符的所有普通文件（不包含目录），并且后续询问用户是否要删除该文件
    public static void main(String[] args) {
        System.out.println("请输入要要查找的目录:");

        Scanner scanner = new Scanner(System.in);
        String searchRoot = scanner.next();
        File file = new File(searchRoot);

        System.out.println("请输入要查询并且删除的关键字:");
        String searchWord = scanner.next();

        if (!file.isDirectory()){
            System.out.println("输入的路径有误");
        }
        scanDir(file, searchWord);
    }


    //遍历查找的这个目录
    public static void scanDir(File file, String searchWord){
        File[] files = file.listFiles();

        if (files == null || files.length == 0){
            return;
        }
        for (File x : files){
            if (x.isDirectory()){
                scanDir(x, searchWord);
            }else{
                operFile(x, searchWord);
            }
        }

    }

    public static void operFile(File file, String searchWord){
        if (!file.getName().contains(searchWord)){
            return;
        }

        file.deleteOnExit();
        System.out.println("删除成功");
    }
}
