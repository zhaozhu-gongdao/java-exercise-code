import java.io.File;
import java.io.IOException;

public class FileSyTest {
    public static void createFile() throws IOException {
        File file = new File("./","test1.txt");
        file.createNewFile();
        File file1 = new File("./test2.txt");
        file1.createNewFile();
    }

    public static void main(String[] args) throws IOException {
        //createFile();
        //delete();
        //createMadirs();
        renameDir();
    }

    public static void delete(){
        File file = new File("./test1.txt");
        file.deleteOnExit();
        file.delete();
    }

    public static void createMadirs(){
        File file = new File("./test/first/second");
        file.mkdirs();
    }

    public static void renameDir() throws IOException {
        File file1 = new File("./test/first/test.txt");
        File file2 = new File("./test/test3.txt");
        file2.createNewFile();
        file2.renameTo(file1);
    }
}
