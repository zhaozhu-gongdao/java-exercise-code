import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileConTest {
    public static void readFile() throws IOException {
        try(InputStream inputStream = new FileInputStream("./test/first/test.txt")){
            while (true){
                int b = inputStream.read();
                if (b == -1){
                    break;
                }

                System.out.printf("%c", b);
            }
        }
    }

    public static void readFile1() throws IOException {
        try(InputStream inputStream = new FileInputStream("./test/first/test.txt")){
            byte[] buf = new byte[1024];
            int len;

            while (true){
                len = inputStream.read(buf);
                if (len == -1){
                    break;
                }

                for (int i = 0; i < len; i++) {
                    System.out.printf("%c", buf[i]);
                }
            }
        }
    }

    public static void readFile2() throws IOException {
        try(InputStream inputStream = new FileInputStream("./test/first/test.txt")){
            try(Scanner scanner = new Scanner(inputStream,"UTF-8")){
                while(scanner.hasNext()){
                    String s = scanner.next();
                    System.out.print(s);
                }
            }
        }
    }
//    public static void main(String[] args) throws IOException {
//        //readFile2();
//        //search();
//        copyFile();
//    }

    public static void main(String[] args) throws IOException {
        System.out.println("请输入要查找的目录：");
        Scanner scanner = new Scanner(System.in);
        File rootPath = new File(scanner.next());
        System.out.println("请输入要删除的文件名：");
        String deleteName = scanner.next();

        //判断是不是目录
        if (!rootPath.isDirectory()){
            System.out.println("输入的不是目录，无法查询");
            return;
        }
        scanDir(rootPath, deleteName);

    }

    public static void scanDir(File currentFile, String deleteName){
        File[] files  = currentFile.listFiles();
        if (files == null || files.length == 0){
            return;
        }



        for (File f : files){
            System.out.println(f.getAbsolutePath());
            if (f.isDirectory()){
                scanDir(f, deleteName);
            }else {
                dealFile(f, deleteName);
            }
        }
    }

    public static void dealFile(File file, String deleteName){
        if (!file.getName().contains(deleteName)){
            return;
        }

        System.out.println("您要删除的文件是" + file.getAbsolutePath() + "请问是否要删除（Y/N）");
        Scanner scanner = new Scanner(System.in);
        String res = scanner.next();

        if (res.equals("y") || res.equals("Y")){
            file.delete();
            System.out.println("删除成功");
        }else{
            System.out.println("已取消删除");
        }

    }
    public static void search() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入要查找的根目录：");
        File rootPath = new File(scanner.next());
        if (!rootPath.isDirectory()){
            System.out.println("您输入的根目录有误");
            return;
        }

        System.out.println("请输入要查找的文件名");
        String fileName = scanner.next();
        List<File> res = new ArrayList<>();

        scanDir(rootPath, fileName, res);
        System.out.println("共找到了符合条件的文件 " + res.size() + "个，它们分别是" );

        for (File file : res) {
            System.out.println(file.getCanonicalPath());
        }
    }

    public static void scanDir(File currentDir, String searchName, List<File> res) throws IOException {
        File[] files = currentDir.listFiles();

        if (files == null || files.length == 0){
            return;
        }

        for (File f : files){
            if (f.isDirectory()){
                scanDir(f, searchName, res);
            }else{
                if (isContentContains(f, searchName)) {
                    res.add(f.getAbsoluteFile());
                }
            }
        }
    }

    public static boolean isContentContains(File currentFile, String searchName) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try(InputStream inputStream = new FileInputStream(currentFile)){
            try(Scanner scanner = new Scanner(inputStream, "UTF-8")){
                while (scanner.hasNextLine()){
                    stringBuilder.append(scanner.nextLine());
                    stringBuilder.append("\r\n");
                }
            }
        }

        return stringBuilder.indexOf(searchName) != -1;
    }



    public static void copyFile() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入要复制的源文件路径: ");
        String src = scanner.next();
        File srcFile = new File(src);
        if (!srcFile.isFile()) {
            System.out.println("您输入的源文件路径非法!");
            return;
        }
        System.out.println("请输入要复制到的目标路径: ");
        String dest = scanner.next();
        File destFile = new File(dest);

        if (!destFile.getParentFile().isDirectory()) {
            System.out.println("您输入的目标文件路径非法!");
            return;
        }

        try (InputStream inputStream = new FileInputStream(srcFile);
             OutputStream outputStream = new FileOutputStream(destFile)) {
            while (true) {
                byte[] buffer = new byte[20480];
                int n = inputStream.read(buffer);
                System.out.println("n = " + n);
                if (n == -1) {
                    System.out.println("读取到 eof, 循环结束. ");
                    break;
                }
                outputStream.write(buffer, 0, n);
            }
        }
    }

}
