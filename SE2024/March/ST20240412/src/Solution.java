import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);

        List<List<Integer>> ret = new ArrayList<>();
        int n = nums.length;
        for (int i = 0; i < n;){
            //优化操作
            if (nums[i] > 0){
                break;
            }

            int left = i + 1;
            int right = n - 1;
            int target = -nums[i];
            while (left < right){
                if (nums[left] + nums[right] > target){
                    right--;
                }else if (nums[left] + nums[right] < target){
                    left++;
                }else if (nums[left] + nums[right] == target){
                    ret.add(new ArrayList<Integer>(Arrays.asList(nums[i],
                            nums[left], nums[right])));
                    right--;
                    left++;

                    while (left < right && nums[left] == nums[left - 1]){
                        left++;
                    }

                    while (left < right && nums[right] == nums[right + 1]){
                        right--;
                    }
                }
            }

            i++;
            while (i < n && nums[i] == nums[i - 1]){
                i++;
            }
        }

        return ret;
    }

    public static void main(String[] args) {
        Solution test = new Solution();
        test.threeSum(new int[]{-1, 0, 1, 2, -1, -4});
    }
}
