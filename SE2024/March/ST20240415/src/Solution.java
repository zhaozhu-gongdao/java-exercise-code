import java.util.ArrayList;

public class Solution {
    public int minOperations(int[] nums, int x) {
        int n = nums.length;
        int tend = 0;
        int ret = -1;
        //遍历求和
        for (int f : nums){
            tend += f;
        }

        int target = tend - x;

        if (target < 0){
            return -1;
        }
        int len = 0;

        for (int left = 0, right = 0, sum = 0; right < n; right++){
            sum += nums[right];

            while (sum > target){
                sum -= nums[left++];
            }

            if (sum == target){
                len = Math.max(len, right - left + 1);
            }
        }

        if(ret == -1) return ret;
        else return nums.length - ret;
    }

    public  void tesr (String[] args) {
        int ret = 0;

        for (String s : args){
            char[] ch = s.toCharArray();

            for (int i = 0; i < ch.length; i++){
                if (ch[i] == '2'){
                    ret++;
                }
            }
        }

        System.out.println(ret);
    }

    public ArrayList<Integer> intersection (ArrayList<Integer> nums1, ArrayList<Integer> nums2) {
        ArrayList<Integer> ret = new ArrayList<>();
        for (int i = 0; i < nums1.size(); i++){
            for (int j = 0; j < nums2.size(); j++){
                int tmp = nums2.get(j);
                if (nums1.contains(tmp) && !ret.contains(tmp)){
                    ret.add(tmp);
                }
            }
        }
        return ret;
    }

    String

    public static void main(String[] args) {
        Solution test = new Solution();
//        test.minOperations(new int[]{1, 1, 4, 2, 3}, 5);
//        System.out.println();
        //tesr(new String[]{"2", "22"});
        String[] a = new String[]{"2", "22"};

        ArrayList nums1 = new ArrayList();
        ArrayList nums2 = new ArrayList();
        nums1.add(1);
        nums1.add(2);
        nums1.add(3);
        nums2.add(8);
        nums2.add(2);
        nums2.add(2);
        nums2.add(3);
        nums2.add(8);

        test.intersection(nums1, nums2);
    }
}
