public class Solution {
    public int lengthOfLongestSubstring(String ss) {
        char[] s = ss.toCharArray();
        int[] hash = new int[128];
        int ret = 0;
        int n = ss.length();

        for (int left = 0, right = 0; right < n; ){
            char ch = s[right];
            hash[ch]++;

            while (hash[ch] > 1){
                hash[s[left++]]--;
            }

            ret = Math.max(ret, right - left + 1);
            right++;
        }

        return ret;
    }

    public static void main(String[] args) {
        Solution test = new Solution();
        test.lengthOfLongestSubstring("abcabcbb");
    }
}
