package DirTest;

import sun.java2d.pipe.SolidTextRenderer;

import java.io.*;
import java.util.Scanner;

public class FileCopy {
//    public static void main(String[] args) throws FileNotFoundException {
//        //输入流指的是把硬盘中的东西写到硬盘
//        FileInputStream fin = new FileInputStream("./src/a.txt");
//        //输入流指的是把内存的东西写到硬盘
//        FileOutputStream fout = new FileOutputStream("./src/b.txt");
//
//        while ()
//    }

    public void readfile(String path ) throws IOException {
        try(InputStream fileInputStream = new FileInputStream(path)){

            while (true){
                int n = fileInputStream.read();

                if (n == -1){
                    break;
                }

                System.out.printf("%c", n);
            }

        }
    }

    public void readFile() throws IOException {
        try(InputStream inputStream = new FileInputStream("./a.txt")){
            byte[] buf =  new byte[256];
            while (true){
                int len = inputStream.read(buf);

                if (len == 0){
                    break;
                }

                for (int i = 0; i < len; i++){
                    System.out.printf("%c", buf[i]);
                }
            }
        }
    }

    public void readFile2() throws IOException {
        try (InputStream inputStream = new FileInputStream("./a.txt")){
            try (Scanner fileScanner = new Scanner(inputStream, "UTF-8")){
                while (fileScanner.hasNext()){
                    String s = fileScanner.next();
                    System.out.println(s);
                }
            }

        }
    }

    public void writeFile() throws IOException {
        try (OutputStream os = new FileOutputStream("./b.txt")){
            try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(os)){
                try (PrintWriter writer = new PrintWriter(outputStreamWriter)){
                    writer.println("hello, 快斗");
                    writer.flush();
                }
            }
        }
    }

    public void copyFile() throws IOException {
        try (InputStream inputStream = new FileInputStream("./a.txt")){
            try (OutputStream outputStream = new FileOutputStream("./b.txt")){
                try (Scanner fileScanner = new Scanner(inputStream, "UTF-8")){
                    try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream)){
                        try (PrintWriter writer = new PrintWriter(outputStreamWriter)){
                            while (fileScanner.hasNext()){
                                String s = fileScanner.next();
                                writer.println(s);
                                writer.flush();
                            }
                        }
                    }
                }
            }
        }
    }
    public static void main(String[] args) throws IOException {
        FileCopy test = new FileCopy();
        //test.readFile2();
        //test.writeFile();
        test.copyFile();
    }
}
