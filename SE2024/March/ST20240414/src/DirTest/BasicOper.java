package DirTest;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class BasicOper {
    public static void main(String[] args) throws IOException {
        //检查当前路径下是否有a.txt文件，如果没有就创建，然后分类打印
        File file = new File("./a.txt");

        //怎么判断在不在？
        if (!file.exists()){
            file.createNewFile();
        }

        //遍历查找
        File rootFile = new File("./");
        scan(rootFile);

    }

    public static void scan(File rootFile){
        File[] files = rootFile.listFiles();

        if (files == null || files.length == 0){
            return;
        }

        for (File f : files){
            if (f.isDirectory()){
                System.out.println("文件夹路径: " + f.getAbsolutePath());
                scan(f);
            }else {
                System.out.println("文件路径: " + f.getAbsolutePath());
            }
        }
    }
}
