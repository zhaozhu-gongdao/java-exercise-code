import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Solution {
    public void fileBasicOper() throws IOException {

        File file = new File("./src/a.txt");
        //检查该文件夹中是否存在文件a.txt
        if (!file.exists()){
            file.createNewFile();
        }

        File dir = new File("./");
        String[] filesName = dir.list();
        for (String fileName : filesName){
            File f = new File(dir.getPath() + File.separator + fileName);

            if (f.isDirectory()){
                System.out.println("这是文件夹" + f.getAbsolutePath());
            }else{
                System.out.println("这是文件" + f.getAbsolutePath());
            }
        }
    }

    public void delectCurFile(){
        System.out.println("请输入要删除的目录：");
        Scanner scanner = new Scanner(System.in);
        File rootPath = new File(scanner.next());

        if (!rootPath.isDirectory()){
            System.out.println("输入的目录有误");
        }

        System.out.println("请输入要删除的文件关键名");
        String deleteWord = scanner.next();

        scan(rootPath, deleteWord);
    }

    public void scan(File rootPath, String deleteWord){
        File[] files = rootPath.listFiles();

        if (files == null || files.length == 0){
            return;
        }

        for (File f : files){
            if (f.isDirectory()){
                scan(f, deleteWord);
            }else{
                deletFile(f, deleteWord);
            }
        }
    }

    public void deletFile(File file, String deleteWord){
        Scanner scanner = new Scanner(System.in);
        if (!file.getName().contains(deleteWord)){
            return;
        }

        System.out.println("你要删除的是：" + file.getAbsolutePath() + "是否要删除");
        if (scanner.next().equals("是")){
            file.deleteOnExit();
            System.out.println("删除成功");
        }else{
            System.out.println("取消删除成功");
        }
    }

    public static void main(String[] args) throws IOException {
        Solution test = new Solution();
        //test.delectCurFile();
        test.fileBasicOper();
    }
}
