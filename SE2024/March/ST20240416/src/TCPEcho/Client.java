package TCPEcho;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    Socket socket = null;

    public Client(String serverIP, int serverPort) throws IOException {
        socket = new Socket(serverIP, serverPort);
    }
    public void start() throws IOException {
        System.out.println("客户端启动！");
        Scanner scanner = new Scanner(System.in);

        try (InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {
            while (true){
                System.out.print("------>");

                PrintWriter writer = new PrintWriter(outputStream);
                String request = scanner.next();
                writer.println(request);
                writer.flush();

                Scanner scannerConsole = new Scanner(inputStream);
                String response = scannerConsole.next();
                System.out.println(response);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }finally {
            socket.close();
        }
    }

    public static void main(String[] args) throws IOException {
        Client client = new Client("127.0.0.1", 9090);
        client.start();
    }
}
