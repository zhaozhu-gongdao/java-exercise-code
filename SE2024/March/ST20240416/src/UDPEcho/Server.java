package UDPEcho;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Scanner;

public class Server {
    DatagramSocket socket = null;

    public Server() throws SocketException {
        socket = new DatagramSocket(9090);
    }

    public void start() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("服务器启动");

        while (true){
//            String request = scanner.next();

            byte[] buf = new byte[4096];
            DatagramPacket requestPacket = new DatagramPacket(buf, buf.length);
            socket.receive(requestPacket);
            String request = new String(requestPacket.getData(), 0, requestPacket.getLength());

            String response = process(request);
            DatagramPacket responsePacket = new DatagramPacket(response.getBytes(),response.getBytes().length,
                    requestPacket.getSocketAddress());
            socket.send(requestPacket);
        }

    }

    public String process(String request){
        return request;
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server();
        server.start();
    }
}
