package UDPEcho;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class Client {
    private DatagramSocket socket = null;
    private int serverPort = 9090;
    private String serverIP = "127.0.0.1";

    public Client() throws SocketException {
        socket = new DatagramSocket();
    }

    public void start() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("客户端启动");

        while (true){
            System.out.print("------>");
            String request = scanner.next();
            DatagramPacket requestPacket = new DatagramPacket(request.getBytes(),request.getBytes().length,
                    InetAddress.getByName("127.0.0.1"), serverPort);
            socket.send(requestPacket);

            byte[] buf = new byte[4096];
            DatagramPacket responsePacket = new DatagramPacket(buf, buf.length);
            socket.receive(responsePacket);
            String response = new String(responsePacket.getData(), 0, responsePacket.getLength());

            System.out.println(response);
        }

    }

    public static void main(String[] args) throws IOException {
        Client client = new Client();
        client.start();
    }
}
