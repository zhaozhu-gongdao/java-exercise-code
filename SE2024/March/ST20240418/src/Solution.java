import java.util.Scanner;

//简写单词
public class Solution {
    public void simpleWriteWord(){
        Scanner scanner = new Scanner(System.in);
        StringBuilder ret = new StringBuilder();

        while (scanner.hasNext()){
            String s = scanner.next().toUpperCase();

            ret.append(s.charAt(0));
        }

        System.out.println(ret.toString());
    }

    //除二
    public void countTwo(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        int[] a = new int[n];

        for (int i = 0; i < n; i++){
            a[i] = scanner.nextInt();
        }

        for (int i = 0; i < k; i++){
            //循环找这其中数最大的下标
            int max = Integer.MIN_VALUE;
            for (int j = 0; j < n; j++){
                if (a[j] > max && a[j] % 2 == 0){
                    max = j;
                }
            }

            a[max] /= 2;
        }

        //求和
        int sum = 0;
        for (int i = 0; i < n; i++){
            sum += a[i];
        }

        System.out.println(sum);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x = scanner.nextInt();
        int[] a = new int[n];

        for (int i = 0; i < n; i++){
            a[i] = scanner.nextInt();
        }

        int ret = Integer.MAX_VALUE;
        int prev1 = -1;
        int prev2 = -1;
        //滑动窗口编写
        for (int left = 0, right = 0, sum = 0; right < a.length; right++){
            sum += a[right];
            while (sum >= x){
                sum -= a[left++];
                ret = Math.min(ret, right - left + 1);
                if (ret == (right - left + 1)){
                    //说明左右最小值没有改变
                    prev1 = left;
                    prev2 = right;
                }
            }


        }

        System.out.printf("%d %d", prev1, prev2);
    }
}
