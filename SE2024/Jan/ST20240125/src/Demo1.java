public class Demo1 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {
            // Thread.currentThread 能获取到当前线程的对象(t)
            // Thread对象内部提供了一个标志位(boolean)，线程应该要结束（true)，线程先不必结束（false）
            //lambda 表达式是在构造 t 之前就定义好的. 编译器看到的 lambda 里的 t 就会
            // 认为这是一个还没初始化的对象.所以不能用t.isInterrupted()
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("hello thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();    //打印出了报错信息
                    break;
                }
            }
        });

        t.start();
        Thread.sleep(3000);

        // 把上述的标志位给设置成 true
        t.interrupt();
    }
}

