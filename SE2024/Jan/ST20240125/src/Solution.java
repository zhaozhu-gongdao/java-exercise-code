public class Solution {
    public boolean isUnique(String astr) {
        if (astr.length() > 26){
            return false;
        }

        int bitMap = 0;
        //如何判断这个数要移动几步？
        for (int i = 0; i < astr.length(); i++){
            int x = astr.charAt(i) - 'a';
            if (((bitMap >> x) & 1) == 1){
                return false;
            }else{
                //把……设计为1
                bitMap |= (1<<x);
            }
        }
        return true;
    }
}
