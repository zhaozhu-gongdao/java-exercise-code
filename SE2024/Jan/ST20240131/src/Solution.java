import java.util.Arrays;

public class Solution {
    public static int missingNumber(int[] nums) {
        //位图
//        int bitMap = 0;
//        for (int i = 0; i < nums.length; i++){
//            int x = nums[i];
//            bitMap |= (1<<x);
//        }
//
//        for (int i = 0; i <= nums.length; i++){
//            if (((bitMap >> i) & 1) == 0){
//                return i;
//            }
//        }
//        return -1;

        //排序
//        Arrays.sort(nums);
//        for (int i = 0; i < nums.length; i++){
//            if (nums[i] != i){
//                return i;
//            }
//        }
//
//        return nums.length;

        int ret = 0;
        for (int x : nums){
            ret ^= x;
        }

        for (int i = 0; i <= nums.length; i++){
            ret ^= i;
        }

        return ret;
    }
    public static int getSum(int a, int b) {
        while (b != 0){
            int carry = (a & b) << 1;
            a = a ^ b;

            b = carry;
        }
        return a;
    }
    public static void main(String[] args) {
        //missingNumber(new int[]{3, 0, 1});

        getSum(1,2);
    }
}
