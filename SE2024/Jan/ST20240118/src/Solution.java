

public class Solution {
    public boolean isUnique(String astr) {
        if(astr.length() > 26){
            return false;
        }

        int bitMap = 0;
        for (int i = 0; i < astr.length(); i++){
            int x = astr.charAt(i) - 'a';

            if (((bitMap >> x) & 1) == 1){
                return false;
            }

            bitMap |= 1 << x;
        }

        return true;
    }

    public int missingNumber(int[] nums) {
        int ret = 0;
        for (int x : nums){
            ret ^= x;
        }

        for (int i = 0; i <= nums.length; i++){
            ret ^= i;
        }

        return ret;
    }

    public int getSum(int a, int b) {
        while(b != 0){
            int x = a ^ b; //无进位相加
            int carry = (a & b)<<1; //计算进位
            a = x;
            b = carry;
        }
        return a;
    }


    public int singleNumber(int[] nums) {
        int ret = 0;
        for(int i = 0; i < 32; i++){  // 修改每个bit位
            int sum = 0;
            for(int x : nums){
                if (((x >> i) & 1) == 1){ //统计nums所有数的第i位的和
                    sum++;
                }
            }
            sum %= 3;
            if (sum == 1){
                ret |= 1 << i;
            }
        }
        return ret;
    }

    public int[] missingTwo(int[] nums) {
        // 1. 先把所有的数异或在⼀起
        int tmp = 0;
        for (int x : nums)
            tmp ^= x;
        for (int i = 1; i <= nums.length + 2; i++)
            tmp ^= i;
        // 2. 找出 a，b 两个数⽐特位不同的那⼀位
        int diff = 0;
        while (true) {
            if (((tmp >> diff) & 1) == 1)
                break;
            else
                diff++;
        }
        // 3. 将所有的数按照 diff 位不同，分两类异或
        int[] ret = new int[2];
        for (int x : nums)
            if (((x >> diff) & 1) == 1)
                ret[1] ^= x;
            else
                ret[0] ^= x;
        for (int i = 1; i <= nums.length + 2; i++)
            if (((i >> diff) & 1) == 1)
                ret[1] ^= i;
            else
                ret[0] ^= i;
        return ret;
    }
}
