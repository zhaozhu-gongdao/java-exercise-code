package thread;


class myThread extends Thread{
    @Override
    public void run() {
        while(true){
            System.out.println("hello thread");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
public class Demo1 {
    public static void main(String[] args) throws InterruptedException {
        myThread myThread = new myThread();
        myThread.start();
        while (true){
            System.out.println("hello main");
            Thread.sleep(1000);
        }
    }
}
