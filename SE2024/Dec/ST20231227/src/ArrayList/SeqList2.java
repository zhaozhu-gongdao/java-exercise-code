package ArrayList;

import java.util.Arrays;

public class SeqList2 {
    private int[] elem = new int[DEFAULT_CAPACITY];
    private int useSize;
    private static final int DEFAULT_CAPACITY = 5;


    // 打印顺序表，注意：该方法并不是顺序表中的方法，为了方便看测试结果给出的
    public void display(){
        for (int i = 0; i < this.useSize; i++) {
            System.out.print(this.elem[i] + " ");
        }
        System.out.println();
    }

    // 获取 pos 位置的元素
    public int get(int pos) {
        if (!checkPos(pos)){
            //throw new PosException("pos不在合法范围中");
        }
        return this.elem[pos];
    }

    private boolean checkPos(int pos){
        if (pos <= useSize && pos > 0){
            return true;
        }else{
            return false;
        }
    }


    // 新增元素,默认在数据最后新增
    public void add(int data){
        if (isFull()){
            elem = Arrays.copyOf(elem, 2 * elem.length);
        }else{
            elem[useSize] = data;
            useSize++;
        }
    }
    public boolean isFull(){
        if (this.useSize == elem.length){
            return true;
        }else{
            return false;
        }
    }


    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        for (int i = 0; i < this.useSize; i++) {
            if (this.elem[i] == toFind){
                return true;
            }
        }
        return false;
    }

    // 查找某个元素对应的下标
    public int indexOf(int toFind) {
        for (int i = 0; i < this.useSize; i++) {
            if (this.elem[i] == toFind){
                return i;
            }
        }
        return -1;
    }
    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        if(pos < 0 || pos > this.useSize) {
            //throw new PosOutBoundsException("add 元素的时候，pos位置不合法！");
        }

        if(isFull()) {
            //resize();
        }

        //挪数据
        for (int i = this.useSize-1; i >= pos ; i--) {
            this.elem[i+1] = this.elem[i];
        }

        //存数据
        this.elem[pos] = data;
        this.useSize++;
    }


    // 获取顺序表长度
    public int size() {
        return useSize;
    }

    // 清空顺序表
    public void clear() {
        //这是引用类型的做法
        /*for (int i = 0; i < useSize; i++) {
            this.elem[i] = null;
        }*/

        useSize = 0;
    }

    //删除第一次出现的关键字key
    public void remove(int toRemove) {
        if(isEmpty()) {
            return;
        }
        int index = indexOf(toRemove);
        if(index == -1) {
            return;//没有你要删除的数字
        }
        for (int i = index; i < useSize-1; i++) {
            this.elem[i] = this.elem[i+1];
        }
        useSize--;
        //elem[us] = null;
    }
    public boolean isEmpty() {
        return useSize == 0;
    }


}

