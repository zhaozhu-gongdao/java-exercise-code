package ArrayList;

public class SeqList {
    private int[] array;
    private int size;
    // 默认构造方法
    SeqList(){
        this.array = null;
    }
    // 将顺序表的底层容量设置为initcapacity
    SeqList(int initcapacity){
        this.array = new int[initcapacity];
    }
    // 新增元素,默认在数组最后新增
    public void add(int data) {
        //没有判断有没有超过
        array[size] = data;
        size++;
    }
    // 在 pos 位置新增元素
    //数组中的下标搞不清
    public void add(int pos, int data) {
        //pos后面的值后移
        for (int i = size; i >= pos; i--) {
            array[i] = array[i - 1];
        }

        array[pos] = data;
        size++;

    }
    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        int flag = 0;    //0代表没有找到，1代表找到了
        for (int x : array){
            if (x == toFind){
                flag = 1;
                break;
            }
        }

        return flag == 0?false: true;
    }
    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == toFind){
                return i;
            }
        }
        return -1;  //-1表示没有找到这个数值
    }
    // 获取 pos 位置的元素
    public int get(int pos) {
        return array[pos];
    }
    // 给 pos 位置的元素设为 value
    public void set(int pos, int value) {
        array[pos] = value;
    }
    //删除第一次出现的关键字key
    public void remove(int toRemove) {
        //找到那个数对应的下标
        int re = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == toRemove){
                re = i;
                break;
            }
        }

        //前移一位
        for (int i = re; i < array.length; i++) {
            array[i] = array[i + 1];
        }
        size--;

    }
    // 获取顺序表长度
    public int size() {
        return size;
    }
    // 清空顺序表
    public void clear() {
        for (int i = 0; i < array.length; i++) {
            array[i] = 0;
        }

        size = 0;
    }
    // 打印顺序表，注意：该方法并不是顺序表中的方法，为了方便看测试结果给出的
    public void display() {
        for (int x : array){
            System.out.print(x + " ");
        }
    }
}
