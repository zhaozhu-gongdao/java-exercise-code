import LinkedList.SingleLinkedList;

public class Main {
    public static void main(String[] args) {
//        SeqList seqList = new SeqList();
//        seqList.add(0,1);
//        seqList.add(1,2);
//        seqList.add(2,4);
//        seqList.add(2,3);
//
//        seqList.display();
        SingleLinkedList test = new SingleLinkedList();
        test.createLinkedList();
        test.addFirst(0);
        test.display();
        test.addIndex(2,8);
        System.out.println();
        test.display();
        System.out.println(test.contains(2));
        System.out.println(test.contains(7));
        test.addLast(7);
        test.display();
        test.remove(2);
        System.out.println();
        test.display();
        test.remove(2);
        System.out.println();
        test.display();
        test.remove(7);
        System.out.println();
        test.display();
        System.out.println();
        System.out.println(test.size());
        test.addLast(2);
        test.addLast(2);
        test.addLast(2);
        test.addLast(2);
        test.display();
        test.removeAllKey(2);
        System.out.println();
        test.display();

    }
}