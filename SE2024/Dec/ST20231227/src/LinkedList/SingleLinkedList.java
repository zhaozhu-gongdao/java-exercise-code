package LinkedList;

public class SingleLinkedList {
    private ListNode head;
    class ListNode{
        public int val;
        public ListNode next;

        public ListNode(int val){
            this.val = val;
        }
    }
    public void createLinkedList(){
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);
        head = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        //node5.next = null;
    }
    //头插法
    public void addFirst(int data){
        ListNode node = new ListNode(data);
        node.next = head;
        head = node;
    }
    //尾插法
    public void addLast(int data){
        ListNode node = new ListNode(data);
        ListNode cur = head;

        if (cur == null){
            head = node;
            return;
        }

        while(cur.next != null){
            cur = cur.next;
        }

        cur.next = node;
    }
    //任意位置插入,第一个数据节点为0号下标
    public void addIndex(int index,int data){
        ListNode node = new ListNode(data);
        ListNode cur = head;
        while(index != 0){
            cur = cur.next;
            index--;
        }

        node.next = cur.next;
        cur.next = node;

    }
    //查找是否包含关键字key是否在单链表当中
    public boolean contains(int key){
        ListNode cur = head;
        while(cur.next != null){
            if (cur.val == key){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }
    //删除第一次出现关键字为key的节点
    public void remove(int key){
        ListNode cur = head;

        if (head == null){
            return;
        }

        ListNode delPrev = searchPrev(key);
        if (delPrev != null){
            //删除操作
            delPrev.next = delPrev.next.next;
        }else{
            System.out.println("没有找到这个数");
        }

    }

    public ListNode searchPrev(int key){
        ListNode cur = head;
        while(cur.next != null){
            if (cur.next.val == key){
                return cur;
            }else{
                cur = cur.next;
            }
        }
        return null;
    }

    //删除所有值为key的节点
    public void removeAllKey(int key){

        if (head == null){
            return;
        }

//        while (cur != null){
//            if (cur.next.val == key){
//                cur.next = cur.next.next;
//            }else{
//                cur = cur.next;
//            }
//
//        }
        ListNode cur = head.next;
        ListNode prev = head;
        while(cur != null){
            if (cur.val == key){
                prev.next = cur.next;
                cur = cur.next;
            }else{
                prev = prev.next;
                cur = cur.next;
            }
        }

        if (head.val == key){
            head = head.next;
        }

    }
    //得到单链表的长度
    public int size(){
        ListNode cur = head;
        int res = 0;
        while(cur != null){
            res++;
            cur = cur.next;
        }
        return res;
    }
    public void clear() {
    }
    public void display() {
        ListNode node = head;
        while(node != null){
            System.out.print(node.val + "  ");
            node = node.next;
        }
    }
}
