public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.lengthOfLongestSubstring("tmmzuxt");
    }
}


class Solution {
    //无重复字符的最长子串
    public int lengthOfLongestSubstring(String s) {
        char[] ch = s.toCharArray();
        int[] hash = new int[128];
        int left = 0;
        int ret = 0;

        for (int right = 0; right < s.length(); right++){
            hash[ch[right]]++;

            while (hash[ch[right]] >1){
                hash[ch[left]]--;
                left++;
            }

            ret = Math.max(ret, right - left + 1);
        }

        return ret;
    }


    //长度最小的子数组
    public int minSubArrayLen(int target, int[] nums) {
        int ret = Integer.MAX_VALUE;
        int sum = 0;

        for (int left = 0, right = 0; right < nums.length; right++){
            sum += nums[right];

            while (sum >= target){
                ret = Math.min(ret, right - left + 1);
                sum -= nums[left++];
            }

        }

        return ret == Integer.MAX_VALUE?0:ret;
    }

    //查找总价格为目标值的两个商品
    public int[] twoSum(int[] price, int target) {
        int left = 0;
        int right = price.length - 1;
        int sum = 0;

        while (left < right){
            sum = price[left] + price[right];
            if (sum > target){
                right--;
            }else if (sum < target){
                left++;
            }else{
                break;
            }
        }
        return new int[]{price[left], price[right]};
    }
}