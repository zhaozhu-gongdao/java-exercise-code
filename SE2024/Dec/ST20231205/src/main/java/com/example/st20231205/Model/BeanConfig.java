package com.example.st20231205.Model;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {
    @Bean
    public UserInfo userInfo1(){
        UserInfo user = new UserInfo();
        user.setName("zhangsan");
        user.setAge(18);
        return user;
    }

    @Bean
    public UserInfo userInfo2(){
        UserInfo user = new UserInfo();
        user.setName("lisi");
        user.setAge(22);
        return user;
    }
}

