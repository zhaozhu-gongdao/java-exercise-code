package com.example.st20231205;

import com.example.st20231205.Controller.UserController;
import com.example.st20231205.Model.UserInfo;
import com.example.st20231205.Service.UserService;

import org.apache.catalina.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.example.st20231205.Model")
public class St20231205Application {

    public static void main(String[] args) {
        //获取Spring上下文对象
        ApplicationContext context = SpringApplication.run(St20231205Application.class, args);
        //从上下文中获取对象
//        UserController userController1 = context.getBean(UserController.class);
//        UserController userController2 = (UserController) context.getBean("userController");
//        UserController userController3 = context.getBean("userController", UserController.class);
//
//        System.out.println(userController1);
//        System.out.println(userController2);
//        System.out.println(userController3);

//        UserService service = context.getBean(UserService.class);
//        service.sayHi();

        UserInfo userInfo1 = (UserInfo) context.getBean("userInfo1");
        UserInfo userInfo2 = (UserInfo) context.getBean("userInfo2");

        System.out.println(userInfo1);
        System.out.println(userInfo2);
    }

}
