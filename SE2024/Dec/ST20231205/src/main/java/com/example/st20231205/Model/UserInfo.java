package com.example.st20231205.Model;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;


@Data
public class UserInfo {
    private String name;
    private Integer age;
}
