public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
}


class Solution {
    public int longestOnes(int[] nums, int k) {
        int left = 0, right = 0;
        int ret = 0, zero = 0;

        for (;right < nums.length; right++){
            if (nums[right] == 0){
                zero++;
            }


            while (zero > k){
                if (nums[left++] == 0){
                    zero--;
                }
            }

            ret = Math.max(ret, right - left + 1);
        }

        return ret;
    }
}