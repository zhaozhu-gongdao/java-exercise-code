
/*class OuterClass{
    public int a = 1;
    public static int b = 2;
    public static int c = 0;

    class InnerClass{
        int c = 3;                 //在实例内部类当中，不能定义静态成员变量
        //public static int d = 4;     会报错，因为内部类的使用需要依赖对象
        public static final int e = 5;    //不会报错
        //原理：final修饰表示这个是个常量，常量e在编译时就知道是个确定的值5，如果是static修饰的，要到运行的时候，才知道值是多少


        public void state(){
            System.out.println("这是实例内部类");

            System.out.println(a);
            System.out.println(b);
            System.out.println(c);     //优先访问这个类中自己的成员
            System.out.println(OuterClass.this.c);    //打印外部类中的同名变量
            //结论：实例内部类，是存在于外部类的this中的

            //System.out.println(d);     无法正常打印
            System.out.println(e);

        }
    }
}
public class Main {
    public static void main(String[] args) {
        OuterClass.InnerClass innerClass = new OuterClass().new InnerClass();

        OuterClass outerClass = new OuterClass();
        OuterClass.InnerClass innerClass2 = outerClass.new InnerClass();

        innerClass2.state();
        //innerClass.state();         两者的效果一样
    }
}*/



/*
class OuterClass{
    public int date1 = 1;
    public static int date2 = 2;

    static class InnerClass{
        public int date3 = 3;
        public static int date4 = 4;

        public void state(){
            //System.out.println(date1);     会报错，静态内部类当中无法访问外部的非静态成员

            //如何访问静态内部类当中，如何访问外部的非静态成员
            OuterClass outerClass = new OuterClass();
            System.out.println(outerClass.date1);

            System.out.println(date2);
            System.out.println(date3);
            System.out.println(date4);
        }
    }
}

public class Main{
    public static void main(String[] args) {
        OuterClass.InnerClass innerClass = new OuterClass.InnerClass();
        innerClass.state();
    }
}*/



class Student implements Comparable<Student>{
    @Override
    public int compareTo(Student o) {
        return 0;
    }
}
interface Shape {
    void draw();
}
public class Main {
    public static void main(String[] args) {
        Comparable<Student> comparable = new Student();

        new Comparable<Student>(){    //创建了一个类，这个类实现了Comparable<Student>接口
            @Override               //又重写了compareTo方法，但是不知道这个对象名是谁
            public int compareTo(Student o) {
                return 0;
            }
        };


        int a = 10;
        new Shape() {      //创建了一个类，这个类实现了shape接口
            @Override
            public void draw() {
                //a = 99;         在匿名内部类当中，访问的变量是不可以改变的
                System.out.println("矩形！" + a);
            }
        }.draw();    //调用方法
    }
}
