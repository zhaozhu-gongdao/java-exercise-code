package Demo2;

public class Rectangle {
    private int width;
    private int length;

    public Rectangle(int width, int length){
        this.width = width;
        this.length = length;
    }

    public int getPerimeter(){
        return 2 * (this.length + this.width);
    }

    public int getArea(){
        return this.length * this.width;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", length=" + length +
                '}';
    }
}
