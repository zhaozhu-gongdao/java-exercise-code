package Demo2;

public class Circle {
    private int radius;

    public Circle(int radius){
        this.radius = radius;
    }

    public int getPerimeter(){
        int ret = (int)(2 * Math.PI * radius);
        return ret;
    }

    public int getArea(){
        int ret = (int)(radius * radius * Math.PI);
        return ret;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
}
