package Demo2;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Rectangle[] rectangles = new Rectangle[2];
        Circle[] circles = new Circle[2];
        int allPerimeter = 0;
        int allArea = 0;
        for (int i = 0; i < 2; i++) {
            int length = scanner.nextInt();
            int width = scanner.nextInt();
            Rectangle rectangle = new Rectangle(width,length);
            rectangles[i] = rectangle;
            allPerimeter += rectangles[i].getPerimeter();
            allArea += rectangles[i].getArea();
        }

        for (int i = 0; i < 2; i++) {
            int radius = scanner.nextInt();
            Circle circle = new Circle(radius);
            circles[i] = circle;
            allPerimeter += circles[i].getPerimeter();
            allArea += circles[i].getArea();
        }


        System.out.println(allPerimeter);
        System.out.println(allArea);


        System.out.println(Arrays.deepToString(rectangles));
        System.out.println(Arrays.deepToString(circles));
    }
}
