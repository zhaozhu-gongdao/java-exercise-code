package ListTest;

public class FindKIndex {
    public class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode FindKthToTail(ListNode head,int k) {
        ListNode fast = head;
        ListNode slow = head;

        if (head == null || k <= 0){
            return null;
        }
        for(int i = 0; i < k - 1; i++){
            fast = fast.next;
            if (fast == null){
                return null;
            }
        }

        while (fast.next != null){
            slow = slow.next;
            fast = fast.next;
        }
        return slow;
    }
}
