package Demo1;

public class Person{
    private String name;
    private boolean gender;
    private int age;
    private int id = num;
    private static int num;

    {
        num++;
        System.out.println("This is initialization block, id is" + id);
    }
    public Person(){
        System.out.println("This is constructor");
        System.out.println(this.name + "," + this.age + ","
                + this.gender + "," + this.age);;

    }

    static{
        System.out.println("This is static initialization block");
    }

    public Person(String name, int age, boolean gender){
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public String toString(){
        return "Person [name=" + this.name +
                ", age=" + this.age + ", gender=" +
                this.gender + ", id=" + id +"]";
    }
}