public class PosException extends RuntimeException{
    public PosException(String message) {
        super(message);
    }
}
