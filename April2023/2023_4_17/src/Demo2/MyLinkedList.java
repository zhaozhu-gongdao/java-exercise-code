package Demo2;
import java.util.List;

public class MyLinkedList {
        static class ListNode {
            public int val;
            public ListNode prev;
            public ListNode next;

            public ListNode(int val) {
                this.val = val;
            }
        }
        public ListNode head;
        public ListNode last;
        //得到链表的长度   和双向 没有关系 ！！！
        public int size(){
            int len = 0;
            ListNode cur = head;
            while (cur != null) {
                cur = cur.next;
                len++;
            }
            return len;
        }

        public void display(){
            ListNode cur = head;
            while (cur != null) {
                System.out.print(cur.val+" ");
                cur = cur.next;
            }
            System.out.println();
        }

        //查找是否包含关键字key是否在链表当中
        public boolean contains(int key){
            ListNode cur = head;
            while (cur != null) {
                if(cur.val == key) {
                    return true;
                }
                cur = cur.next;
            }
            return false;
        }

        //头插法
        public void addFirst(int data){
            ListNode node = new ListNode(data);
            if(head == null) {
                head = node;
                last = node;         //第一种写法
                return;
            }
            node.next = head;
            head.prev = node;
            head = node;
        /*if(head == null) {
            head = node;
        }else {                  //第二种写法
            node.next = head;
            head.prev = node;
            head = node;
        }*/
        }

        //尾插法
        public void addLast(int data){
            ListNode node = new ListNode(data);
            if(head == null) {
                head = node;
                last = node;
            }else {
                last.next = node;
                node.prev = last;
                last = node;//last = last.next;
            }
        }

        //任意位置插入,第一个数据节点为0号下标
        public void addIndex(int index,int data){
            int size = size();
            if(index < 0 || index > size) {
                //throw new IndexOutOfBounds("双向链表index不合法！");
            }
            if(index == 0) {
                addFirst(data);
                return;
            }
            if(index == size) {
                addLast(data);
                return;
            }
            ListNode cur = head;
            while (index != 0) {
                cur = cur.next;
                index--;
            }
            ListNode node = new ListNode(data);
            node.next = cur;
            cur.prev.next = node;
            node.prev = cur.prev;
            cur.prev = node;
        }



        //删除第一次出现关键字为key的节点
        public void remove(int key){
            ListNode cur = head;
            while (cur != null) {
                if(cur.val == key) {
                    //开始删
                    if(cur == head) {
                        //删除头节点
                        head = head.next;
                        //只要1个节点的时候
                        if(head != null) {
                            head.prev = null;
                        }else {
                            last = null;
                        }
                    }else {
                        cur.prev.next = cur.next;
                        if(cur.next != null) {
                            //cur.prev.next = cur.next;
                            cur.next.prev = cur.prev;
                        }else {
                            //cur.prev.next = cur.next;
                            last = last.prev;
                        }
                    }
                    return;
                }else {
                    cur = cur.next;
                }
            }
        }




        //删除所有值为key的节点
        public void removeAllKey(int key){
            ListNode cur = head;
            while (cur != null) {
                if(cur.val == key) {
                    //开始删
                    if (cur == head) {
                        //删除头节点
                        head = head.next;
                        //只要1个节点的时候
                        if (head != null) {
                            head.prev = null;
                        }else {
                            last = null;
                        }
                    } else {
                        cur.prev.next = cur.next;
                        if (cur.next != null) {
                            //cur.prev.next = cur.next;
                            cur.next.prev = cur.prev;
                        } else {
                            //cur.prev.next = cur.next;
                            last = last.prev;
                        }
                    }
                }

                cur = cur.next;
            }
        }
        public void clear(){
            ListNode cur = head;
            while (cur != null) {
                ListNode curNext = cur.next;
                cur.prev = null;
                cur.next = null;
                //cur.val = null;
                cur = curNext;
            }
            head = null;
            last = null;
        }
}
