
/*class Person implements Cloneable{
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();     //表示会有个编译时的CloneNotSupportedException异常，
    }                      //但是这边并不想处理，于是只是用throws声明一下，异常的处理，交给方法的调用者
}
public class Main {
    public static void main(String[] args) throws CloneNotSupportedException{
        Person person1 = new Person();       //这边还是不想处理，所以再声明一下，这个异常最终会交给JVM处理，而一旦发生问题
        Person person2 = (Person)person1.clone();   //JVM就会异常终止

    }
}*/


/*import java.util.InputMismatchException;
import java.util.Scanner;

public class Main{
    *//*public static void main(String[] args) {
        System.out.println("before");
        try {
            System.out.println(10/0);
            int[] array = null;
            System.out.println(array[2]);
            System.out.println("fdsfsafdsdsafsafsa");
        }*//**//*catch (ArithmeticException | NullPointerException e) {
            System.out.println();        //不建议写在一起，因为这样就看不出来是什么异常了，可读性差
        }*//**//*catch (ArithmeticException e) {
            e.printStackTrace();
            System.out.println("捕获了ArithmeticException");
        }catch (NullPointerException e) {   //如果捕获了这个异常，就执行里面的语句
            e.printStackTrace();
            System.out.println("我捕获了空指针异常！");
        }
    }*//*

    *//*public static void main(String[] args) {
        System.out.println("before");
        try {
            System.out.println(10/0);
        }catch (ArithmeticException e) {   //其实我们这里可以放Exception e，但那样就看不出来是因为什么异常了，不推荐
            System.out.println("捕获了ArithmeticException");
        }catch (Exception e) {
            System.out.println("我捕获到了：Exception！,这里一般处理异常");
        }
        System.out.println("after");
    }*//*


    public static int getData(){
        Scanner sc = null;
        try{
            sc = new Scanner(System.in);
            int data = sc.nextInt();
            return data;
        }catch (InputMismatchException e){
            e.printStackTrace();
        }finally {
            System.out.println("finally中代码");
        }
        System.out.println("try-catch-finally之后代码");
        if(null != sc){
            sc.close();
        }
        return 0;
    }
    public static void main(String[] args) {
        int data = getData();
        System.out.println(data);
    }
}*/




class UserNameException  extends RuntimeException{
    public UserNameException() {
    }

    public UserNameException(String message) {
        super(message);
    }
}

public class Main {
    private String userName = "admin";
    private String password = "123456";
    public  void loginInfo(String userName, String password) {
        if (!this.userName.equals(userName)) {
            throw new UserNameException(userName+" 用户名错误！");
        }
        System.out.println("登陆成功");
    }
    public static void main(String[] args) {
        Main logIn = new Main();
        logIn.loginInfo("admin12", "123456111");
    }
}