package Demo1;
/*import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main1(String[] args) {
        final int HUMANNUM=10;
        String persons[]=new String[HUMANNUM];
        Scanner in=new Scanner(System.in);
        for(int i=0;i<persons.length;i++)
            persons[i]=in.nextLine();
        int result=numofHan(persons);
        System.out.println(result);



    }

    public static int numofHan(String data[]){
        int count = 0;
        for (int i = 0; i < data.length; i++){
            String[] newDate = data[i].split(",");
            if (newDate[3].equals("汉族")){
                count++;
            }
        }

        return count;
    }

    public static void main(String[] args) {
        int[][] arr = {{1,2,3},{4,5,6}};
        System.out.println(Arrays.deepToString(arr));
    }

}*/


/*
import java.util.Objects;

class Person2 {
    public String name;
    public int age;
    public Person2(String name, int age) {
        this.name = name;
        this.age = age;
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
public class Main {
    public static void main(String[] args) {
        Person2 per1 = new Person2("gaobo", 20) ;
        Person2 per2 = new Person2("gaobo", 20) ;
        System.out.println(per1.hashCode());
        System.out.println(per2.hashCode());     //执行结果一样
    }
}
*/

class Father{

    public void eat(){
        System.out.println("正在吃饭");
    }
}

class Son extends Father{
    public void eat(){    //方法的重写
        System.out.println("正在吃黄焖鸡米饭");
    }
}
public class Main {
    public static void main(String[] args) {
        Father father = new Son();
        father.eat();
    }
}





