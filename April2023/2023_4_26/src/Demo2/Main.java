package Demo2;

import java.util.*;

public class Main{
    public static void main(String[] args){
        List<String> wordList = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);

        int notReNum = 0;
        while (true){
            String word = scanner.nextLine();

            //退出的条件
            if (word.equals("!!!!!")){
                break;
            }

            //先存进去每一行的值,存的时候看看有没有重复

            String[] words = word.split("\\s+");


            for (int i = 0; i < words.length; i++) {
                int searchIndex = wordList.indexOf(words[i]);
                if (searchIndex == -1){
                    wordList.add(words[i]);
                    notReNum++;
                }
            }


        }

        System.out.println(notReNum);

        //打印，看不同的单词有没有超过10个
        if (notReNum < 10){
            System.out.println(wordList);
            return;
        }else{
            for (int i = 0; i < wordList.size() - 1; i++) {
                for (int j = i + 1; j < wordList.size(); j++) {
                    firstWordComp firstWordComp = new firstWordComp();
                    int ret = firstWordComp.compare(wordList.get(i), wordList.get(j));
                    if (ret > 0){
                        String tmp = wordList.get(i);
                        wordList.set(i, wordList.get(j));
                        wordList.set(j, tmp);
                    }
                }
            }
        }

        //在单词数字大于10的情况下，打印
        for (int i = 0; i < 10; i++) {
            System.out.println(wordList.get(i));
        }
    }
}

class firstWordComp implements Comparator<String>{
    @Override
    public int compare(String o1, String o2) {
        return o1.compareTo(o2);
    }
}
