package Demo3;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;



public class Main {
    public static void main(String[] args) {
        List<String> wordList = new ArrayList<>();
        List<Integer> indexList = new ArrayList<>();

        //输入一行行的的单词，输入到!!!截止，空格分隔，注意空行
        Scanner scanner = new Scanner(System.in);
        int reNum = 0;

        while (true){
            String words = scanner.nextLine();
            if (words.equals("!!!!!")){
                break;
            }

            //!.,:*?
            words = words.replaceAll("!"," ");
            words = words.replaceAll("\\."," ");
            words = words.replaceAll(":"," ");
            words = words.replaceAll("\\*"," ");
            words = words.replaceAll("\\?"," ");

            String[] wordArr = words.split("\\s+");
            if (wordArr.length == 0 || wordArr[0].equals("")){
                continue;
            }

            for (int i = 0; i < wordArr.length; i++) {
                if (wordArr[i].equals("!!!!!")){
                    break;
                }
                int searchIndex = wordList.indexOf(wordArr[i]);
                if (searchIndex == -1){  //如果没查到，说明就没有这个单词，对应的次数+1
                    wordList.add(wordArr[i]);
                    indexList.add(1);
                    reNum++;

                } else {    //如果能查到，说明之前有这个单词，那就在查到的位置上加上
                    indexList.set(searchIndex, indexList.get(searchIndex) + 1);
                }
            }


        }

        System.out.println(reNum);
        //按照次序打印
        //排序
        for (int i = 0; i < wordList.size() - 1; i++) {
            for (int j = i + 1; j < wordList.size(); j++) {
                //按照次序排
                if (indexList.get(i) < indexList.get(j)){
                    int tmp = indexList.get(i);
                    indexList.set(i, indexList.get(j));
                    indexList.set(j, tmp);

                    String tmp2 = wordList.get(i);
                    wordList.set(i, wordList.get(j));
                    wordList.set(j, tmp2);
                }

                //按照字母顺序排
                if (indexList.get(i) == indexList.get(j)){
                    if (wordList.get(i).compareToIgnoreCase(wordList.get(j)) > 0){
                        int tmp = indexList.get(i);
                        indexList.set(i, indexList.get(j));
                        indexList.set(j, tmp);

                        String tmp2 = wordList.get(i);
                        wordList.set(i, wordList.get(j));
                        wordList.set(j, tmp2);
                    }
                }
            }
        }

        //打印
        for (int i = 0; i < 10; i++) {
            System.out.println(wordList.get(i) + "=" + indexList.get(i));
        }
    }
}
