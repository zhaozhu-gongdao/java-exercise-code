package Demo4;

/*public class Main {
    public static void main(String[] args) {
        System.out.println("Hello Java!");
        System.out.println("Programming is fun!");
    }
}*/


/*
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()){
            int n = scanner.nextInt();
            int begin = scanner.nextInt();
            int end = scanner.nextInt();

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < n - 1; i++) {
                stringBuilder.append(i);
            }

            String str = stringBuilder.substring(begin, end);
            System.out.println(str);
        }

    }
}*/


/*public class Main {
    public static void main(String[] args) {
        *//********* begin *********//*
        // 声明并实例化一Person对象p
        Person person = new Person("张三",18);
        // 给p中的属性赋值

        // 调用Person类中的talk()方法
        person.talk();
        *//********* end *********//*
    }
}

// 在这里定义Person类
class Person {
    *//********* begin *********//*
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void talk(){
        System.out.println("我是：" + this.name + ", 今年: " + this.age + "岁");
    }

    */


import java.util.*;

/********* end *********//*
}*/


/*
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        List <Person> personList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            String name = scanner.next();
            int age = scanner.nextInt();
            boolean gender = scanner.nextBoolean();
            //有参新建对象，n个

            Person person = new Person(name,age,gender);
            personList.add(person);
        }

        //逆序输出
        for (int i = n - 1; i >= 0 ; i--) {
            System.out.println(personList.get(i));
        }

        Person person = new Person();
        System.out.println(person);

    }
}

class Person{
    private String name;
    private int age;
    private boolean gender;
    private int id;

    public Person(String name, int age, boolean gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public Person(){
        System.out.println("This is constructor");
        System.out.println(this.name + "," + this.age + "," + this.gender + "," +this.id);
    }

    @Override
    public String toString() {
        return "Person [" +
                "name=" + name +
                ", age=" + age +
                ", gender=" + gender +
                ", id=" + id +
                ']';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}*/



/*
abstract class Shape{
    protected static double PI = 3.14;

    public abstract double getPerimeter();
    public abstract double getArea();
}

class Rectangele extends Shape{
    private int width;
    private int length;

    @Override
    public double getPerimeter() {
        return 2 * (width + length);
    }

    @Override
    public double getArea() {
        return width * length;
    }

    public Rectangele(int width, int length) {
        this.width = width;
        this.length = length;
    }

    @Override
    public String toString() {
        return "Rectangele [" +
                "width=" + width +
                ", length=" + length +
                ']';
    }
}

class Circle extends Shape{
    private int radius;

    @Override
    public double getPerimeter() {
        return 2 * Shape.PI * radius;
    }

    @Override
    public double getArea() {
        return Shape.PI * radius * radius;
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle [" +
                "radius=" + radius +
                ']';
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<Shape> shapeList = new ArrayList<>();

        double sumPer = 0;
        double sumArea = 0;
        for (int i = 0; i < n; i++) {
            String shaprOper = scanner.next();

            if (shaprOper.equals("rect")){
                int width = scanner.nextInt();
                int length = scanner.nextInt();
                Rectangele rect = new Rectangele(width, length);
                sumArea += rect.getArea();
                sumPer += rect.getPerimeter();

                shapeList.add(rect);
            }else {
                int radius = scanner.nextInt();
                Circle cir = new Circle(radius);
                sumArea += cir.getArea();
                sumPer += cir.getPerimeter();

                shapeList.add(cir);
            }
        }

        System.out.println(sumPer);
        System.out.println(sumArea);

        System.out.println(shapeList);

        //输出类型
        for (int i = 0; i < n; i++) {
            System.out.print(shapeList.get(i).getClass());
            System.out.println(",class Shape");
        }
    }
*/
//}


/*
class PersonSortable2{
    public String name;
    public int age;

    public PersonSortable2(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return this.name + "-" + this.age;
    }
}

class NameComparactor implements Comparator<String>{
    @Override
    public int compare(String o1, String o2) {
        return o1.compareTo(o2);
    }
}

class AgeComparactor implements Comparator<Integer>{
    @Override
    public int compare(Integer o1, Integer o2) {
        return o1 - o2;
    }
}

public class Main{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        PersonSortable2[] persons = new PersonSortable2[n];

        for (int i = 0; i < n; i++) {
            String name = scanner.next();
            int age = scanner.nextInt();
            PersonSortable2 person = new PersonSortable2(name, age);
            persons[i] = person;
        }

        System.out.println("NameComparator:sort");
        //按照名字排序
        for (int i = 0; i < persons.length - 1; i++) {
            for (int j = i + 1; j < persons.length; j++) {
                NameComparactor nameComparactor = new NameComparactor();

                //按照名字排
                if (nameComparactor.compare(persons[i].name, persons[j].name) > 0){
                    PersonSortable2 tmp = persons[i];
                    persons[i] = persons[j];
                    persons[j] = tmp;
                }

                //按照年龄排
                AgeComparactor ageComparactor = new AgeComparactor();
                if (nameComparactor.compare(persons[i].name, persons[j].name) == 0){
                    if (ageComparactor.compare(persons[i].age, persons[j].age) < 0){
                        PersonSortable2 tmp = persons[i];
                        persons[i] = persons[j];
                        persons[j] = tmp;
                    }
                }
            }
        }

        //打印
        for (int i = 0; i < persons.length; i++) {
            System.out.println(persons[i]);
        }

        System.out.println("AgeComparator:sort");
        //按照年龄排序
        for (int i = 0; i < persons.length - 1; i++) {
            for (int j = i + 1; j < persons.length; j++) {
                AgeComparactor ageComparactor = new AgeComparactor();

                if (ageComparactor.compare(persons[i].age, persons[j].age) > 0){
                    PersonSortable2 tmp = persons[i];
                    persons[i] = persons[j];
                    persons[j] = tmp;
                }

                if (ageComparactor.compare(persons[i].age, persons[j].age) == 0){
                    NameComparactor nameComparactor = new NameComparactor();

                    //按照名字排
                    if (nameComparactor.compare(persons[i].name, persons[j].name) > 0){
                        PersonSortable2 tmp = persons[i];
                        persons[i] = persons[j];
                        persons[j] = tmp;
                    }
                }
            }
        }
        //打印
        for (int i = 0; i < persons.length; i++) {
            System.out.println(persons[i]);
        }

        System.out.println(Arrays.toString(NameComparactor.class.getInterfaces()));
        System.out.println(Arrays.toString(AgeComparactor.class.getInterfaces()));

        

    }
}*/


/*
class IllegalScoreException extends RuntimeException{
    public IllegalScoreException(String message) {
        super(message);
    }
}

class IllegalNameException extends RuntimeException{
    public IllegalNameException(String message) {
        super(message);
    }
}

class Studen {
    private String name;
    private int score;

    public Studen(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Studen [" +
                "name='" + name + '\'' +
                ", score=" + score +
                ']';
    }

    public int addScore(int score){
        if (score < 0 || score > 100){
            throw new IllegalScoreException("score out of range, score=" + this.score);
        }

        return 1;
    }

    public int setName(){
        if (this.name.charAt(0) >= '0' && this.name.charAt(0) <= '9'){
            throw new IllegalNameException();
        }

        return 1;
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List <Studen> studenList = new ArrayList<>();

        while (true){
            String oper = scanner.next();
            if (oper.equals("new")){
                int score = scanner.nextInt();
                Studen stu = new Studen(scanner.next(), score);
                studenList.add(stu);


                try{
                    stu.setName();
                    stu.addScore(score);
                }catch(Exception e){
                    System.out.println(e);
                }

            }else{
                break;
            }
        }
    }
}
*/


/*
class Person{
    String name;
    boolean gender;
    String birthdate;

    public Person(){

    }

    public Person(String name, boolean gender) {
        this.name = name;
        this.gender = gender;
    }

    public Person(String name, boolean gender, String birthdate) {
        this.name = name;
        this.gender = gender;
        this.birthdate = birthdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

}


interface Judgeable{
    //boolean judeg(String);
}

public class Main {

    public static int[] countPerson(List<Person> personList) {
        int count5 = 0;
        int count7 = 0;
        int countnull = 0;

        for (int i = 0; i < personList.size(); i++) {
            String name = personList.get(i).getName();
            if (name == "null"){
                countnull++;
                continue;
            }
            if (name.length() == 5){
                count5++;
            }
            if (name.length() == 7){
                count7++;
            }


        }

        return new int[]{count5,count7,countnull};
    }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Person> personList = new ArrayList<Person>();


        //在这里输入个数
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            String name = sc.next();
            boolean gender  = sc.nextBoolean();
            String  birthDate = sc.next();
            Person person = new Person(name,gender,birthDate);
            personList.add(person);
        }

        //循环创建person对象存入personList最后

        //注意：当name输入为null时，将其置为空

        int[] count = countPerson(personList);

        System.out.println("Number of person with nameLength == 5:" + count[0]);

        System.out.println("Number of person with nameLength == 7:"+ count[1]);

        System.out.println("Number of person with null name:"+ count[2]);
    }
}*/



import java.util.LinkedList;
import java.util.Scanner;

interface IntegerStack {
    public Integer push(Integer item);
    public Integer pop();
    public Integer peek();
    public boolean empty();
    public int size();
}

class ArrayListIntegerStack implements IntegerStack{
    private LinkedList<Integer> list;

    public ArrayListIntegerStack(){
        list = new LinkedList<>();
    }

    public LinkedList<Integer> getList() {
        return list;
    }

    @Override
    public Integer push(Integer item) {
        if (item == null){
            return null;
        }
        list.add(item);
        return item;
    }

    @Override
    public Integer pop() {
        if (list.size() == 0){
            return null;
        }
        int topNum = list.getLast();
        list.removeLast();
        return topNum;
    }

    @Override
    public Integer peek() {
        if (list.size() == 0){
            return null;
        }
        return list.getLast();
    }

    @Override
    public boolean empty() {
        return list.size() == 0;
    }

    @Override
    public int size() {
        return list.size();
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayListIntegerStack alis = new ArrayListIntegerStack();
        int m = scanner.nextInt();
        while(m-- > 0){
            int item = scanner.nextInt();
            System.out.println(alis.push(item));
        }
        System.out.println(alis.peek() + "," + alis.empty() + "," + alis.size());
        System.out.println(alis.getList());
        int x = scanner.nextInt();
        while(x-- > 0){
            System.out.println(alis.pop());
        }
        System.out.println(alis.peek() + "," + alis.empty() + "," + alis.size());
        System.out.println(alis.getList());
    }
}
