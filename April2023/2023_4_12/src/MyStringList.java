public class MyStringList {
    private ListNode head;

    class ListNode{
        private int val;
        private ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public void creatList(){
        ListNode node1 = new ListNode(12);
        ListNode node2 = new ListNode(24);
        ListNode node3 = new ListNode(36);
        ListNode node4 = new ListNode(48);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = null;

        head = node1;
    }

    //打印这个链表的所有元素
    public void display() {
        ListNode cur = head;
        while (cur != null){
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        System.out.println();
    }

    //得到单链表的长度
    public int size(){
        int count = 0;
        ListNode cur = head;
        while (cur != null){
            count++;
            cur = cur.next;
        }
        return count;
    }

    //查找是否包含关键字key是否在单链表当中
    public boolean contains(int key){
        ListNode cur = head;
        while (cur != null){
            if (cur.val == key){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }


    //头插法
    public void addFirst(int data){
        ListNode node = new ListNode(data);
        node.next = head;
        head = node;
    }

    //尾插法
    //要考虑是一个节点的情况
    public void addLast(int data){
        ListNode node = new ListNode(data);
        if (head == null){
            head = node;
            return;
        }

        ListNode cur = head;

        while (cur.next != null){
            cur = cur.next;
        }

        cur.next = node;
    }


    //任意位置插入,第一个数据节点为0号下标
    public void addIndex(int index,int data){
        ListNode cur = head;
        ListNode node = new ListNode(data);
        if (!checkIndex(index)){
            throw new IndexException("下标输入错误 " + index);
        }

        if (index == 0){
            addFirst(data);
            return;
        }

        if (index == this.size()){
            addLast(data);
        }

        //找到
        for (int i = 0; i < index - 1; i++) {
            cur = cur.next;
        }

        //交换
        node.next = cur.next;
        cur.next = node;


    }

    private boolean checkIndex(int index){
        if (index < 0 || index > this.size() ){
            return false;
        }

        return true;
    }

    //删除第一次出现关键字为key的节点
    public void remove(int key){
        if (head == null){
            return;
        }

        if (head.val == key){
            head = head.next;
            return;
        }

        ListNode prev = searchPrev(key);
        if (prev == null){
            System.out.println("没有这个数据");
            return;
        }
        ListNode del = prev.next;
        prev.next = del.next;
    }

    private ListNode searchPrev(int key){
        ListNode prev = head;
        while (prev.next != null){
            if (prev.next.val == key){
                return prev;
            }else{
                prev = prev.next;
            }
        }
        return null;
    }

    //删除所有值为key的节点
    public void removeAllKey(int key){
        if (head == null){
            return;
        }

        if (head.val == key){
            head = head.next;
        }

        ListNode cur = head.next;
        ListNode prev = head;

        while (cur != null){
            if (cur.val == key){
                prev.next = cur.next;
                cur = cur.next;
            }else {
                prev = cur;
                cur = cur.next;
            }
        }
    }
}
