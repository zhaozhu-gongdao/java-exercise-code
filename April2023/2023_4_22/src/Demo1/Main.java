package Demo1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<String> nameList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        //最先
        String name;
        while (!"end".equals(name = scanner.nextLine())){
            if (!(nameList.contains(name))){
                nameList.add(name);
            }
        }

        System.out.println(nameList);


        //加下来
        int index = scanner.nextInt();
        scanner.nextLine();
        String x = scanner.nextLine();
        nameList.add(index, x);

        int j = scanner.nextInt();
        nameList.remove(j);

        System.out.println(nameList);


        //最后
        scanner.nextLine();
        String name1 = scanner.nextLine();
        String name2 = scanner.nextLine();
        int k = nameList.indexOf(name1);
        System.out.println("k=" + k);
        if (k >= 0){
            nameList.set(k, name2);
        }else if (k == -1){
            nameList.add(name2);
        }

        System.out.println(nameList);
    }
}
