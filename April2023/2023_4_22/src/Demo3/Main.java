package Demo3;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author 梓叶枫林
 * @date 2020/10/28
 */
/*public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int count = 0;
        int num = scanner.nextInt();
        while (num > 0){
            num /= 10;
            count++;
        }

        System.out.println(count);
        //判断是否是回文数
        String newnum = String.valueOf(num);
        char[] ch = newnum.toCharArray();
        int i = 0;
        int j = ch.length - 1;
        boolean flag = true;
        while (i < j){
            if (ch[i] != ch[j]){
                flag = false;
                break;
            }
            i++;
            j--;
        }

        if (flag){
            System.out.println("Y");
        }else{
            System.out.println("N");
        }
    }
}*/


/*public class Main{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        for (int i = 0; i <= Math.sqrt(num); i++) {
            for (int j = 1; j <= Math.sqrt(num); j++) {
                if (Math.pow(i, 2) + Math.pow(j,2)== num){
                    System.out.println(i + "," +j);
                    return;
                }
            }
        }

        System.out.println(-1);
    }
}*/

/*
class Animal{
    public void introduce(){
        ;
    }
}

class Dog extends Animal{
    private String name;
    private String color;
    private int nb;

    public Dog(String name, String color, int nb) {
        this.name = name;
        this.color = color;
        this.nb = nb;
    }

    public void catchFrisbee(){
        System.out.println("catch frisbee");
    }


    @Override
    public void introduce() {
        System.out.println("My name is " + this.name +
                ", my color is " + this.color +
                ", my IQ is" + this.nb);
    }
}

class Cat extends Animal{
    private String name;
    private String color;
    private String earColor;

    public Cat(String name, String color, String earColor) {
        this.name = name;
        this.color = color;
        this.earColor = earColor;
    }

    public void catchMouse(){
        System.out.println("catch mouse");
    }

    @Override
    public void introduce() {
        System.out.println("My name is " + this.name +
                ", my color is " + this.color +
                ", my eyecolor is" + this.earColor);
    }
}

class TestAnimal{
    public static void introduce(Animal animal){
        if (animal instanceof Cat){
            ((Cat) animal).introduce();
        }else{
            ((Dog) animal).introduce();
        }
    }

    public static void action (Animal animal){
        if (animal instanceof Cat){
            ((Cat) animal).catchMouse();
        }else{
            ((Dog) animal).catchFrisbee();
        }
    }
}
*/



/*class Coboid{
    private double length = 1;
    private double width = 1;
    private double height = 1;

    public Coboid() {
        ;
    }

    public Coboid(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public double getArea(){
        return length * width * 2 +
                length * height * 2 +
                width * height * 2;
    }


    public double getVolume(){
        return length * width * height;
    }

}*/

/*
class MyClock{
    private int hour;
    private int minutes;
    private int second;

    public MyClock() {
        ;
    }

    public MyClock(int hour, int minutes, int second) {
        this.hour = hour;
        this.minutes = minutes;
        this.second = second;
    }

    public void setClock(int hour, int minutes, int second){
        this.hour = hour;
        this.minutes = minutes;
        this.second = second;
    }

    public void display(){
        System.out.format("%2d:%2d:%2d",this.hour,this.minutes,this.second);
    }
}


public class Main{
    public static void main(String[] args) {
        MyClock myClock1 = new MyClock();

        Scanner scanner = new Scanner(System.in);
        MyClock myClock2 = new MyClock(scanner.nextInt(), scanner.nextInt(),scanner.nextInt());

        System.out.println("输入一个时间：（时 分 秒用空格分隔）");
        myClock1.display();
        System.out.println();
        myClock2.display();
    }
}*/


/*class BankBusiness{
    private String Personname;
    private String password;
    private int balance;
    public static  String  bankName = "中国银行";

    public static void welcome(){
        System.out.println(bankName + "欢迎您的到来!");
    }

    public static void welcomeNext(){
        System.out.println("请收好您的证件和物品，欢迎您下次光临！");
    }

    public BankBusiness(String personname, String password) {
        this.Personname = personname;
        this.password = password;
    }

    public void deposit(String password,int depositMoney){
        if (!password.equals(this.password)){
            System.out.println("您的密码错误");
            return;
        }

        this.balance += depositMoney;
        System.out.format("您的余额有%.1f元",(double)this.balance);
        System.out.println();
    }

    public void withdraw(String password,int withdrawMoney){
        if (!password.equals(this.password)){
            System.out.println("您的密码错误");
            return;
        }

        if (this.balance < withdrawMoney){
            System.out.println("您的余额不足");
            return;
        }

        balance -= withdrawMoney;
        System.out.format("请取走钞票，您的余额还有%.1f元。",
                (double)this.balance);

        System.out.println();
    }
}

public class Main{
    public static void main(String[] args) {
        BankBusiness.welcome();
        Scanner scanner = new Scanner(System.in);

        String personName = scanner.next();
        String password = scanner.next();
        BankBusiness account = new BankBusiness(personName,password);

        password = scanner.next();
        int depositMoney = scanner.nextInt();
        account.deposit(password,depositMoney);


        password = scanner.next();
        int withdrawMoney = scanner.nextInt();
        account.withdraw(password,withdrawMoney);

        password = scanner.next();
        withdrawMoney = scanner.nextInt();
        account.withdraw(password,withdrawMoney);

        password = scanner.next();
        withdrawMoney = scanner.nextInt();
        account.withdraw(password,withdrawMoney);

        BankBusiness.welcomeNext();


    }
}*/


/*class IllegalScoreException extends RuntimeException{

}

class IllegalNameException extends RuntimeException{

}

class Student{
    private String name;
    private int score;


    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    //改造setName       //如果姓名首字母为数字则抛出IllegalNameException
    public int addScore(int score)  //如果加分后分数<0 或>100，则抛出IllegalScoreException，加分不成功。


}*/



public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        char[] ch = name.toCharArray();

        int numCount = 0;
        int zimuCount = 0;
        int spaceCount = 0;
        boolean flag = true;
        int elesnum = 0;
        for (int i = 0; i < ch.length; i++) {
            if (ch[i] >= '0' && ch[i] <= '9'){
                numCount++;
                flag = false;
            }

            if (ch[i] >= 'a' && ch[i] <= 'z'){
                zimuCount++;
                flag = false;

            }

            if (ch[i] ==' '){
                spaceCount++;
                flag = false;

            }

            if (flag){
                elesnum++;
            }
        }

        System.out.println("字母个数：" + zimuCount);
        System.out.println("数字个数：" + numCount);
        System.out.println("空格个数：" + spaceCount);
        System.out.println("其他字符个数：" + elesnum);

    }
}