package Demo2;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n1 = scanner.nextInt();

        //2.1
        PersonOverride[] persons1 = new PersonOverride[n1];
        for (int i = 0; i < n1; i++) {
            PersonOverride person = new PersonOverride();
            persons1[i] = person;
        }

        //2.2
        int count = 0;
        int n2 = scanner.nextInt();
        PersonOverride[] persons2 = new PersonOverride[n2];
        scanner.nextLine();
        for (int i = 0; i < n2; i++) {
            String name = scanner.next();
            int age = scanner.nextInt();
            boolean gender = scanner.nextBoolean();
            PersonOverride person = new PersonOverride(name, age, gender);

            boolean flag = true;
            for (int j = 0; j < i; j++) {
                while(person.equals(persons2[j])){
                    flag = false;
                    break;
                }
            }

            if (flag){
                persons2[count] = person;
                count++;
            }

        }

        //2.3
        for (PersonOverride hand:persons1) {
            System.out.println(hand);
        }

        //2.4
        for (int i = 0; i < count; i++) {
            System.out.println(persons2[i]);
        }

        System.out.println(count);

        //2.5
        System.out.println(Arrays.toString(PersonOverride.class.getConstructors()));
    }
}
