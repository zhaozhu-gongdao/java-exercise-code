package Demo2;

public class PersonOverride {
    private String name;
    private int age;
    private boolean gender;

    public PersonOverride(String name, int age, boolean gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public PersonOverride() {
        this("default", 1, true);
    }

    @Override
    public String toString() {
        return this.name + "-" + this.age + "-" + this.gender;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }

        if (!(obj instanceof PersonOverride)){
            return false;
        }

        if (obj == this){
            return true;
        }

        PersonOverride newObj = (PersonOverride) obj;
        if (this.name.equals(newObj.name) && this.gender == newObj.gender &&
            this.age == newObj.age){
            return true;
        }
        return false;
    }
}
