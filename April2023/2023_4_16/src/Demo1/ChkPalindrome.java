package Demo1;

public class ChkPalindrome {

    public class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }
    }

    public boolean chkPalindrome(ListNode A) {
        ListNode slow = A;
        ListNode fast = A;

        while (fast != null && fast.next != null){
            fast = fast.next.next;
            slow = slow.next;
        }

        //翻转
        ListNode cur = slow.next;
        while (cur != null){
            ListNode curNext = cur.next;
            cur.next = slow;
            slow = cur;
            cur = curNext;
        }

        //判断
        while (A != slow){
            if (A.val != slow.val){
                return false;
            }

            if (A.next == slow){
                return true;
            }

            A = A.next;
            slow = slow.next;
        }

        return true;
    }
}
