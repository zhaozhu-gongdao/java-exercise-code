package Demo1;

public class MergeTwoLists {
    public class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }

    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode newHead = new ListNode();
        ListNode tmpH = newHead;

        while (list1 != null && list2 != null){
            if (list1.val < list2.val){
                tmpH.next = list1;
                tmpH = tmpH.next;
                list1 = list1.next;
            }else {
                tmpH.next = list2;
                tmpH = tmpH.next;
                list2 = list2.next;
            }
        }

        if (list1 != null){
            tmpH.next = list1;
        }else {
            tmpH.next = list2;
        }

        return newHead.next;
    }

}
