package Demo2;

public abstract class Shape {
    protected static final double PI = 3.14;

    abstract public double getPerimeter();

    abstract public double getArea();

}
