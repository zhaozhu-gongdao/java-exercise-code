package Demo2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static double sumAllArea(List<Shape> shapes){
        double ret = 0;
        for (Shape shape:shapes) {
            ret += shape.getArea();
        }

        return ret;
    }

    public static double sumAllPerimeter(List<Shape> shapes){
        double ret = 0;
        for (Shape shape:shapes) {
            ret += shape.getPerimeter();
        }

        return ret;
    }

    public static void main(String[] args) {
        List<Shape> shapes = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            scanner.nextLine();
            String type = scanner.nextLine();
            if (type.equals("rect")){
                int width = scanner.nextInt();
                int length = scanner.nextInt();
                Shape shape = new Rectangle(width, length);
                shapes.add(shape);
            }else{
                int radius = scanner.nextInt();
                Shape shape = new Circle(radius);
                shapes.add(shape);
            }
        }

        System.out.println(sumAllPerimeter(shapes));
        System.out.println(sumAllArea(shapes));


        Shape[] newShapes = shapes.toArray(new Shape[shapes.size()]);
        //输出每个对象的信息
        System.out.println(Arrays.toString(newShapes));

        for (Shape shape:newShapes) {
            System.out.print(shape.getClass() + "," + shape.getClass().getSuperclass());
        }
    }
}
