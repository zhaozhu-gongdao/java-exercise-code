package Demo2;

public class Circle extends Shape{
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "[Circle [" +
                "radius=" + radius +
                ']';
    }


    @Override
    public double getPerimeter() {
        double ret = 2 * PI * this.radius;
        return ret;
    }

    @Override
    public double getArea() {
        double ret = this.radius * this.radius * PI;
        return ret;
    }

}
