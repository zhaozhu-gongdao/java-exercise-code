/*class MyArray {
    public Object[] objects = new Object[10];

    public Object getPos(int pos) {
        return objects[pos];
    }

    public void setVal(int pos,Object val) {
        objects[pos] = val;
    }
}*/

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 *
 * @param <T>  T是一个占位符，表示当前类是一个泛型类
 *
 */
class MyArray <T> {
    //public Object[] objects = new Object[10];
    //public T[] objects = new T[10];
    public T[] objects ;//这样写也是不好的 ！！！
    public MyArray() {
    }

    public MyArray(Class<T> clazz, int capacity) {
        objects = (T[]) Array.newInstance(clazz, capacity);
    }

    public T getPos(int pos) {
        return objects[pos];
    }

    public void setVal(int pos,T val) {
        objects[pos] = val;
    }

    public T[] getArray() {
        return objects;
    }
}

class MyArray2<T> {
    public Object[] objects = new Object[10];
    //public T[] objects = new T[10];
    public T getPos(int pos) {
        return (T)objects[pos];
    }
    public void setVal(int pos,T val) {
        objects[pos] = val;
    }

    public Object[] getArray() {
        return objects;
    }
}
public class Main {

    public static void main(String[] args) {
        MyArray2<Integer> myArray1 = new MyArray2<Integer>();
        myArray1.setVal(0,48);
        myArray1.setVal(1,10);
        int val = myArray1.getPos(1);
        System.out.println(val);
    }


/*
    public static void main3(String[] args) {
        MyArray<Integer> myArray1 = new MyArray<Integer>(Integer.class,10);
        //不允许吧Object类型 强制类型转换
        Integer[] integers = myArray1.getArray();

    }
    public static void main2(String[] args) {
        MyArray<Integer> myArray1 = new MyArray<Integer>();
        myArray1.setVal(0,48);
        myArray1.setVal(1,10);
        int val = myArray1.getPos(1);
        System.out.println(val);
        System.out.println("=======");
        MyArray<String> myArray2 = new MyArray<>();
        myArray2.setVal(0,"hello");
        myArray2.setVal(1,"bit");
        String ret = myArray2.getPos(1);
        System.out.println(ret);
    }
    public static void main1(String[] args) {
        MyArray myArray = new MyArray();
        myArray.setVal(0,"121");
        myArray.setVal(1,10);

        int val = (int)myArray.getPos(1);
        System.out.println(val);
    }*/
}