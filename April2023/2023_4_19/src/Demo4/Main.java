package Demo4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        int age = scanner.nextInt();
        String sex = scanner.next();

        Student stu = new Student(name, age, sex);
        System.out.println(stu);
    }
}
