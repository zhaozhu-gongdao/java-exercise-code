package Demo5;

public class Birthday {
    private int year;
    private int month;
    private int day;

    public Birthday(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void getAge(){
        int age = 2017 - this.year;
        if (this.month == 12 || this.month == 1){
            if (this.day > 25){
                age++;
            }
        }
        System.out.println("age=" + age);
    }
}

