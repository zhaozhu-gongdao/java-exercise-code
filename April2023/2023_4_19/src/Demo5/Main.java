package Demo5;
import java.util.Scanner;

public class Main{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int year = scanner.nextInt();
        int month = scanner.nextInt();
        int day = scanner.nextInt();

        Birthday birthday = new Birthday(year, month, day);
        birthday.getAge();
    }
}
