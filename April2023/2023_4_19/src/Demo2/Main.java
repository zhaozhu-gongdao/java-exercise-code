package Demo2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


class Person{
    private int id;
    private String name;
    private String sex;
    private int groupScore;
    private int handScore;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getGroupScore() {
        return groupScore;
    }

    public void setGroupScore(int groupScore) {
        this.groupScore = groupScore;
    }

    public int getHandScore() {
        return handScore;
    }

    public void setHandScore(int handScore) {
        this.handScore = handScore;
    }

    public Person() {
        ;
    }

    public Person(int id, int groupScore, int handScore ,String name, String sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.groupScore = groupScore;
        this.handScore = handScore;
    }

    @Override
    public String toString() {
        return this.id + "," + this.groupScore + "," + this.handScore
                + "," + this.name + "," + this.sex;
    }
}

class CompareGroupScore implements Comparator<Person>{
    @Override
    public int compare(Person o1, Person o2) {
        return (o1.getGroupScore() + o1.getHandScore()) - (o2.getHandScore() + o2.getGroupScore());
    }

}

class CompareHandScore implements Comparator<Person>{
    @Override
    public int compare(Person o1, Person o2) {
        return o1.getGroupScore() - o2.getGroupScore();
    }
}
public class Main{
    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person(1,500,400,"职工1","female"));
        personList.add(new Person(2,400,600,"职工2","female"));
        personList.add(new Person(3,600,300,"职工3","male"));
        personList.add(new Person(4,800,200,"职工4","female"));
        personList.add(new Person(5,500,700,"职工5","male"));

        System.out.println("编号，团险，个险，姓名，性别");
        //冒泡排序一下
        CompareGroupScore comp = new CompareGroupScore();
        for (int i = 0; i < personList.size() - 1; i++) {
            for (int j = i + 1; j < personList.size(); j++) {
                if (comp.compare(personList.get(i),personList.get(j)) > 0){
                    Person tmp = personList.get(i);
                    personList.set(i,personList.get(j));
                    personList.set(j,tmp);
                }
            }
        }

        for (int i = 0; i < personList.size(); i++) {
            System.out.println(personList.get(i));
        }


        System.out.println("编号，团险，个险，姓名，性别");
        //冒泡排序一下
        CompareHandScore comp2 = new CompareHandScore();
        for (int i = 0; i < personList.size() - 1; i++) {
            for (int j = i + 1; j < personList.size(); j++) {
                if (comp2.compare(personList.get(i),personList.get(j)) > 0){
                    Person tmp = personList.get(i);
                    personList.set(i,personList.get(j));
                    personList.set(j,tmp);
                }
            }
        }

        for (int i = 0; i < personList.size(); i++) {
            System.out.println(personList.get(i));
        }

    }
}


