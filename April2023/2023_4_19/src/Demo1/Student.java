package Demo1;

public class Student extends Person{
    private String stuNO;
    private String clazz;

    public Student(String name, int age, boolean gender,String stuNO, String clazz) {
        super(name, age, gender);
        this.stuNO = stuNO;
        this.clazz = clazz;

    }

    @Override
    public String toString() {
        return "Student:" + super.toString() + "-" + this.stuNO + "-" + this.clazz;
    }

    @Override
    public boolean equals(Object obj) {
        if(super.equals(obj) && obj instanceof Student){
            Student hand = (Student) obj;
            if (this.stuNO.equals(hand.stuNO) && this.clazz.equals(hand.clazz)){
                return true;
            }
        }

        return false;
    }
}
