package Demo1;

public abstract class Person implements Comparable<Person>{
    private String name;
    private int age;
    private boolean gender;

    public Person(String name, int age, boolean gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String toString(){
        return this.name +"-" + this.age + "-" + this.gender;
    }         //返回"name-age-gender"格式的字符串

    public boolean equals(Object obj){
        if (obj == null){
            return false;
        }

        if (!(obj instanceof Person)){
            return false;
        }

        Person hand = (Person)obj;
        if (this.name.equals((hand.name)) && this.age == hand.age && this.gender == hand.gender){
            return true;
        }

        return false;
    }//比较name、age、gender,都相同返回true,否则返回false

    @Override
    public int compareTo(Person o) {
        int temp = this.name.compareTo(o.name);
        return temp == 0 ? this.age - o.age : temp;
    }
}
