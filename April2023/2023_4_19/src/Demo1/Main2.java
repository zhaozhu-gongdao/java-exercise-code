package Demo1;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import static java.lang.System.exit;


public class Main2{
    public static void main(String[] args){
        List<Person> personList = new ArrayList<>();

        //1
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()){

            String tmp = scanner.next();
            if (tmp.equals("s")){

                String name = scanner.next();
                int age  = scanner.nextInt();
                boolean gender  = scanner.nextBoolean();

                String stuNo = scanner.next();
                String clazz = scanner.next();

                Student student = new Student(name, age, gender, stuNo, clazz);
                personList.add(student);
            }else if(tmp.equals("e")){
                String name = scanner.next();
                int age  = scanner.nextInt();
                boolean gender  = scanner.nextBoolean();

                double salary = scanner.nextDouble();
                String comName = scanner.next();
                Company company = new Company(comName);

                Employee employee = new Employee(name, age, gender, salary, company);
                personList.add(employee);
            }else{
                break;
            }
        }

        //2
        Collections.sort(personList);

        //3  接受输入，如果输入为exit则return退出程序，否则继续下面步骤

        //4
        //定义两个List
        List<Student> stuList = new ArrayList<>();
        List<Employee> empList = new ArrayList<>();


        for (int i = 0; i < personList.size(); i++) {
            Person hand = personList.get(i);
            boolean flag = true;
            if (hand instanceof Student){
                if (stuList.isEmpty()){
                    stuList.add((Student) hand);
                }else{
                    for (int j = 0; j < stuList.size(); j++) {
                        Student com = stuList.get(j);
                        if (com.equals((Student) hand)){
                            flag = false;
                            break;
                        }

                    }

                    if (flag){
                        stuList.add((Student) hand);

                    }
                }
            }else if (hand instanceof Employee){
                if (empList.isEmpty()){
                    empList.add((Employee) hand);
                }else{
                    for (int j = 0; j < empList.size(); j++) {
                        Employee com = empList.get(j);
                        if (com.equals((Employee) hand)){
                            flag = false;
                            break;
                        }
                    }

                    if (flag){
                        empList.add((Employee) hand);

                    }
                }
            }
        }


        for (int i = 0; i < personList.size(); i++) {
            System.out.println(personList.get(i));
        }

        System.out.println("stuList");
        for (int i = 0; i < stuList.size(); i++) {
            System.out.println(stuList.get(i));
        }

        System.out.println("empList");
        for (int i = 0; i < empList.size(); i++) {
            System.out.println(empList.get(i));
        }


    }
}