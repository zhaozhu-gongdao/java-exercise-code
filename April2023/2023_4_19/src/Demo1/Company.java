package Demo1;

public class Company {
    private String name;

    public Company(String name){
        this.name = name;
    }

    public String toString(){
        return this.name;
    }         //直接返回name


    public boolean equals(Object obj){
        if (obj == null){
            return false;
        }

        if (!(obj instanceof Company)){
            return false;
        }
        Company tmp = (Company) obj;
        if (this.name.equals(tmp.name)){
            return true;
        }

        return false;
    }//name相同返回true

}
