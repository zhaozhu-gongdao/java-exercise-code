package Demo1;

import java.text.DecimalFormat;

public class Employee extends Person{
    private Company company;
    private double salary;

    //建议使用super复用Person类的相关有参构造函数
    public Employee(String name, int age, boolean gender, double salary, Company company){
        super(name,age,gender);
        this.salary = salary;
        this.company = company;
    }
    public String toString(){
        return "Employee:" + super.toString() + "-" + this.company + "-" + this.salary;
    }         //返回"Employee:person的toString-company-salary"格式的字符串

    public boolean equals(Object obj){
        if(super.equals(obj) && obj instanceof Employee){
            Employee tmp = (Employee) obj;

            DecimalFormat df = new DecimalFormat("#.#");
            String newsalary = df.format(this.salary);
            String comsalary = df.format(tmp.salary);
            if (newsalary.equals(comsalary) && this.company.equals(tmp.company)){
                return true;
            }
        }

        return false;
    }//首先调用父类的equals方法,如果返回true。再比较company与salary。
//比较salary属性时，使用DecimalFormat df = new DecimalFormat("#.#");保留1位小数

}
