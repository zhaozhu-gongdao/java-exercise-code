import java.util.Scanner;
/*
class Solution {
    public int countSegments(String s) {
        if (s == null){
            return 0;
        }

        if (s.equals(" ")){
            return 0;
        }

        Scanner scanner = new Scanner(System.in);
        String[] str = s.split(" ");

        int count = 0;
        for (int i = 0; i < str.length; i++){
            if (str[i].length() != 0){
                count++;
            }
        }

        return count;

    }
}*/


/*
class Solution {
    public String toLowerCase(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            char ch = s.charAt(i);
            if (ch >= 65 && ch <= 90) {
                ch |= 32;
            }
            sb.append(ch);
        }
        return sb.toString();
    }
}
*/


/*
public class Main {
    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();     //String变为StringBuilder: 利用StringBuilder的构造方法或append()方法
        stringBuilder.append("12");
        System.out.println(stringBuilder);


        String ret = stringBuilder.toString();   //StringBuilder变为String: 调用toString()方法
        System.out.println(ret);
    }
}*/



public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int ret = firstUniqChar(scanner.nextLine());
        System.out.println(ret);
    }


    public static int firstUniqChar(String s) {
        char[] ch = s.toCharArray();
        if (ch.length == 0 || ch.length == 1){
            return -1;
        }

        if (ch == null){
            return -1;
        }

        int i = 0;
        for (; i < ch.length; i++){
            int count = 0;

            for (int j = 0; j < ch.length; j++){
                if (ch[i] == ch[j]){
                    count++;
                }
            }

            if (count < 2){
                return i;
            }
        }

        return -1;
    }
}