package Demo;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static List<Card> setCards(){
        List<Card> cards = new ArrayList<>();
        String suit[] = {"桃花","梅花","爱心","方块"};

        for (int i = 0; i < suit.length; i++) {
            for (int j = 1; j <= 13; j++) {
                Card card = new Card(suit[i],j);
                cards.add(card);
            }
        }

        return cards;
    }

    public static void bufferCards(List<Card> cards){
        Random random = new Random();

        for (int i = cards.size() - 1; i > 0; i--) {
            int j = random.nextInt(i);
            Card card = cards.get(j);
            cards.set(j,cards.get(i));
            cards.set(i,card);
        }
    }

    public static  void perGetCard(List<Card> cards){
        List<List<Card>> perCards = new ArrayList<>();
        List<Card> hand1 = new ArrayList<>();
        List<Card> hand2 = new ArrayList<>();
        List<Card> hand3 = new ArrayList<>();
        perCards.add(hand1);
        perCards.add(hand2);
        perCards.add(hand3);


        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                Card card = cards.remove(0);
                perCards.get(j).add(card);
            }
        }

        System.out.println("第1个人牌：");
        System.out.println(hand1);
        System.out.println("第2个人牌：");
        System.out.println(hand2);
        System.out.println("第3个人牌：");
        System.out.println(hand3);
        System.out.println("剩下的牌：");
        System.out.println(cards);
    }

    public static void main(String[] args) {
        List<Card> cards = setCards();
        System.out.println(cards);
        bufferCards(cards);
        perGetCard(cards);
    }

}
