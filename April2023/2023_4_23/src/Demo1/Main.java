package Demo1;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;


public class Main {


    /*covnertStringToList函数代码*/

    /*remove函数代码*/

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()){
            List<String> list = convertStringToList(sc.nextLine());
            System.out.println(list);
            String word = sc.nextLine();
            remove(list,word);
            System.out.println(list);
        }
        sc.close();


    }


    //以空格(单个或多个)为分隔符，将line中的元素抽取出来，放入一个List
    public static List<String> convertStringToList(String line) {
        List<String> list = new ArrayList<>();

        String[] newLine= line.split("\\s+");     //如果有多个空格的问题
        for (String s: newLine){
            list.add(s);
        }

        return list;
    }

    //在list中移除掉与str内容相同的元素
    public static void remove(List<String> list, String str){
        for (int i = 0; i < list.size(); ){
            if (list.get(i).equals(str)){
                list.remove(i);
                i = 0;
            }else{
                i++;
            }
        }
    }

}



//删除的时候，把数组的个数缩小
//看removeAll的源代码