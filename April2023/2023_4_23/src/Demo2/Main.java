package Demo2;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        ListNode node1 = new ListNode(9);
        ListNode node2 = new ListNode(9);
        ListNode node3 = new ListNode(9);
        ListNode node4 = new ListNode(9);
        ListNode node5 = new ListNode(9);
        ListNode node6 = new ListNode(9);
        ListNode node7 = new ListNode(9);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;
        node6.next = node7;

        ListNode twonode1 = new ListNode(9);
        ListNode twonode2 = new ListNode(9);
        ListNode twonode3 = new ListNode(9);
        ListNode twonode4 = new ListNode(9);

        twonode1.next = twonode2;
        twonode2.next = twonode3;
        twonode3.next = twonode4;

        ListNode pre = solution.addTwoNumbers(node1,twonode1);
                //l1 = [2,4,3], l2 = [5,6,4]
        System.out.println(pre.val);
    }

}
