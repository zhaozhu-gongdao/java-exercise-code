class Person {
    private String name;
    private int age;
    private boolean gender;
    private int id;

    public Person(){
        System.out.println("This is constructor");
        System.out.println(this.name + "," + this.age + "," + gender +
                "," + id);
    }

    public Person(String name, int age, boolean gender){
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String toString(){
        return "Person [name=" + this.name +
                ", age=" + this.age + ", gender= " +
                this.gender + ", id=" + id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
