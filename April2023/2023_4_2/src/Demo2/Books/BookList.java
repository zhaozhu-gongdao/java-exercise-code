package Demo2.Books;

public class BookList {
    private Book[] bookList = new Book[10];
    private int booksize = 3;

    public BookList (){
        bookList[0] = new Book("西游记","吴承恩",29,"小说");
        bookList[1] = new Book("三国演义","罗贯中",19,"小说");
        bookList[2] = new Book("红楼梦","曹雪芹",39,"小说");

    }

    public Book getBook(int pos) {
        return bookList[pos];
    }

    public int getBooksize() {
        return booksize;
    }

    public void setBookList(int pos, Book book){
        bookList[pos] = book;
    }

    public void setUsedSize(int num){
        booksize = num;
    }

    public int getUsedSize() {
    }
}
