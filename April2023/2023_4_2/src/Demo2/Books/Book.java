package Demo2.Books;

public class Book {
    private String name;
    private String author;
    private int price;
    private boolean isBorred;
    private String type;

    public Book(String name, String author, int price, String type) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", isBorred=" + isBorred +
                ", type='" + type + '\'' +
                '}';
    }

    public void setBorred(boolean borred) {
        isBorred = borred;
    }

    public boolean isBorred() {
        return isBorred;
    }
}
