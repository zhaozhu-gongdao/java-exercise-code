package Demo2;

import Demo2.Books.BookList;
import Demo2.User.GenUser;
import Demo2.User.NormUser;
import Demo2.User.User;

import java.util.Scanner;
public class Main {
    public static User login(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你的名字:");
        String name = scanner.nextLine();
        System.out.println("请输入你的身份: 1表示管理员,0 表示普通用户");
        int choice = scanner.nextInt();
        if (choice == 1){
            return new GenUser(name);
        }else{
            return new NormUser(name);
        }
    }

    public static void main(String[] args) {
        BookList bookList = new BookList();
        User user = login();
        while (true){
            int choice = user.menu();
            user.doOperation(choice, bookList);
        }
    }
}
