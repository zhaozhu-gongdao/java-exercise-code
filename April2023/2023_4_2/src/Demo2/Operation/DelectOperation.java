package Operation;

import Demo2.Books.Book;
import Demo2.Books.BookList;

import java.util.Scanner;

public class DelectOperation implements IoOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("删除图书");

        System.out.println("请输入你要删除的图书名字");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        boolean flag = false;
        for (int i = 0; i < bookList.getBooksize(); i++) {
            Book book = bookList.getBook(i);

            if (book.getName().equals(name)){
                System.out.println("删除成功");
                flag = true;
            }
        }

        if (!flag){
            System.out.println("查不到这本书");
        }


        int currentSize = bookList.getBooksize();
        for (int i = currentSize; i < currentSize - 1; i++) {
            Book book = bookList.getBook(i + 1);
            bookList.setBookList(i + 1, book);
        }

        bookList.setUsedSize(currentSize - 1);
    }
}
