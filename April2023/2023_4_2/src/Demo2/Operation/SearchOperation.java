package Operation;

import Demo2.Books.Book;
import Demo2.Books.BookList;
import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Scanner;

public class SearchOperation implements IoOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("查找图书");
        System.out.println("请输入要查找的书籍名字:");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        int booksSize = bookList.getBooksize();
        for (int i = 0; i < booksSize; i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)) {
                System.out.println("找到这本书了！");
                System.out.println(book);
                return;
            }
        }
        System.out.println("没有找到这本书");
    }

}
