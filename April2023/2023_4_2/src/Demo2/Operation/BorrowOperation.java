package Operation;

import Demo2.Books.BookList;
import java.util.Scanner;
import Demo2.Books.Book;

public class BorrowOperation implements IoOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("借阅图书");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要归还的图书的名字：");
        String name = scanner.nextLine();//水浒传

        int currentSize = bookList.getUsedSize();
        for (int i = 0; i < currentSize; i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)) {
                if(book.isBorred()) {
                    System.out.println("该书已经被借出！");
                }else{
                    book.setBorred(true);
                }
                return;
            }
        }
        System.out.println("没有你要归还的图书！");
    }
}
