package Operation;

import Demo2.Books.Book;
import Demo2.Books.BookList;

public class ShowOperation implements IoOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("显示图书");
        int booksSize = bookList.getBooksize();
        for (int i = 0; i < booksSize; i++) {
            Book book = bookList.getBook(i);
            System.out.println(book);
        }
    }

}
