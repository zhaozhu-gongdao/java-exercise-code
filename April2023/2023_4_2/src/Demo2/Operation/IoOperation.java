package Operation;

import Demo2.Books.BookList;

public interface IoOperation {
    void work(BookList bookList);
}

