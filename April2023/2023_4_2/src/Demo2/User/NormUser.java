package Demo2.User;

import Operation.*;

import java.util.Scanner;

public class NormUser extends User {
    public NormUser(String name) {
        super(name);
        super.ioperation = new IoOperation[]{
                new ExitOperation(),
                new SearchOperation(),
                new BorrowOperation(),
                new ReturnOperation()
        };
    }

    @Override
    public int menu() {
        System.out.println("普通用户的菜单！");
        System.out.println("****************************");
        System.out.println("hello " + this.name +" 欢迎来到图书小练习");
        System.out.println("1. 查找图书");
        System.out.println("2. 借阅图书");
        System.out.println("3. 归还图书");
        System.out.println("0. 退出系统！");
        System.out.println("****************************");
        System.out.println("请输入你的操作：");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        return choice;
    }
}
