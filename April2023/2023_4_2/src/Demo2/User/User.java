package Demo2.User;

import Demo2.Books.BookList;
import Operation.IoOperation;

abstract public class User {
    protected String name;
    protected IoOperation[] ioperation;

    public User(String name) {
        this.name = name;
    }

    public abstract int menu();

    public void doOperation(int choice, BookList bookList){
        this.ioperation[choice].work(bookList);
    }
}
