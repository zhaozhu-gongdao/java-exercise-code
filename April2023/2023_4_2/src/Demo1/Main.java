package Demo1;

/*public class Main {
    public static void main(String[] args) {
        System.out.println("       **");
        System.out.println("      *  *");
        System.out.println("     *    *");
        System.out.println("*   *      *   *");
        System.out.println(" * *        * * ");
        System.out.println("  *          *");
    }
}*/


/*public class Main{
    public static void main(String[] args) {
        int c = 5 * (100 - 32) / 9;
        System.out.println("fahr = 100, celsius = " + c);
    }
}*/


/*
import java.util.Scanner;

public class Main{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double m = scanner.nextInt() / 100.0;
        int inch = (int)((m / 0.3048) % 12);
        int foot = (int)((m / 0.3048) / 12);
        System.out.println(foot + " " + inch);
    }
}*/


import javax.swing.*;
import java.util.Scanner;

/*public class Main {
    public static void main(String[] args) {
        int a = 10;
        System.out.printf("%d",a);

        System.out.format("%d",a);
    }
}*/




/*
import java.text.DecimalFormat;

public class Main {
    public static void main(String[] args) {

        DecimalFormat df = new DecimalFormat();
        double data = 1234.56789;
        System.out.println("格式化之前的数字: " + data);

        // 定义要显示的数字的格式，模式中的"#"表示如果位数不足则以 0填充
        String style = "0.00000000";
        df.applyPattern(style);   // 将格式应用于格式化器
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

        // 模式中的"#"表示如果该位存在字符，则显示字符，如果不存在，则不显示。
        style = "######.##########";
        df.applyPattern(style);
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

        // 在格式后添加单位字符
        style = "00000.000 kg";
        df.applyPattern(style);
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

        // 模式中的"-"表示输出为负数，要放在最前面
        style = "-000.000";
        df.applyPattern(style);
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

        // 模式中的","在数字中添加逗号，方便读数字
        style = "-0,000.0#";
        df.applyPattern(style);
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

        // 模式中的"E"表示输出为指数，"E"之前的字符串是底数的格式，"E"之后的是字符串是指数的格式
        style = "0.000E000";
        df.applyPattern(style);
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

        // 模式中的"%"表示乘以100并显示为百分数，要放在最后。
        style = "0.00 %";
        df.applyPattern(style);
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

        // 模式中的"\u2030"表示乘以1000并显示为千分数，要放在最后。
        style = "0.00 \u2030";
        DecimalFormat df1 = new DecimalFormat(style);  //在构造函数中设置数字格式
        System.out.println("采用style: " + style + "  格式化之后: " + df1.format(data));

        // 以逗号分隔
        // 如果使用具有多个分组字符的模式，则最后一个分隔符和整数结尾之间的间隔才是使用的分组大小。
        // 所以 "#,##,###,####" == "######,####" == "##,####,####"。
        style = "000,0,00.##########";
        df.applyPattern(style);
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

        // 嵌入文本中
        style = "这件衣服的价格是 ##.## 元";
        df.applyPattern(style);
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

        // 货币符号
        style = "##.##\u00A4\u00A4";
        df.applyPattern(style);
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

        // 特殊符号
        style = "'#'##.##";
        df.applyPattern(style);
        System.out.println("采用style: " + style + "  格式化之后: " + df.format(data));

    }
}*/


/*
class Person{
    private String name;
    private int age;
    private boolean gender;
    private int id = num;
    private static int num = 0;

    public Person() {
        System.out.println("This is constructor");;
    }

    public Person(String name, int age, boolean gender) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    {
        System.out.println("This is initialization block, id is " + this.id);
        num++;
    }

    static{
        System.out.println("This is static initialization block");
    }


    @Override
    public String toString() {
        return "Person [" +
                "name=" + name +
                ", age=" + age +
                ", gender=" + gender +
                ", id=" + id +
                '}';
    }
}
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        Person[] person = new Person[n];
        for (int i = 0; i < n; i++) {
            String name = scanner.next();
            int age = scanner.nextInt();
            boolean gender = scanner.nextBoolean();
            person[i] = new Person(name, age, gender);
        }

        for (int i = n - 1; i >= 0 ; i--) {
            System.out.println(person[i]);
        }

        Person person1 = new Person();
        System.out.println(person1);
    }
}*/




import java.util.*;

/*
class Person{
    private String name;
    private int age;
    private boolean gender;
    private int id;
    private static int num=0;
    //初始化块
    static {
        System.out.println("This is static initialization block");
    }
    public Person(){
        id=num;
        num++;
        System.out.println("This is initialization block, id is "+id);
        System.out.println("This is constructor");
        System.out.println(name+","+age+","+gender+","+id);
    }
    public Person(String name,int age,boolean gender){
        id=num;
        num++;
        System.out.println("This is initialization block, id is "+id);
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public boolean isGender() {
        return gender;
    }
    public void setGender(boolean gender) {
        this.gender = gender;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
    }
}
public class Main{
    public static void main(String []args){
        Scanner in = new Scanner(System.in);
        int n=in.nextInt();
        Person a[] = new Person[n];
        for(int i=0;i<n;i++){
            String name = in.next();
            int age = in.nextInt();
            boolean gender = in.nextBoolean();
            a[i] = new Person(name,age,gender);
        }
        for(int i=n-1;i>=0;i--) {
            System.out.println(a[i].toString());
        }
        Person b = new Person();
        System.out.println(b);
    }
}*/

