package Demo1;

class Money implements Cloneable{
    public int money;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

class Stu implements Cloneable{
    private String name;
    private int age;
    Money m = new Money();

    public Stu(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Stu{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Stu tmp = (Stu)super.clone();
        tmp.m = (Money) this.m.clone();
        return tmp;
    }
}
public class Clone {
    public static void main(String[] args)  throws CloneNotSupportedException{
        Stu student1 = new Stu("aaa", 19);
        Stu student2 = (Stu)student1.clone();
        student1.m.money = 19;
        System.out.println(student1.m.money);
        System.out.println(student2.m.money);
        System.out.println();
        student1.m.money = 29;
        System.out.println(student1.m.money);
        System.out.println(student2.m.money);

        Object obj = new Object();
    }
}



