package Demo4;

public class ArrayUtil {
    public static int findMax(int[] arr, int begin, int end){
        if (begin >= end){
            throw new IllegalArgumentException("begin:"+begin+" >= end:"+end);
        } else if (begin < 0){
            throw new IllegalArgumentException("begin:"+begin+" < 0");
        } else if (end > arr.length){
            throw new IllegalArgumentException("end:"+end+" > arr.length");
        }

        return end;
    }
}
