package Demo4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];

        inputArr(n, arr,scanner);

        int logarnum = scanner.nextInt();
        for (int i = 0; i < logarnum; i++) {
            try{
                int[] arr2 = inputLogar(scanner);
                int ret = ArrayUtil.findMax(arr,arr2[0], arr2[1]);
                System.out.println(ret);
            }catch(IllegalArgumentException e){
                System.out.println(e);
            }
        }


    }

    public static void inputArr(int n, int[] arr, Scanner scanner){
        for (int i = 0; i < n; i++) {
            int num = scanner.nextInt();
            arr[i] = num;
        }
    }

    public static int[] inputLogar(Scanner scanner){
        int begin = scanner.nextInt();
        int end = scanner.nextInt();
        return new int[]{begin, end};
    }
}
