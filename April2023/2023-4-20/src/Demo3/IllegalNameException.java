package Demo3;

public class IllegalNameException extends RuntimeException{
    public IllegalNameException(String message) {
        super(message);
    }
}
