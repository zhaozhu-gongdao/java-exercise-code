package Demo3;

public class Student {
    private String name;
    private int score;

    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student [" +
                "name=" + name +
                ", score=" + score +
                "]";
    }

    public void SetName(){
        char newname = this.name.charAt(0);
        if(newname >= '0' && newname <= '9'){
            throw new IllegalNameException("the first char of name must not be digit, name=" + this.name);
        }
    }

    public int addScore(int score){
        if (this.score < 0 || this.score > 100){
            throw new IllegalScoreException("score out of range, score=" + this.score);
        }

        return 0;
    }
}
