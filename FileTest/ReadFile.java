import java.io.*;
import java.util.Scanner;

//读取文件的操作
public class ReadFile {
    //通过字节流读取
    public void readByStream(File file) throws IOException {
        try(InputStream inputStream = new FileInputStream(file)){
            try(Scanner scanner = new Scanner(inputStream, "utf-8")){
                while (scanner.hasNext()){
                    System.out.println(scanner.next());
                }
            }
        }

    }


    public static void main(String[] args) throws IOException {
        File file = new File("./test.txt");
        ReadFile test = new ReadFile();
        test.readByStream(file);

//        File file2 = new File("./test.txt");
//        test.readByReader(file2);
    }
}
