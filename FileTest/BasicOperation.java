import java.io.File;
import java.io.IOException;

public class BasicOperation {
    //创建文件
    public void createFile() throws IOException {
        File file = new File("./test.txt");
        file.createNewFile();
    }
    //删除文件
    public void deleteFile() throws IOException {
        File file = new File("./test.txt");
        try {
            //执行了删除操作
            file.deleteOnExit();
            int a = 10/0;
        }catch (Exception e){

        }

    }

    //创建文件夹，分单级和多级
    public void createDir() throws IOException {
        File file = new File("./A/test/aaa");
        file.mkdirs();

    }

    public void reNameFile() throws IOException {
        File file = new File("./test.txt");
        File newFileName = new File("./test2");
        newFileName.renameTo(file);

    }
    public static void main(String[] args) throws IOException {
        BasicOperation test = new BasicOperation();
        //test.createFile();
//        test.deleteFile();
        //test.createDir();
        test.reNameFile();
    }
}