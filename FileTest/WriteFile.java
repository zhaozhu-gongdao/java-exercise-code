import java.io.*;

public class WriteFile {
    //使用字节流写入
    public void wiriteByStream() throws IOException {
        try(OutputStream os = new FileOutputStream("./test.txt", true)){
            try(OutputStreamWriter outputStreamWriter = new OutputStreamWriter(os, "UTF-8")){
                try(PrintWriter writer =  new PrintWriter(outputStreamWriter)){
                    writer.println("xxxxx");
                    writer.print("这是第二行内容");
                }
            }
        }
    }
    //使用字符流写入
    public void wiriteByWriter() throws IOException {
        try(Writer writer = new FileWriter("./test.txt", true)){
            writer.write("这是第三行内容");
        }
    }

    public static void main(String[] args) throws IOException {
        WriteFile writeFile = new WriteFile();
        writeFile.wiriteByStream();

        writeFile.wiriteByWriter();
    }
}
