package Demo1;

import java.util.PriorityQueue;

/*public class Main {
    PriorityQueue test = new PriorityQueue<>();
    public class Card {
        public int rank; // 数值
        public String suit; // 花色

        public Card(int rank, String suit) {
            this.rank = rank;
            this.suit = suit;
        }

        @Override
        public boolean equals(Object o) {
        // 自己和自己比较
            if (this == o) {
                return true;
            }
        // o如果是null对象，或者o不是Card的子类
            if (o == null || !(o instanceof Card)) {
                return false;
            }
        // 注意基本类型可以直接比较，但引用类型最好调用其equal方法
            Card c = (Card) o;
            return rank == c.rank
                    && suit.equals(c.suit);
        }
    }
}*/

public class Main {
    public static void heapSort(){
        int end = usedSize - 1;
        while (end > 0){
            int tmp = elem[0];
            elem[0] = elem[end];
            elem[end] = tmp;
            shiftDown(0, end);
            end--;
        }
    }
}
