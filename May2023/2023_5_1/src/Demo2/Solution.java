package Demo2;

public class Solution {
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] tmpArr = new int[m + n];

        int p1 = 0;
        int p2 = 0;
        int num = 0;
        while (m != 0 && n != 0){
            if (nums1[p1] >= nums2[p2]){
                tmpArr[num++] = nums2[p2];
                p2++;
                n--;
            }else if (nums1[p1] < nums2[p2]){
                tmpArr[num++] = nums1[p1];
                p1++;
                m--;
            }
        }

        if (m == 0){
            //nums2里面还有元素
            while (n != 0){
                tmpArr[num++] = nums2[p2];
                p2++;
                n--;
            }

        }else {
            while (m != 0){
                tmpArr[num++] = nums1[p1];
                p1++;
                m--;
            }
        }

        System.arraycopy(tmpArr, 0, nums1, 0, tmpArr.length);
    }

    public static void main(String[] args) {
        merge(new int[]{2,0},1,new int[]{1},1);
    }
}
