package Demo4;

public class Main {
    public static void main(String[] args) {
        USB usb1 = new Mouse();
        usb1.work();
        usb1.stop();

        USB[] usbs = new USB[2];
        usbs[0] = new UPan();
        usbs[1] = new Mouse();

        for (int i = 0; i < 2; i++) {
            usbs[i].work();;
            usbs[i].stop();
        }
    }
}
