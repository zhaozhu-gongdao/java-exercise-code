package Demo1;

public class Solution {
    public static void rotate(int[] nums, int k) {
        for(int i = 0; i < k; i++){
            int endIndex = nums.length;
            int outer = nums[endIndex - 1];

            for(int j = endIndex - 1; j > 0; j-- ){
                nums[j] = nums[j - 1];
            }

            nums[0] = outer;
        }
    }


    public static void main(String[] args) {
        rotate(new int[]{1,2,3,4,5,6}, 3);
    }
}

class Solution2 {
    public int removeElement(int[] nums, int val) {
        int left = 0;
        for(int right = 0; right < nums.length; right++){
            if (nums[right] != val){
                nums[left] = nums[right];
                left++;
            }
        }

        return left;
    }
}


class Solution3 {
    public int removeDuplicates(int[] nums) {
        int left = 1;
        for (int right = 1; right < nums.length; right++){
            if (nums[right] != nums[right - 1]){
                nums[left] = nums[right];
                left++;
            }
        }

        return left;
    }
}