package Demo5;

import java.util.Scanner;

public class Main {
    public static int computer(ICompute o, int a, int b){
        return o.computer(a,b);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println(computer(new Add(), a, b));
        System.out.println(computer(new Sub(), a, b));
    }
}
