package MyArrayList;

import java.util.Arrays;

public class MyArraylist {

    public int[] elem;
    public int usedSize;//0
    //默认容量
    private static final int DEFAULT_SIZE = 10;

    public MyArraylist() {
        this.elem = new int[DEFAULT_SIZE];
    }

    /**
     * 打印顺序表:
     * 根据usedSize判断即可
     */
    public void display() {
        for (int i = 0; i < elem.length; i++) {
            System.out.print(elem[i] + " ");
        }

        System.out.println();
    }

    // 新增元素,默认在数组最后新增
    public void add(int data) {
        if (!isFull()){
            elem[usedSize++] = data;
        }else {
            elem = Arrays.copyOf(elem, elem.length * 2);
        }

    }

    /**
     * 判断当前的顺序表是不是满的！
     *
     * @return true:满   false代表空
     */
    public boolean isFull() {
        if (usedSize == elem.length){
            return true;
        }
        return false;
    }


    private boolean checkPosInAdd(int pos) {
        if (pos < 0 || pos >= elem.length){
            return false;
        }
        return true;//合法
    }

    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        if (checkPosInAdd(pos)){
            elem[pos] = data;
        }
    }

    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        for (int i = 0; i < usedSize; i++) {
            if (elem[i] == toFind){
                return true;
            }
        }
        return false;
    }

    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        for (int i = 0; i < usedSize; i++) {
            if (elem[i] == toFind){
                return i;
            }
        }
        return -1;
    }

    // 获取 pos 位置的元素
    public int get(int pos) {
        if (checkPosInAdd(pos)){
            return elem[pos];
        }

        return -1;
    }

    private boolean isEmpty() {
        if (usedSize == 0){
            return true;
        }
        return false;
    }

    // 给 pos 位置的元素设为【更新为】 value
    public void set(int pos, int value) {
        if (checkPosInAdd(pos)){
            elem[pos] = value;
        }
    }

    /**
     * 删除第一次出现的关键字key
     *
     * @param key
     */
    public void remove(int key) {
        int left = 0;
        int count = 0;
        int[] tmpArr = new int[elem.length];
        for (int right = 0; right < usedSize; right++) {
            if (elem[right] == key && count == 0){
                count++;
            } else{
                tmpArr[left] = elem[right];
                left++;
            }
        }
        elem = tmpArr;
    }

    // 获取顺序表长度
    public int size() {
        return usedSize;
    }

    // 清空顺序表
    public void clear() {
        for (int i = 0; i < usedSize; i++) {
            elem[i] = 0;
        }
        usedSize = 0;
    }
}
