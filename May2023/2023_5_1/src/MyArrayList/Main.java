package MyArrayList;

public class Main {
    public static void main(String[] args) {
        MyArraylist myArraylist = new MyArraylist();
        myArraylist.add(4);
        myArraylist.add(8);
        myArraylist.display();

        myArraylist.add(1,10);
        myArraylist.display();

        myArraylist.add(6);
        myArraylist.add(6);
        myArraylist.remove(6);
        myArraylist.display();

        System.out.println(myArraylist.contains(4));
        System.out.println(myArraylist.indexOf(4));
    }
}
