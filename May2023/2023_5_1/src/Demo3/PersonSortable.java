package Demo3;

public class PersonSortable implements Comparable<PersonSortable>{
    private String name;
    private int age;

    public PersonSortable(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return this.name + "-" + this.age;
    }

    @Override
    public int compareTo(PersonSortable o) {
        //名字是否相同
        if (!this.name.equals(o.name)){
            return this.name.compareTo(o.name);
        }else{
            //根据年龄判断
            return this.age - o.age;
        }
    }
}
