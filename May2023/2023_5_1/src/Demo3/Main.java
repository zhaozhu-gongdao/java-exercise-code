package Demo3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            int a = scanner.nextInt();
            int b = scanner.nextInt();

            if (a > 1000 || -a > 1000){
                System.out.println("|a|>1000");
            }else{
                System.out.println(a + b);
            }
        }
    }
}
