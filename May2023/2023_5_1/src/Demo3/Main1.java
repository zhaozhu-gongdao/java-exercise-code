package Demo3;

import java.util.Arrays;
import java.util.Scanner;

public class Main1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        PersonSortable[] persons = new PersonSortable[n];

        //输入n个数据
        for (int i = 0; i < n; i++) {
            String name = scanner.next();
            int age = scanner.nextInt();
            PersonSortable person = new PersonSortable(name,age);

            persons[i] = person;
        }

        //比较
        Arrays.sort(persons);

        //打印
        for(PersonSortable s:persons){
            System.out.println(s);
        }

        System.out.println(Arrays.toString(PersonSortable.class.getInterfaces()));
    }
}
