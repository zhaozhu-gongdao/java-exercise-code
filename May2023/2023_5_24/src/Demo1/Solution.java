package Demo1;

import java.util.Scanner;

public class Solution {
    static class TreeNode{
        public char val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(char val) {
            this.val = val;
        }
    }

    public static int i = 0;
    public TreeNode creatTree1(String str){
        char ch = str.charAt(i);

        TreeNode root = null;
        if (ch != '#'){
            root = new TreeNode(ch);
            i++;
            root.left = creatTree1(str);
            root.right = creatTree1(str);
        }else{
            i++;
        }

        return root;
    }

    public static int j = 0;
    public TreeNode creatTree2(String str){
        char ch = str.charAt(j);

        TreeNode root = null;
        if (ch != '#'){
            root = new TreeNode(ch);
            j++;
            root.left = creatTree2(str);
            root.right = creatTree2(str);
        }else{
            j++;
        }

        return root;
    }
/*
    public  int getHeight(TreeNode root) {
        if(root == null) {
            return 0;
        }
        int leftH = getHeight(root.left);
        int rightH = getHeight(root.right);

        return (leftH > rightH ? leftH : rightH) + 1;
    }

    public  int getHeight2(TreeNode root) {
        if(root == null) {
            return 0;
        }

        return (getHeight2(root.left) > getHeight2(root.right) ?
                getHeight2(root.left) :getHeight2(root.right)) + 1;
    }
*/


    public boolean isBalanced(TreeNode root) {
        if (root == null){
            return true;
        }

        int leftH = getHeight(root.left);
        int rightH = getHeight(root.right);

        return Math.abs(leftH - rightH) < 2 && isBalanced(root.left)
                && isBalanced(root.right);

    }

    public int getHeight(TreeNode root){
        if (root == null){
            return 0;
        }

        int leftH = getHeight(root.left);
        int rightH = getHeight(root.left);

        return (leftH > rightH? leftH : rightH) + 1;
    }


/*    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        if (root == null){
            return false;
        }

        if (isSameTree(root, subRoot)){
            return true;
        }

        if (isSameTree(root.left, subRoot)){
            return true;
        }

        if (isSameTree(root.right, subRoot)){
            return true;
        }

        return false;
    }

    public boolean isSameTree(TreeNode root, TreeNode subRoot) {
        if (root == subRoot) {
            return true;
        }

        if (root == null && subRoot == null) {
            return true;
        }

        if (root == null && subRoot != null
                || root != null && subRoot == null) {
            return false;
        }

        if (root.val != subRoot.val) {
            return false;
        }

        return isSameTree(root.left,subRoot.left)
                && isSameTree(root.right,subRoot.right);
    }*/


    // 时间复杂度： O(min(m,n))
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if(p == null && q != null || p != null && q == null) {
            return false;
        }
        if(p == null && q == null) {
            return true;
        }
        //一定是p 和 q 都不等于空！
        if(p.val != q.val) {
            return false;
        }
        return isSameTree(p.left,q.left)
                && isSameTree(p.right,q.right);
    }

    //时间复杂度：O(S*T)
    //每个s  都要和 t 判断是不是相同的！
    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        if(root == null) {
            return false;
        }
        if(isSameTree(root,subRoot)) {
            return true;
        }
        if(isSubtree(root.left,subRoot)) {
            return true;
        }
        if(isSubtree(root.right,subRoot)) {
            return true;
        }
        return false;
    }

}

class Main{
    public static void main(String[] args) {
        Solution test = new Solution();
        Scanner scanner = new Scanner(System.in);
        Solution.TreeNode  root = test.creatTree1(scanner.nextLine());
/*        boolean ret = test.isBalanced(root);
        System.out.println(ret);*/
        Solution.TreeNode  subRoot = test.creatTree2(scanner.nextLine());
        boolean ret = test.isSubtree(root,subRoot);
        System.out.println(ret);
    }
}
