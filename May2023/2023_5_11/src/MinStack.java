import java.util.Arrays;

public class MinStack {
    private int[] arr;
    private int useSize;

    public MinStack() {
        arr = new int[5];
        useSize = 0;
    }

    public void push(int val) {
        while (useSize == arr.length){
            arr = Arrays.copyOf(arr,arr.length * 2);
        }
        arr[useSize] = val;
        useSize++;
    }

    public void pop() {
        useSize--;
    }

    public int top() {
        return arr[useSize - 1];
    }

    public int getMin() {
        int min = arr[0];
        for(int i = 0; i < useSize; i++){
            if (min > arr[i]){
                min = arr[i];
            }
        }

        return min;
    }
}

class Main{
    public static void main(String[] args) {
        MinStack minStack = new MinStack();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        minStack.getMin();
        minStack.pop();
        minStack.top();
        int ret = minStack.getMin();
        System.out.println(ret);
    }
}