package Demo1;

import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
class TreeNode{
    public char val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(char val){
        this.val = val;
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String str = in.nextLine();
            TreeNode root = creat(str);
            inOrder(root);
        }
    }

    private static int i = 0;
    public static TreeNode creat(String str){
        char val = str.charAt(i);
        TreeNode root = null;
        if (val  != '#'){
            root = new TreeNode(val);
            i++;
            root.left = creat(str);
            root.right = creat(str);
        }else{
            i++;
        }

        return root;
    }

    public static void inOrder(TreeNode root){
        if (root == null){
            return;
        }

        inOrder(root.left);
        System.out.print(root.val + " ");
        inOrder(root.right);
    }
}

