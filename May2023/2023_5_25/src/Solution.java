public class Solution {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
        }
    }

    public boolean isSymmetric(TreeNode root) {
        if (root == null){
            return false;
        }


        return isSametricTree(root.left, root.right);
    }


    public boolean isSametricTree(TreeNode leftNode, TreeNode rightNode){
        if (leftNode == null && rightNode == null){
            return true;
        }


        if (leftNode != null && rightNode == null ||
                leftNode == null && rightNode != null){
            return false;
        }


        if (leftNode.val != rightNode.val){
            return false;
        }


        return isSametricTree(leftNode.left, rightNode.right)
                && isSametricTree(leftNode.right, rightNode.left);
    }
}
