package Demo2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double longRadious = scanner.nextDouble();
        double shortRadius = scanner.nextDouble();

        Oval oval = new Oval(longRadious,shortRadius);
        System.out.println("The area of " + oval + "is " + oval.area());
        System.out.println("The perimeterof " + oval + "is " + oval.perimeter());
    }
}
