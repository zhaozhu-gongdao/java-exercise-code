package Demo2;

public class Oval extends Shape{
    private double longRadius;
    private double shortRadius;

    public Oval(double longRadius, double shortRadius) {
        this.longRadius = longRadius;
        this.shortRadius = shortRadius;
    }

    public Oval(){
        ;
    }

    @Override
    public double area() {
        return PI * this.longRadius * this.shortRadius;
    }

    @Override
    public double perimeter() {
        return  2 * PI * Math.sqrt((shortRadius * shortRadius +
                longRadius * longRadius) / 2);

    }

    @Override
    public String toString() {
        return "Oval(" +
                "a:" + longRadius +
                ",b:" + shortRadius +
                ')';
    }
}
