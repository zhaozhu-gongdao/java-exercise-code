package Demo2;

public abstract class Shape {
    protected double PI = 3.1415926;

    public abstract double area();

    public abstract double perimeter();
}

