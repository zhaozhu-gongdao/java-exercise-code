package Demo3;

import java.util.Comparator;

public class AreaComp implements Comparator<Double> {
    @Override
    public int compare(Double o1, Double o2) {
        return (int)(o1 - o2);
    }
}
