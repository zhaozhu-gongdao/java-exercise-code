package Demo3;

public class Circle extends Shape{
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
        sumArea += this.getArea();
    }

    public Circle(){
        ;
    }

    @Override
    public double getArea() {
        return Math.PI * Math.pow(radius,2);
    }

    public String toString(){
        return String.format("Circle:%.2f",this.getArea());
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
