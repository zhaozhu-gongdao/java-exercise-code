package Demo3;

public class Trangle extends Shape{
    private double a;
    private double b;
    private double c;

    public Trangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
        sumArea += this.getArea();

    }

    public Trangle(){
        ;
    }

    @Override
    public double getArea() {
        double p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    public String toString(){
        return String.format("Trangle:%.2f",this.getArea());
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
