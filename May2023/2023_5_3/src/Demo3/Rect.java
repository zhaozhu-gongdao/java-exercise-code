package Demo3;

public class Rect extends Shape{
    private double width;
    private double len;

    public Rect(double width, double len) {
        this.width = width;
        this.len = len;
        sumArea += this.getArea();
    }

    public Rect(){
        ;
    }

    @Override
    public double getArea() {
        return width * len;
    }

    @Override
    public String toString() {
        return String.format("Rectangle:%.2f",this.getArea());
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
