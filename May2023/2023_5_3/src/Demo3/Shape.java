package Demo3;

public abstract class Shape implements Cloneable{
    public static double sumArea;

    public abstract double getArea();

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
