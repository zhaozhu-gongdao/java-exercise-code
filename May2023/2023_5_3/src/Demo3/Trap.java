package Demo3;

public class Trap extends Shape{
    private double upRow;
    private double bottomRow;
    private double height;

    public Trap(double upRow, double bottomRow, double height) {
        this.upRow = upRow;
        this.bottomRow = bottomRow;
        this.height = height;
        sumArea += this.getArea();

    }

    public Trap(){
        ;
    }

    @Override
    public double getArea() {
        return ((upRow + bottomRow) * height) / 2;
    }

    public String toString(){
        return String.format("Trapezoid:%.2f",this.getArea());
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
