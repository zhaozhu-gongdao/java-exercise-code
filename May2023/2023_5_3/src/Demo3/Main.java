package Demo3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException{
        Scanner scanner = new Scanner(System.in);
        List<Integer> opers = new ArrayList<>();
        List<Shape> shapes = new ArrayList<>();


        int oper = 0;
        while ((oper = scanner.nextInt()) != 0){
            opers.add(oper);
        }

        //先判断会不会出错
        for (int a : opers){
            if (a != 1 && a != 2 && a != 3 && a != 4){
                System.out.println("Wrong Format");
                return;
            }
        }

        for (int i = 0; i < opers.size(); i++) {
            switch(opers.get(i)){
                case 1:
                    double radious = scanner.nextDouble();
                    add(new Circle(radious),shapes);
                    break;
                case 2:
                    double width = scanner.nextDouble();
                    double len = scanner.nextDouble();
                    add(new Rect(width,len),shapes);
                    break;
                case 3:
                    double a = scanner.nextDouble();
                    double b = scanner.nextDouble();
                    double c = scanner.nextDouble();
                    //判断是否合理
                    if (!isTrangleOK(a,b,c)){
                        System.out.println("Wrong Format");
                        return;
                    }
                    add(new Trangle(a,b,c), shapes);
                    break;
                case 4:
                    double upRow = scanner.nextDouble();
                    double bottomRow = scanner.nextDouble();
                    double height = scanner.nextDouble();
                    add(new Trap(upRow,bottomRow,height),shapes);
                    break;
            }
        }

        System.out.println("The original list:");
        for (int i = 0; i < shapes.size(); i++) {
            System.out.print(shapes.get(i));

            if (i != shapes.size() - 1){
                System.out.print(",");
            }
        }

        System.out.println();

        System.out.println("The sorted list:");
        //按照大小排序
        AreaComp areaComp = new AreaComp();
        for (int i = 0; i < shapes.size() - 1; i++) {
            for (int j = 0; j < shapes.size() - 1 - i; j++) {
                int ret = areaComp.compare(shapes.get(j).getArea(),shapes.get(j + 1).getArea());
                if (ret <= 0){
                    Shape tmp = shapes.get(j);
                    shapes.set(j, shapes.get(j + 1));
                    shapes.set(j + 1, tmp);

                }
            }
        }
        for (int i = 0; i < shapes.size(); i++) {
            System.out.print(shapes.get(i));

            if (i != shapes.size() - 1){
                System.out.print(",");
            }
        }
        System.out.println();

        //总面积的输出
        System.out.println("Sun of area:" + String.format("%.2f",Shape.sumArea));
    }

    public static void add(Shape shape, List<Shape> shapes){
        shapes.add(shape);
    }

    public static boolean isTrangleOK(double a, double b, double c){
        if (a + b <= c || a + c <= b || b + c <= a){
            return false;
        }

        return true;
    }


}
