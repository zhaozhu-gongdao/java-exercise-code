package Demo1;

public class Cylinder {
    private double height;
    private Circle circle;

    public Cylinder(double height, Circle circle) {
        this.height = height;
        this.circle = circle;
    }

    public Cylinder(){
        this.circle = new Circle();
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public double getArea(){
        double AreaCircle = this.circle.getArea() * 2;
        double eles = this.circle.getPerimeter() * this.height;
        return AreaCircle + eles;
    }

    public double getVolume(){
        return this.height * this.circle.getArea();
    }

    @Override
    public String toString() {
        return "Cylinder(" +
                "h:" + this.height +
                "," + this.circle +
                ')';
    }
}
