package Demo1;

// 2、无头双向链表实现
public class LinkedList {
    ListNode head;
    ListNode end;

    private class ListNode{
        int val;
        ListNode next;
        ListNode prev;

        public ListNode(int val) {
            this.val = val;
        }
    }

    //头插法
    public void addFirst(int data){
        ListNode node = new ListNode(data);
        if (head == null){
            head = node;
            end = node;
            return;
        }

        node.prev = null;
        node.next = head;
        head.prev = node;
        head = node;
    }

    //尾插法
    public void addLast(int data){

        if (end == null){
            addFirst(data);
            return;
        }

        ListNode node = new ListNode(data);
        end.next = node;
        node.prev = end;
        end = node;
    }

    //任意位置插入,第一个数据节点为0号下标
    public boolean addIndex(int index,int data){
        if (!checkIndex(index)){
            return false;
        }

        if (index == 0){
            addFirst(data);
            return true;
        }

        if (index == size()){
            addLast(data);
            return true;
        }

        ListNode node = new ListNode(data);
        ListNode indexNode = searchNode(index);
        if (indexNode != null){
            node.next = indexNode;
            indexNode.prev.next = node;
            node.prev = indexNode.prev;
            indexNode.prev = node;
            return true;
        }else{
            return false;
        }
    }

    public ListNode searchNode(int index){
        ListNode cur = head;

        if (checkIndex(index)){
            for (int i = 0; i < index; i++) {
                cur = cur.next;
            }
            return cur;
        }else{
            return null;
        }
    }

    public boolean checkIndex(int index){
        if (index < 0 || index > size()){
            System.out.println("下标错误");
            return false;
        }

        return true;
    }

    //查找是否包含关键字key是否在单链表当中
    public boolean contains(int key){
        ListNode cur = head;
        while (cur != null){
            if (cur.val == key){
                return true;
            }

            cur = cur.next;
        }
        return false;
    }

    //删除第一次出现关键字为key的节点
    //删除第一次出现关键字为key的节点
    public void remove(int key){
        ListNode cur = head;
        while (cur != null) {
            if(cur.val == key) {
                //开始删
                if(cur == head) {
                    //删除头节点
                    head = head.next;
                    //只有1个节点
                    if(head != null) {
                        head.prev = null;
                    }else {
                        end = null;
                    }
                }else {
                    cur.prev.next = cur.next;
                    if(cur.next != null) {
                        cur.next.prev = cur.prev;
                    }else {
                        end = end.prev;
                    }
                }
                return;
            }else {
                cur = cur.next;
            }
        }
    }


    //删除所有值为key的节点
    public void removeAllKey(int key){
        ListNode cur = head;
        while (cur != null){
            if (cur.val == key){
                remove(key);
            }

            cur = cur.next;
        }
    }
    //得到单链表的长度
    public int size(){
        ListNode cur = head;
        int count = 0;

        while (cur != null){
            count++;
            cur = cur.next;
        }
        return count;
    }

    public void display(){
        ListNode cur = head;
        StringBuilder ret = new StringBuilder();
        while (cur != null){
            ret.append(cur.val);
            if (cur.next != null){
                ret.append(',');
            }

            cur = cur.next;
        }

        System.out.println(ret);
    }
    public void clear(){
        if (head == null){
            return;
        }

        ListNode cur = head;
        while (cur != null){
            ListNode curNext = cur.next;
            cur.prev = null;
            cur.next = null;
            cur = curNext;
        }

        head = null;
        end = null;
    }

}

class Main{
    public static void main(String[] args) {
        LinkedList test = new LinkedList();
/*        test.addFirst(1);
        test.addFirst(2);
        test.addFirst(3);*/
        test.addLast(4);
        test.addLast(5);
        test.addIndex(2,6);
        test.addIndex(5,7);
        test.addIndex(1,8);
        boolean ret = test.contains(8);
        test.display();
        test.remove(8);
        test.display();
        test.remove(10);
        test.display();
        test.remove(6);
        test.display();
        test.addLast(5);
        test.addLast(5);
        test.addLast(5);
        test.addLast(5);
        test.display();
        test.removeAllKey(5);
        test.display();
        test.clear();
        test.display();

    }
}
