package Demo1;

public class Solution {
    class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }

    public ListNode reverseList(ListNode head) {
        if (head == null){
            return null;
        }

        ListNode cur = head.next;
        head.next = null;

        while (cur != null){
            ListNode curNext = cur.next;
            cur.next = head;
            head = cur;;
            cur = curNext;
        }

        return head;
    }

    public ListNode middleNode(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;

        if (head == null){
            return null;
        }

        while (fast != null && fast.next != null){
            fast = fast.next.next;
            slow = slow.next;
        }

        return slow;

    }

}
