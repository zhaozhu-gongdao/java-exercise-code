package Demo1;

import java.util.LinkedList;
import java.util.Queue;

public class MyQueue {
    Queue<Integer> queue1;
    Queue<Integer> queue2;

    public MyQueue() {
        queue1 = new LinkedList<>();
        queue2 = new LinkedList<>();
    }

    public void push(int x) {
        if (!queue1.isEmpty()){
            queue1.offer(x);
        }else if (!queue2.isEmpty()){
            queue2.offer(x);
        }else{
            queue1.offer(x);
        }
    }

    public int top() {
        int x = 0;
        if (!queue1.isEmpty()){
            while (!queue1.isEmpty()){
                x = queue1.poll();
                queue2.offer(x);
            }

        }else if (!queue2.isEmpty()){
            while (!queue2.isEmpty()){
                x = queue2.poll();
                queue1.offer(x);
            }
        }else{
            return -1;
        }

        return x;
    }

    public int pop() {
        if (!queue1.isEmpty()){
            int size = queue1.size();
            for (int i = 0; i < size - 1; i++){
                int x = queue1.poll();
                queue2.offer(x);
            }
            return queue1.poll();

        }else if (!queue2.isEmpty()){
            int size = queue2.size();
            for (int i = 0; i < size - 1; i++){
                int x = queue2.poll();
                queue1.offer(x);
            }

            return queue2.poll();
        }else{
            return -1;
        }

    }

    public boolean empty() {
        return queue1.isEmpty() && queue2.isEmpty();
    }
}


class Main{
    public static void main(String[] args) {
        //["MyStack","push","push","top","pop","empty"]
        MyQueue test = new MyQueue();
        test.push(1);
        test.push(2);
        test.push(3);
        test.push(4);
        test.push(5);

        test.top();
        int ret = test.pop();
        System.out.println(ret);
    }
}
