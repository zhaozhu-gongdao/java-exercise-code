package Demo1;

/*public class MyCircularQueue {
    private int[] elem;
    private int font;
    private int rear;

    public MyCircularQueue(int k) {
        elem = new int[k];
    }

    //入队
    public boolean enQueue(int value) {
        if (isFull()){
            return false;
        }

        elem[rear] = value;
        rear = (rear + 1) % elem.length;
        return true;
    }

    //出队
    public boolean deQueue() {
        if (isEmpty()){
            return false;
        }

        font = (font + 1) % elem.length;
        return true;
    }


    public int Front() {
        return font;
    }

    public int Rear() {
        return rear;
    }

    public boolean isEmpty() {
        return font == rear;
    }

    public boolean isFull() {
        return (rear + 1) % elem.length == font;
    }
}*/


public class MyCircularQueue {
    private int[] elem;
    private int front;//队头下标
    private int rear;//队尾下标

    public MyCircularQueue(int k) {
        this.elem = new int[k+1];
    }

    //入队
    public boolean enQueue(int value) {
        if(isFull()) {
            return false;
        }
        elem[rear] = value;
        rear = (rear+1) % elem.length;
        return true;
    }

    //出队
    public boolean deQueue() {
        if(isEmpty()) {
            return false;
        }
        front = (front+1) % elem.length;
        return true;
    }

    //得到队头元素
    public int Front() {
        if(isEmpty()) {
            return -1;
        }
        return elem[front];
    }
    //得到队尾元素
    public int Rear() {
        if(isEmpty()) {
            return -1;
        }
        int index = (rear == 0) ? elem.length-1 : rear-1;
        return elem[index];
    }

    public boolean isEmpty() {
        return rear == front;
    }

    public boolean isFull() {
        return (rear+1) % elem.length == front;
    }
}

class Main2{
    public static void main(String[] args) {
        MyCircularQueue test = new MyCircularQueue(3);
        boolean ret1 = test.enQueue(1);
        boolean ret2 = test.enQueue(2);
        boolean ret3 = test.enQueue(3);
        boolean ret4 = test.enQueue(4);
        boolean ret5 = test.enQueue(5);

    }
}
