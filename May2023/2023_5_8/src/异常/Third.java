package 异常;

import java.util.Scanner;

//7-3
public class Third{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n= sc.nextInt();
        double[] arr=new double[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i]= sc.nextDouble();
        }
        while (sc.hasNextDouble()){
            try{
                System.out.println(ArrayUtils.findMax(arr, sc.nextInt(), sc.nextInt()));
            }catch (Exception e){
                System.out.println(e.toString());
            }
        }
        try {
            System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class,int.class,int.class));
        } catch (Exception e1) {
        }
        sc.close();
    }
}

class  ArrayUtils{
    public static double findMax(double[] arr,int begin, int end) throws IllegalArgumentException{
        if (!(begin<end))throw new IllegalArgumentException("begin:"+begin+" >= end:"+end);
        else if (!(begin>=0))throw new IllegalArgumentException("begin:"+begin+" < 0");
        else if (!(end<=arr.length))throw new IllegalArgumentException("end:"+end+" > arr.length");
        double max=0;
        for (int i=begin;i<end;i++){
            max=Math.max(arr[i],max);
        }
        return max;
    }
}
