package 异常;

import java.util.Scanner;

//7-1
public class First {
    public static void main(String[] args) {
        int []arr = new int [5];
        Scanner input = new Scanner(System.in);
        while (input.hasNext()){
            String  a = input.next();
            if (a.equals("arr")){
                int b = input.nextInt();
                try {
                    int d = arr[b];
                }catch (Exception e){
                    System.out.println(e.toString());
                }
            }else if (a.equals("num")){
                String  b = input.next();
                try{
                    int d = Integer.parseInt(b);
                }catch (Exception e){
                    System.out.println(e.toString());
                }
            }else if (a.equals("cast")){
                try {
                    Object str = new String("cc");
                    System.out.println((Integer)(str));
                }catch (Exception e){
                    System.out.println(e.toString());
                }
            }else if (a.equals("null")){
                try {
                    String t = null;
                    int l = t.length();
                }catch (Exception e){
                    System.out.println(e.toString());
                }
            }else {
                System.exit(0);
            }

        }
    }
}