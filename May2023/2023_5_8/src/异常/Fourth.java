package 异常;

//7-4
import java.util.Scanner;

class IllegalScoreException extends RuntimeException {
    String message;
    public IllegalScoreException() {
    }

    public IllegalScoreException(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "IllegalScoreException: score out of range, score=" + message;
    }
}

class IllegalNameException extends RuntimeException {
    String message;
    public IllegalNameException() {
    }

    public IllegalNameException(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "IllegalNameException: the first char of name must not be digit, name=" + message;
    }
}

class Student {
    private String name;
    private int score;

    public Student(){
        this.score = 0;
    }

    @Override
    public String toString() {
        return "Student [" +
                "name=" + name +
                ", score=" + score +
                ']';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.charAt(0) >= '0' && name.charAt(0) <= '9'){
            throw new IllegalNameException(name);
        }
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int addScore(int score) throws IllegalScoreException{
        if (this.score + score < 0||this.score + score > 100){
            throw new IllegalScoreException(Integer.toString(this.score + score));
        }
        setScore(this.score + score);
        return this.score;
    }
}

public class Fourth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            String str = scanner.next();
            if (!str.equals("new")){
                scanner.close();
                System.out.println("scanner closed");
                break;
            }
            Student stu = new Student();
            scanner.nextLine();
            String nameAndScore = scanner.nextLine();
            String[] arr = nameAndScore.split("\\s+");
            try{
                String name = arr[0];
                int score = Integer.parseInt(arr[1]);
                stu.setName(name);
                stu.addScore(score);
                System.out.println(stu);
            }catch (IllegalNameException | IllegalScoreException e){
                System.out.println(e);
            }catch (Exception e){
                System.out.println("java.util.NoSuchElementException");
            }
        }
    }
}
