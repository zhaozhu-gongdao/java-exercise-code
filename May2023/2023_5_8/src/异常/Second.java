package 异常;

//7-2
import java.util.*;
public class Second {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int []a = new int [n];
        int num = 0;
        for(int i = 0;i < n;i++)
        {
            String s = sc.next();
            try {
                int t = Integer.parseInt(s);
                a[num++] = t;
            }catch(Exception e)
            {
                System.out.println("java.lang.NumberFormatException: For input string: \""+ s +"\"");
                i--;
            }
        }
        System.out.println(Arrays.toString(a));
        sc.close();
    }
}