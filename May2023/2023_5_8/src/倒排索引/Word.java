package 倒排索引;
import java.util.Arrays;

public class Word {
    private String name;
    private int[] line;
    private int useSize;

    public Word(String name) {
        this.name = name;
        line = new int[5];
    }

    public void setLine(int val) {
        if (useSize == line.length){
            line = Arrays.copyOf(line,line.length * 2);
        }

        this.line[useSize++] = val;
    }

    @Override
    public String toString() {
        return Arrays.toString(line);
    }

    public String getName() {
        return name;
    }

    public int getUseSize() {
        return useSize;
    }

    public int[] getLine() {
        return line;
    }
}
