package 倒排索引;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {   public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Word> wordList = new ArrayList<>();
        List<String> list = new ArrayList<>();
        creat(wordList, list, scanner);
        //排序
        sort(wordList);
        //打印全部
        printAll(wordList);
        search(wordList, list, scanner);


    }

    public static void creat(List<Word> wordList, List<String> list,Scanner scanner){
        int lineCount = 1;

        while (true){
            String str = scanner.nextLine();
            list.add(str);
            String[] words = str.split(" ");

            //得到每一个单词
            boolean flag = true;
            for (int i = 0; i < words.length; i++) {
                //退出条件
                if (words[i].equals("!!!!!")){
                    flag = false;
                    break;
                }


                Word word = new Word(words[i]);
                if (wordList.size() == 0){
                    word.setLine(lineCount);
                    wordList.add(word);
                    continue;
                }

                boolean flag2 = false;
                int index = 0;
                for (int j = 0; j < wordList.size(); j++) {
                    if (wordList.get(j).getName().equals(words[i])) {
                        flag2 = true;
                        index = j;
                        break;
                    }
                }

                if (flag2 == false){
                    word.setLine(lineCount);
                    wordList.add(word);
                }else{
                    //看是否重复
                    if (isRe(lineCount, wordList.get(index))){
                        wordList.get(index).setLine(lineCount);
                    }
                }
            }
            if(!flag){
                break;
            }
            lineCount++;
        }

    }

    public static void sort(List<Word> wordList){
        for (int i = 0; i < wordList.size() - 1; i++) {
            for (int j = 0; j < wordList.size() - 1 - i; j++) {
                String com1 = wordList.get(j).getName();
                String com2 = wordList.get(j + 1).getName();
                if (com1.compareTo(com2) > 0){
                    //交换wordList中的位置
                    Word tmp = wordList.get(j);
                    wordList.set(j, wordList.get(j + 1));
                    wordList.set(j + 1,tmp);
                }
            }
        }
    }

    public static void printAll(List<Word> wordList){
        for (int i = 0; i < wordList.size(); i++) {
            System.out.print(wordList.get(i).getName() + "[");
            for (int j = 0; j < wordList.get(i).getUseSize(); j++) {
                int[] line = wordList.get(i).getLine();
                System.out.print(line[j]);

                if (j != wordList.get(i).getUseSize() - 1){
                    System.out.print(",");
                }
            }
            System.out.print("]");
            System.out.println();
        }
    }

    public static void search(List<Word> wordList, List<String> list,Scanner scanner){
        while (scanner.hasNext()){
            String searchStr = scanner.nextLine();
            boolean flag = false;
            for(Word s : wordList){
                if (s.getName().equals(searchStr)){
                    System.out.println(Arrays.toString(s.getLine()));

                    for (int i = 0; i < s.getUseSize(); i++) {
                        int[] line = s.getLine();
                        int index = line[i];
                        System.out.println("line" + index + ":" + list.get(index));
                    }

                    flag = true;

                }
            }

            if (!flag){
                System.out.println("found 0 results");
            }
        }
    }

    public static boolean isRe(int linenum, Word word){
        int[] line = word.getLine();
        for(int x : line){
            if (x == linenum){
                return false;
            }
        }
        return true;
    }
}




