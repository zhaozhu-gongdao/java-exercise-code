package 图形;

//7-21
import java.util.Scanner;

public class newShape {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double longRadious = scanner.nextDouble();
        double shortRadius = scanner.nextDouble();

        Oval oval = new Oval(longRadious,shortRadius);
        System.out.println("The area of " + oval + " is " + oval.area());
        System.out.println("The perimeterof " + oval + " is " + oval.perimeter());
    }
}


class Oval extends Shape{
    private double longRadius;
    private double shortRadius;

    public Oval(double longRadius, double shortRadius) {
        this.longRadius = longRadius;
        this.shortRadius = shortRadius;
    }

    public Oval(){
        ;
    }

    @Override
    public double area() {
        return PI * this.longRadius * this.shortRadius;
    }

    @Override
    public double perimeter() {
        return 2 * PI * Math.sqrt((shortRadius * shortRadius +
                longRadius * longRadius) / 2);
    }

    @Override
    public String toString() {
        return "Oval(" +
                "a:" + longRadius +
                ",b:" + shortRadius +
                ')';
    }
}


abstract class Shape {
    protected double PI = 3.1415926;

    public abstract double area();

    public abstract double perimeter();
}
