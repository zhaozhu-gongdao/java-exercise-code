package 统计个数;

//7-2
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //读取
        int count = 0;
        String oper = null;
        List<Word> words = new ArrayList<>();
        while (!"!!!!!".equals(oper = scanner.next())){
            //看里面是否存在
            oper = oper.replace("!","");
            oper = oper.replace(".","");
            oper = oper.replace(",","");
            oper = oper.replace(":","");
            oper = oper.replace("*","");
            oper = oper.replace("?","");
            oper = oper.toLowerCase();
            boolean flag = true;
            for(Word s : words){
                if(s.getName().compareTo(oper) == 0){
                    s.setCount(s.getCount() + 1);
                    flag = false;
                    break;

                }
            }

            if (flag){
                Word word = new Word(oper);
                words.add(word);
                count++;
            }
        }

        System.out.println(count);
        //重新排序
        com(words);
        //打印
        show(words);

    }

    public static void com(List<Word> words){
        for (int i = 0; i < words.size() - 1; i++) {
            for (int j = 0; j < words.size() - 1 - i; j++) {
                if (words.get(j).getCount() < words.get(j + 1).getCount()){
                    Word word = words.get(j);
                    words.set(j,words.get(j + 1));
                    words.set(j + 1, word);
                }else if (words.get(j).getCount() == words.get(j + 1).getCount()){
                    int ret = words.get(j).getName().compareTo(words.get(j + 1).getName());
                    if (ret > 0){
                        Word word = words.get(j);
                        words.set(j,words.get(j + 1));
                        words.set(j + 1, word);
                    }
                }
            }
        }
    }

    public static void show(List<Word> words){
        for (int i = 0; i < 10; i++) {
            System.out.println(words.get(i));
        }
    }
}


class Word {
    private String name;
    private int count = 1;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Word(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name + "=" + this.count;
    }
}