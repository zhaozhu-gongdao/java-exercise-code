package 倒排索引2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class pList {
    List<Word> allWord = new ArrayList<>();
    List<String> rowWord = new ArrayList<>();

    public void creat(Scanner scanner, List<Word> allWord, List<String> rowWord){

        int line = 1;
        while(true){

            String str = scanner.nextLine();
            if (str.equals("!!!!!")){
                break;
            }

            rowWord.add(str);

            //当前的一行数字，抽出来的
            String[] word = str.split(" ");

            //抽出单个单词，看看这个单词里面是否和里面中的name相等
            for (int i = 0; i < word.length; i++) {
                boolean flag = true;
                for(Word s : allWord){
                    if (s.getName().equals(word[i])){
                        //相等
                        flag = false;

                        //操作
                        boolean fare = true;

                        for(Integer tmp : s.getLine()){
                            //行是一样的情况，不管
                            if (tmp == line){
                                fare = false;
                                break;
                            }
                        }
                        //这个单词行不一样
                        if (fare){
                            s.getLine().add(line);
                        }

                        break;
                    }
                }
                //不相等
                if (flag){
                    Word tmp = new Word(word[i], line);
                    allWord.add(tmp);
                }
            }

            line++;
        }
    }

    public void print(List<Word> wordList){
        for(Word s : wordList){
            System.out.println(s);
        }
    }

    public void sort(List<Word> wordList){
        //根据每个单词的名字排序
        for (int i = 0; i < wordList.size() - 1; i++) {
            for (int j = 0; j < wordList.size() - 1 - i; j++) {
                String name1 = wordList.get(j).getName();
                String name2 = wordList.get(j + 1).getName();
                if (name1.compareTo(name2) > 0){
                    Word tmp = wordList.get(j);
                    wordList.set(j, wordList.get(j + 1));
                    wordList.set(j + 1, tmp);
                }
            }
        }
    }

    public void search(Scanner scanner, List<String> rowWord){
        while(scanner.hasNextLine()){
            String searchStr = scanner.nextLine();
            String[] searchWord = searchStr.split(" ");
            List<Integer> lineNum = new ArrayList<>();


            for (int i = 0; i < rowWord.size(); i++) {
                //这一行
                String line = rowWord.get(i);
                //看看每一行当个要查找的单词，是否在同一列
                int count = 0;
                for(String s : searchWord){
                    //当前查的单词
                    if (line.contains(s)){
                        count++;
                    }
                }

                if (count == searchWord.length){
                    lineNum.add(i + 1);
                }
            }

            //打印上面
            if(lineNum.isEmpty()){
                System.out.println("found 0 results");
            }else{
                System.out.println(lineNum);

                //每一行的内容打印
                for(Integer s : lineNum){
                    System.out.println("line" + s + ":" + rowWord.get(s - 1));
                }
            }
        }
    }
}
