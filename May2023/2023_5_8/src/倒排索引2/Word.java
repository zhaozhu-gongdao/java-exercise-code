package 倒排索引2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Word {
    private String name;
    private List<Integer> line = new ArrayList<>();

    public Word(String name, int cnt) {
        this.name = name;
        line.add(cnt);
    }

    public Word(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getLine() {
        return line;
    }

    @Override
    public String toString() {
        return this.name + "=" + line.toString();
    }
}
