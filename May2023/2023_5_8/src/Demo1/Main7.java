package Demo1;

import java.util.*;
public class Main7{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double s1 = input.nextDouble();
        double s2 = input.nextDouble();
        double s3 = input.nextDouble();
        try {
            Triangle t = new Triangle(s1,s2,s3);
            System.out.println(t);
        }
        catch (IllegalTriangleException ex) {
            System.out.println(ex.getMessage());
        }
    }
}

class Triangle {
    private double side1;
    private double side2;
    private double side3;

    public Triangle(double s1, double s2, double s3) throws IllegalTriangleException {

        if(!((s1 + s2) > s3 && (s1 + s3) > s2 && (s2 + s3) > s1)) {
            throw new IllegalTriangleException("Invalid: " + s1 + "," + s2 +"," + s3);
        } else {
            side1 = s1;
            side2 = s2;
            side3 = s3;
        }
    }

    @Override
    public String toString() {
        return "Triangle [side1=" + side1 + ", side2=" + side2 + ", side3=" + side3 + "]";
    }

}

class IllegalTriangleException extends Exception {
    public IllegalTriangleException() {}
    public IllegalTriangleException(String sq) {
        super(sq);
    }
}

