package Demo1;

import java.util.*;

class PersonSortable implements Comparable{
    private String name;
    private int age;

    public PersonSortable() {
        this.name = name;
        this.age = age;
    }

    public PersonSortable(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return name+"-"+age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(Object o) {
        PersonSortable per=(PersonSortable)o;
        int x = this.name.compareTo(per.name);
        if(x!=0){
            return x;
        }else{
            if (this.age>per.age)
                return 1;
            else if(this.age<per.age)
                return -1;
            else
                return 0;
        }
    }
}
public class Main3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int count=Integer.valueOf(sc.nextLine());
        ArrayList<PersonSortable> list=new ArrayList();
        for (int i=0;i<count;i++){
            String string[]=sc.nextLine().split(" ");
            list.add(new PersonSortable(string[0],Integer.valueOf(string[1])));
        }
        Collections.sort(list);
        for (PersonSortable p:list){
            System.out.println(p);
        }
        System.out.println(Arrays.toString(PersonSortable.class.getInterfaces()));
    }
}
