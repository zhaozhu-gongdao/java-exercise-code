package Demo1;

import java.util.Scanner;

/*public class Main1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            if (a > 1000 || a < -1000){
                System.out.println("|a|>1000");
            }else{
                System.out.println(a + b);
            }
        }
    }
}*/


public class Main1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()){
            String str = scanner.nextLine();
            char[] charStr = str.toCharArray();

            int sum = 0;
            for(char s : charStr){
                if (s == '-' || s == '.'){
                    continue;
                }else{
                    String tmp = String.valueOf(s);
                    sum += Integer.parseInt(tmp);
                }
            }

            System.out.println(sum);
        }
    }
}