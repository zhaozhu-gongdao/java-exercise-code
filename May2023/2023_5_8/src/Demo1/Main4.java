package Demo1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Main4 {
    public static void main(String[] args) throws InterruptedException {
        Scanner sc=new Scanner(System.in);
        List<Person> personList=new ArrayList<Person>();
        String name;
        int age;
        boolean gender;
        String stuNo;
        String clazz;
        String companyName;
        Company company;
        double salary;
        //输入
        while(true) {
            String t=sc.next();
            if(t.equals("s")) {
                name=sc.next();
                age=sc.nextInt();
                gender=sc.nextBoolean();
                stuNo=sc.next();
                clazz=sc.next();
                if(name==null||stuNo==null||clazz==null) {
                    continue;
                }
                personList.add(new Student(name, age, gender, stuNo, clazz));

            }else if(t.equals("e")){
                name=sc.next();
                age=sc.nextInt();
                gender=sc.nextBoolean();
                salary=sc.nextDouble();
                companyName=sc.next();
                company=new Company(companyName);
                if(name==null) {
                    continue;
                }
                //除了companyName可以为空，其他为空均不能创建对象（应该是这么理解吧）
                if(companyName==null) {
                    companyName="null";   //为空的话要给他赋值为空（删了也能过 可能写了句废话 ）
                }
                personList.add(new Employee(name, age, gender, company, salary));
            }else{         //要是遇到感叹号就该结束输入啦（题目上好像没说）
                break;
            }

        }
        //排序（因为之前的类都建好了，Person类还是抽象类，所以比较器只能用Comparator了）
        Collections.sort(personList, new Name_AgeComparator());

        for(int i=0;i<personList.size();i++) {

            System.out.println(personList.get(i).toString());

        }
        //只要不输入return和exit就要分组啦
        String str=sc.next();
        while(true) {
            if(str.equals("return")||str.equals("exit")) {
                break;
            }else {
                //分组
                List<Person> stuList=new ArrayList<Person>();
                List<Person> empList=new ArrayList<Person>();
                //判断过程要根据equals 是自己定义的 所以比较工资那有坑，用contains是不行的 所以还是循环比较吧
                boolean flag1=true;
                boolean flag2=true;
                for(int i=0;i<personList.size();i++) {
                    if(personList.get(i).toString().indexOf("Student")>=0) {
                        if(stuList.size()==0) {
                            stuList.add(personList.get(i));
                        }
                        for(int j=0;j<stuList.size();j++) {
                            if(personList.get(i).equals(stuList.get(j))){
                                flag1=false;
                            }
                        }
                        if(flag1) {
                            stuList.add(personList.get(i));

                        }
                        flag1=true;
                    }else {
                        if(empList.size()==0) {
                            empList.add(personList.get(i));
                        }
                        for(int j=0;j<empList.size();j++) {
                            if(personList.get(i).equals(empList.get(j))){
                                flag2=false;
                            }
                        }
                        if(flag2) {
                            empList.add(personList.get(i));
                        }
                        flag2=true;
                    }
                }
                System.out.println("stuList");
                for(int i=0;i<stuList.size();i++) {

                    System.out.println(stuList.get(i).toString());

                }
                System.out.println("empList");
                for(int i=0;i<empList.size();i++) {

                    System.out.println(empList.get(i).toString());

                }
                break;
            }
        }



    }
    //Comparator需要创建一个类，又因为想在main方法里直接调，所以就要用static修饰
    static class Name_AgeComparator implements Comparator<Person>{  //不加这个泛型也可以，但以后要强制转换

        @Override
        public int compare(Person o1, Person o2) {
            if(o1.name.compareTo(o2.name)==0) {
                if(o1.age==o2.age) {
                    return 0;
                }else if(o1.age<o2.age) {
                    return -1;
                }else {
                    return 1;
                }
            }else {
                //比较字符串的方法（放张图片吧）
                return(o1.name.compareTo(o2.name));
            }
        }

    }


}
abstract class Person{
    String name;
    int age;
    boolean gender;
    public Person(String name, int age, boolean gender) {
        super();
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
    @Override
    public String toString() {

        return this.name+'-'+String.valueOf(this.age)+'-'+String.valueOf(this.gender);
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Person other = (Person) obj;
        if (age != other.age)
            return false;
        if (gender != other.gender)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

}
class Student extends Person{
    String stuNo;
    String clazz;
    public Student(String name, int age, boolean gender, String stuNo, String clazz) {
        super(name,age,gender);
        this.clazz=clazz;
        this.stuNo=stuNo;
    }
    @Override
    public String toString() {
        return"Student:"+ super.toString()+'-'+this.stuNo+'-'+this.clazz;
    }
    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj))
            return false;
        if (this == obj)
            return true;
        if (getClass() != obj.getClass())
            return false;
        Student other = (Student) obj;
        if (clazz == null) {
            if (other.clazz != null)
                return false;
        } else if (!clazz.equals(other.clazz))
            return false;
        if (stuNo == null) {
            if (other.stuNo != null)
                return false;
        } else if (!stuNo.equals(other.stuNo))
            return false;
        return true;
    }


}
class Company{
    String name;
    public Company(){

    }
    public Company(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return name;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Company other = (Company) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }


}
class Employee extends Person{
    Company company;
    double salary;
    public Employee(String name, int age, boolean gender, Company company, double salary) {
        super(name, age, gender);
        this.company = company;
        this.salary = salary;
    }
    @Override
    public String toString() {
        return "Employee:"+ super.toString()+'-'+company+'-'+salary;
    }
    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj))
            return false;
        if (this == obj)
            return true;
        if (getClass() != obj.getClass())
            return false;
        Employee other = (Employee) obj;
        if (company == null) {
            if (other.company != null)
                return false;
        }
        else if (!company.equals(other.company))
            return false;
        //坑在这啊，要用decimal比较，要学会decimal这种用法
        DecimalFormat df = new DecimalFormat("#.#");
        if (!df.format(salary) .equals( df.format(other.salary)))
            return false;
        return true;
    }
}
