package Demo1;

import java.util.Scanner;


class Line{
    public Point p1;
    public Point p2;

    public Line(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }


    public Point getP1() {
        return p1;
    }

    public void setP1(Point p1) {
        this.p1 = p1;
    }

    public Point getP2() {
        return p2;
    }

    public void setP2(Point p2) {
        this.p2 = p2;
    }

    public int getLength(){    //不确定
        return -1;
    }

    @Override
    public String toString() {
        return "Line [" +
                "p1=" + p1.toString() +
                ", p2=" + p2.toString() +
                ']';
    }
}

class Point{
    private int x, y;// x,y为点的坐标
    //求两点之间的距离
    public double distance(Point p1) {
        return Math.sqrt((p1.x -this.x)*(p1.x -this.x)+(p1.y-this.y)*(p1.y-this.y));
    }
    public Point(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public Point() {
        super();
        x = y =0;
    }
    @Override
    public String toString() {
        return "Point [x=" + x + ", y=" + y + "]";
    }
}


public class Main6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int linesLen = scanner.nextInt();
        Line[] lines = new Line[linesLen];

        for (int i = 0; i < linesLen; i++) {
            int point1X = scanner.nextInt();
            int point1Y = scanner.nextInt();
            int point2X = scanner.nextInt();
            int point2Y = scanner.nextInt();
            Point point1 = new Point(point1X,point1Y);
            Point point2 = new Point(point2X,point2Y);

            Line line = new Line(point1, point2);
            lines[i] = line;
        }

        //循环输出
        for (int i = 0; i < linesLen; i++) {
            System.out.println(lines[i]);
            System.out.format("此线段的长度为：%.1f",lines[i].p1.distance(lines[i].p2));
            System.out.println();
        }
    }
}
