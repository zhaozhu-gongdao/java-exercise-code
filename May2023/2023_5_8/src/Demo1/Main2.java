package Demo1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
public class Main2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        PersonSortable2[] ps2 = new PersonSortable2[n];
        for (int i = 0; i < n; i++) {
            String name = sc.next();
            int age = sc.nextInt();
            ps2[i] = new PersonSortable2(name,age);
        }
        Arrays.sort(ps2, new NameComparator());
        System.out.println("NameComparator:sort");
        for (PersonSortable2 p:ps2){
            System.out.println(p.toString());
        }
        Arrays.sort(ps2,new AgeComparator());
        System.out.println("AgeComparator:sort");
        for (PersonSortable2 p1:ps2){
            System.out.println(p1.toString());
        }
        System.out.println(Arrays.toString(NameComparator.class.getInterfaces()));
        System.out.println(Arrays.toString(AgeComparator.class.getInterfaces()));

    }
}
class PersonSortable2{
    private String name;
    private int age;

    public PersonSortable2(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return name + "-" + age;
    }
}
class AgeComparator implements Comparator<PersonSortable2> {
    @Override
    public int compare(PersonSortable2 o1, PersonSortable2 o2) {
        return o1.getAge()-o2.getAge();
    }
}
class NameComparator implements Comparator<PersonSortable2>{

    @Override
    public int compare(PersonSortable2 p1, PersonSortable2 p2) {
        return p1.getName().compareTo(p2.getName());
    }
}
