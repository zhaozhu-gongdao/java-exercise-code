package Demo1;

import java.util.Random;
import java.util.Scanner;

public class Main5 {

    public static void main (String [] args) {

        Scanner scanner = new Scanner(System.in);
        long seed = scanner.nextLong();
        int n = scanner.nextInt();
        //将随机数种子放入随机数中
        Random random = new Random(seed);

        int insideNum = 0;

        for (int i = 0; i < n; i++) {

            //random.nextDouble()的值域[0.0, 1.0)。要使函数为[-1.0, 1.0)，所以进行了下面的操作。
            double x = random.nextDouble() *2 - 1;
            double y = random.nextDouble() *2 - 1;

            //记录点在圆内的数量
            if (Math.pow(x, 2) + Math.pow(y, 2) <= 1) {
                insideNum++;
            }
        }
        //从所给的公式 反推出PI的公式
        //需要把insideNum强转成double，不然整数相除会出错
        System.out.println(4 * ((double) insideNum / n));
    }
}


