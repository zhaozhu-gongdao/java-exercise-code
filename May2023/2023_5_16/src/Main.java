/*class Solution {
    public static int missingNumber(int[] nums) {
        int errorSum = 0;
        int rightSum = 0;
        for (int i = 0; i < nums.length; i++){
            errorSum += nums[i];
            rightSum += i + 1;
        }

        return rightSum - errorSum;
    }
}*/


class Solution {
    public static void reverse(int[] nums, int left, int right){
        while(left < right){
            int tmp = nums[left];
            nums[left] = nums[right];
            nums[right] = tmp;

            left++;
            right--;
        }
    }

    public static void rotate(int[] nums, int k) {
        if (k > nums.length){
            k %= nums.length;
        }

        reverse(nums, 0, nums.length - 1 - k);
        reverse(nums, nums.length - k, nums.length - 1);
        reverse(nums, 0, nums.length - 1);
    }
}
public class Main {
    public static void main(String[] args) {
        /*int[] arr = {1,2,3,4,5,6,7};
        Solution.rotate(arr, 3);*/

        PalindromeList.creat();
        //PalindromeList.chkPalindrome();
    }

}


