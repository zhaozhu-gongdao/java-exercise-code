import java.util.Scanner;
import java.util.Stack;

/*
public class Main {
    public static int isSameTwo(String s){
        char[] newS = s.toCharArray();
        for (int i = 0; i < newS.length - 2; i++) {
            if (newS[i] == newS[i + 1]){
                return i;
            }
        }

        return -1;
    }

    public static String delect(String s){
        char[] newS = s.toCharArray();
        while (isSameTwo(s) != -1){
            int index = isSameTwo(s);
            for (int i = index; i < newS.length - 2; i++) {
                newS[i] = newS[i + 2];
            }
        }

        return newS.toString();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String ret = delect(scanner.nextLine());
        System.out.println(ret);
    }
}
*/


public class Main{
    public static void main(String[] args) {
        Stack<Character> stack = new Stack();
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        for(int i = 0; i<str.length();i++){
            if(stack.empty()){
                stack.push(str.charAt(i));
            }else if(str.charAt(i) == stack.peek()){
                stack.pop();
            }else {
                stack.push(str.charAt(i));
            }
        }

        if (stack.empty())
            System.out.println(0);
        String s = "";
        while(!stack.empty()){
            s = s + stack.pop();
        }
        for (int i = s.length()-1; i >= 0; i--) {
            System.out.print(s.charAt(i));
        }
    }
}
