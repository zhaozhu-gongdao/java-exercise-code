import java.util.Locale;

public class Solution {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param S string字符串
     * @param T string字符串
     * @return bool布尔型
     */
/*    public boolean isSubsequence (String S, String T) {
        // write code here
        if (S == null){
            return false;
        }

        if (S != null && T == null || S == null && T != null){
            return false;
        }

        char[] s1 = S.toCharArray();
        char[] s2 = T.toCharArray();

        int index = 0;
        for (int i = 0; i < s1.length; i++){
            if (s1[i] == s2[0]){
                index = i;
                break;
            }
        }

        int j = 0;
        for (int i = index; i < s1.length; i++) {
            if (s1[i] != s2[j++]){
                return false;
            }
        }

        return true;
    }*/

/*    public boolean isSubsequence (String S, String T) {
        // write code here
        if (S == null){
            return false;
        }

        if (S != null && T == null || S == null && T != null){
            return false;
        }


        if (!S.contains(T)){
            return false;
        }


        return true;
    }*/

    public int isSameTwo(String s){
        char[] newS = s.toCharArray();
        for (int i = 0; i < newS.length - 2; i++) {
            if (newS[i] == newS[i + 1]){
                return i;
            }
        }

        return -1;
    }

    public String delect(String s){
        char[] newS = s.toCharArray();
        while (isSameTwo(s) != -1){
            int index = isSameTwo(s);
            for (int i = index; i < newS.length - 2; i++) {
                newS[i] = newS[i + 2];
            }
        }

        return newS.toString();
    }
}

/*class Main {
    public static void main(String[] args) {
        Solution test = new Solution();
        //boolean ret = test.isSubsequence("nowcoder","nowcoder");
        //System.out.println(ret);
    }
}*/
