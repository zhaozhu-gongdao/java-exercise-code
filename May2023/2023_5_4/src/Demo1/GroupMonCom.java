package Demo1;

import java.util.Comparator;

public class GroupMonCom implements Comparator<Employee> {
    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.getGroupMoney() - o2.getGroupMoney();
    }
}
