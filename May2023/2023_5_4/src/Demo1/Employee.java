package Demo1;

public class Employee implements Comparable<Employee>{
    private int id;
    private String name;
    private String sex;
    private int groupMoney;
    private int privateMoney;

    public Employee(int id, String name, String sex, int groupMoney, int privateMoney) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.groupMoney = groupMoney;
        this.privateMoney = privateMoney;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getGroupMoney() {
        return groupMoney;
    }

    public void setGroupMoney(int groupMoney) {
        this.groupMoney = groupMoney;
    }

    public int getPrivateMoney() {
        return privateMoney;
    }

    public void setPrivateMoney(int privateMoney) {
        this.privateMoney = privateMoney;
    }

    @Override
    public int compareTo(Employee o) {
        return this.groupMoney + this.privateMoney - o.groupMoney - o.privateMoney;
    }

    @Override
    public String toString() {
        return   id +
                "," + groupMoney +
                "," + privateMoney + "," +
                "," + name + "," + sex;
    }
}
