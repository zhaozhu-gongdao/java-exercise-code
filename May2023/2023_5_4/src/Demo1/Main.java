package Demo1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        titlePrint();
        //创建对象
        List<Employee> employees = new ArrayList<>();
        employees = creatEmployee(employees);

        //总业绩比较
        sumMonCom(employees);
        //打印
        printEmployees(employees);

        //团险总业绩比较
        titlePrint();
        groupMonCOm(employees);
        //打印
        printEmployees(employees);


    }

    public static void titlePrint(){
        System.out.println("编号,团险,个险,姓名,性别");
    }

    public static List<Employee> creatEmployee(List<Employee> employees){
        Employee emp1 = new Employee(1,"职工1","female",500,400);
        Employee emp2 = new Employee(2,"职工2","female",400,600);
        Employee emp3 = new Employee(3,"职工3","male",600,300);
        Employee emp4 = new Employee(4,"职工4","female",800,200);
        Employee emp5 = new Employee(5,"职工5","male",500,700);
        employees.add(emp1);
        employees.add(emp2);
        employees.add(emp3);
        employees.add(emp4);
        employees.add(emp5);

        return employees;
    }

    public static void sumMonCom(List<Employee> employees){
        for (int i = 0; i < employees.size(); i++) {
            for (int j = 0; j < employees.size() - 1 - i; j++) {
                if (employees.get(j).compareTo(employees.get(j + 1)) > 0){
                    Employee tmp = employees.get(j);
                    employees.set(j,employees.get(j + 1));
                    employees.set(j + 1, tmp);
                }
            }
        }
    }

    public static void printEmployees(List<Employee> employees){
        for (int i = 0; i < employees.size(); i++) {
            System.out.println(employees.get(i));
        }
    }

    public static void groupMonCOm(List<Employee> employees){
        GroupMonCom com = new GroupMonCom();
        for (int i = 0; i < employees.size(); i++) {
            for (int j = 0; j < employees.size() - 1 - i; j++) {
                if (com.compare(employees.get(j), employees.get(j + 1)) > 0){
                    Employee tmp = employees.get(j);
                    employees.set(j,employees.get(j + 1));
                    employees.set(j + 1, tmp);
                }
            }
        }
    }
}
