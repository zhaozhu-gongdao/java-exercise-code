public class Solution {
    static class ListNode {
       int val;
       ListNode next;
       ListNode() {}
       ListNode(int val) { this.val = val; }
   }

    public static ListNode removeElements(ListNode head, int val) {
        if (head == null){
            return head;
        }

        ListNode pre = head;
        ListNode cur = head.next;




        while (cur != null){
            if (cur.val == val){
                cur = cur.next;
                pre.next = cur;
            }else{
                pre = cur;
                cur = cur.next;
            }
        }

        //只有一个的情况
        if (pre.val == val && cur == null){
            head = null;
            return head;
        }
        return head;

    }

    public static void main(String[] args) {
        removeElements(create(),7);
    }

    public static ListNode create(){
        ListNode node1 = new ListNode(7);
        ListNode node2 = new ListNode(7);
        ListNode node3 = new ListNode(7);
        ListNode node4 = new ListNode(7);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        return node1;
    }
}
