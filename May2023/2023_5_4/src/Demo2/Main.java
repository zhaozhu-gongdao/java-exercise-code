package Demo2;

public class Main {
    public static void main(String[] args) {
        SingleLinkedList list = new SingleLinkedList();
        list.addFirst(1);
        list.addLast(2);
        System.out.println(list.size());
        System.out.println(list.addIndex(2, 3));
        list.display();
        System.out.println(list.contains(2));
        list.remove(3);
        list.clear();
        list.display();
    }
}
