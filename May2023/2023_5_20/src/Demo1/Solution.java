package Demo1;

import java.util.Stack;

public class Solution {
public boolean isValid(String s) {
        Stack<Character> stack = new Stack();


        for(int i = 0; i < s.length(); i++){
        char character = s.charAt(i);
        //拿出的是左括号
        if (character == '(' || character == '{' || character == '['){
        stack.push(character);
        }else {   //拿出的是右括号


        //没有对应匹配的左括号
        if (stack.empty()){
        return false;
        }


        char cur = stack.peek();
        if (cur == '(' && character == ')' ||
            cur == '{' && character == '}' ||
            cur == '[' && character == ']'){
        //匹配对了
        stack.pop();
        }else{
        //匹配错了
        return false;
        }
        }
        }


        if (stack.empty()){
        return true;
        }


        return false;
        }


    public int evalRPN(String[] tokens) {
        Stack<String> stack = new Stack();

        for(int i = 0; i < tokens.length; i++){
            if (!tokens[i].equals("+") && !tokens[i].equals("-") &&
                    !tokens[i].equals("*") && !tokens[i].equals("/")){

                //如果是数字的情况
                stack.push(tokens[i]);

            }else{
                //不是数字
                int s1 = Integer.parseInt(stack.pop());
                int s2 = Integer.parseInt(stack.pop());
                int ret = 0;

                switch (tokens[i]){
                    case "+": ret = s1 + s2;
                        break;
                    case "-": ret = s2 - s1;
                        break;
                    case "*": ret = s1 * s2;
                        break;
                    case "/": ret = s2 / s1;
                        break;
                }

                stack.push(String.valueOf(ret));
            }
        }

        return Integer.parseInt(stack.peek());
    }
}

class Main{
    public static void main(String[] args) {
        Solution test = new Solution();
        String[] strings = {"4","13","5","/","+"};
        int ret = test.evalRPN(strings);
        System.out.println(ret);
    }
}

