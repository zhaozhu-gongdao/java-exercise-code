package grneralChar;

public class Test {
    public static void main1(String[] args) {
        Message<String> message = new Message<>();
        message.setMessage("测试通配符");
        //function(message);
    }

    public static void function2(Message<String> message){
        System.out.println(message.getMessage());
    }

    public static void funcation1(Message<? extends Father> message){
        System.out.println(message.getMessage());
    }

    public static void main(String[] args) {
        Message<Father> messgaeF = new Message<>();
        Message<Son1> messageS = new Message<>();
        funcation1(messgaeF);
        funcation1(messageS);
    }

    public static void funcation(Message<? super Son1> message){
        System.out.println(message.getMessage());
    }
}
