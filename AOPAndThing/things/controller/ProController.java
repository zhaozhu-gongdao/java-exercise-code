package com.example.aoptest.demos.things.controller;

import com.example.aoptest.demos.things.service.LogService;
import com.example.aoptest.demos.things.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/proga")
@RestController
public class ProController {
    @Autowired
    UserService userService;
    @Autowired
    LogService logService;

    @Transactional
    @RequestMapping("/registry")
    public String registry(String userName, String password){
        userService.insertUser(userName, password);
        logService.insertLog(userName, "用户自行注册");
        return "注册成功";
    }
}
