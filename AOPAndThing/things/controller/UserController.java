package com.example.aoptest.demos.things.controller;

import com.example.aoptest.demos.things.service.LogService;
import com.example.aoptest.demos.things.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private DataSourceTransactionManager dataSourceTransactionManager;
    @Autowired
    private TransactionDefinition transactionDefinition;
    @Autowired
    private UserService userService;
    @RequestMapping("/registry")
    public void registry(String username, String password){
        //开启事务
        TransactionStatus transactionStatus = dataSourceTransactionManager.
                getTransaction(transactionDefinition);
        userService.insertUser(username, password);
        //提交事务
        dataSourceTransactionManager.commit(transactionStatus);
        //回滚事务
        //dataSourceTransactionManager.rollback(transactionStatus);
    }
}
