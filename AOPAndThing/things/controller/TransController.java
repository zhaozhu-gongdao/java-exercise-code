package com.example.aoptest.demos.things.controller;

import com.example.aoptest.demos.things.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/trans")
public class TransController {
    @Autowired
    private UserService userService;

    @Transactional(rollbackFor = {Exception.class, Error.class})
    @RequestMapping("/registry")
    public void insetUser(String username, String password) throws IOException {
        userService.insertUser(username, password);
        try {
            int a = 10 / 0;
        }catch (Exception e){
//            TransactionAspectSupport.currentTransactionStatus().
//                    setRollbackOnly();
            throw new IOException();
        }
    }
}
