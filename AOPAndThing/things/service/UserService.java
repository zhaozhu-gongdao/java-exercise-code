package com.example.aoptest.demos.things.service;

import com.example.aoptest.demos.things.mapper.LogInfoMapper;
import com.example.aoptest.demos.things.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Transactional(propagation = Propagation.NESTED)
    public void insertUser(String userName, String password){
        try{
            int a = 10 / 0;
            userInfoMapper.insert(userName, password);
        }catch (Exception e){

        }
    }
}
