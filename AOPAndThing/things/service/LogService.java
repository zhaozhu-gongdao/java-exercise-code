package com.example.aoptest.demos.things.service;

import com.example.aoptest.demos.things.mapper.LogInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LogService {
    @Autowired
    private LogInfoMapper logInfoMapper;
    @Transactional
    public void insertLog(String userName, String op){
        logInfoMapper.insert(userName, op);
    }
}
