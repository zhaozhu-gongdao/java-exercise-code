package com.example.aoptest.demos.staticProxy;

public interface HouseSubject {
    void rentHouse();

    void saleHouse();
}
