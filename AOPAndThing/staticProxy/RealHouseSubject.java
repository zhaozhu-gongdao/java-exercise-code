package com.example.aoptest.demos.staticProxy;

public class RealHouseSubject implements HouseSubject{
    @Override
    public void rentHouse() {
        System.out.println("房东，出租房子");
    }

    @Override
    public void saleHouse() {
        System.out.println("房东，售卖房子");
    }
}
