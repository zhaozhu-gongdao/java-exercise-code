package com.example.aoptest.demos.staticProxy;

public class Main {
    public static void main(String[] args) {
        HouseSubject subject = new RealHouseSubject();
        HouseProxy proxy = new HouseProxy(subject);
        proxy.rentHouse();
    }
}
