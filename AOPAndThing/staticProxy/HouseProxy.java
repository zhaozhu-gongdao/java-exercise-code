package com.example.aoptest.demos.staticProxy;

public class HouseProxy implements HouseSubject{
    private HouseSubject houseSubject;

    public HouseProxy(HouseSubject houseSubject) {
        this.houseSubject = houseSubject;
    }

    @Override
    public void rentHouse() {
        System.out.println("中介开始代理");
        houseSubject.rentHouse();
        System.out.println("中介结束代理");
    }

    @Override
    public void saleHouse() {

    }
}
