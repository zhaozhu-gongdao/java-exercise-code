package com.example.aoptest.demos.dymanicCGLIB;


import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CGLibIntercepter implements MethodInterceptor {
    private Object target;

    public CGLibIntercepter(Object target) {
        this.target = target;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println(method.getName() + "开始代理");
        Object result = methodProxy.invoke(target, objects);
        System.out.println(method.getName() + "结束代理");
        return null;
    }
}
