package com.example.aoptest.demos.dymanicCGLIB;

import com.example.aoptest.demos.dymanicJDK.HouseSubjectClass;
import com.example.aoptest.demos.staticProxy.HouseSubject;
import com.example.aoptest.demos.staticProxy.RealHouseSubject;
import net.sf.cglib.proxy.Enhancer;

public class Main {

    public static void main(String[] args) {
        //代理接口
        HouseSubject target = new RealHouseSubject();
        HouseSubject proxy3 = (HouseSubject) Enhancer.create(target.getClass(),
                new CGLibIntercepter(target));
        proxy3.rentHouse();
        //目标对象2: 代理类
        HouseSubjectClass subjectClass = new HouseSubjectClass();
        HouseSubjectClass proxy4 = (HouseSubjectClass) Enhancer.create(subjectClass.getClass(),
                new CGLibIntercepter(subjectClass));
        proxy4.saleHouse();
    }
}
