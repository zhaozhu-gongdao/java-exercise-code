package com.example.aoptest.demos.dymanicJDK;

import com.example.aoptest.demos.staticProxy.HouseSubject;
import com.example.aoptest.demos.staticProxy.RealHouseSubject;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        HouseSubject subject = new RealHouseSubject();

        HouseSubject proxy1 = (HouseSubject) Proxy.newProxyInstance(
                subject.getClass().getClassLoader(),
                new Class[]{HouseSubject.class},
                new JDKInvocation(subject));
        proxy1.rentHouse();

        HouseSubject proxy2 = (HouseSubject) Proxy.newProxyInstance(
                subject.getClass().getClassLoader(),
                new Class[]{HouseSubject.class},
                new JDKInvocation(subject));
        proxy2.saleHouse();

//        HouseSubjectClass subjectClass = new HouseSubjectClass();
//        HouseSubjectClass proxy3 = (HouseSubjectClass) Proxy.newProxyInstance(
//                subjectClass.getClass().getClassLoader(),
//                new Class[]{HouseSubjectClass.class},
//                new JDKInvocation(subjectClass));
//        proxy3.rentHouse();
    }
}
