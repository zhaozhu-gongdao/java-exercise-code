package com.example.aoptest.demos.dymanicJDK;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class JDKInvocation implements InvocationHandler {
    private Object target;

    public JDKInvocation(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(method.getName() + "开始代理");
        Object result = method.invoke(target, args);
        System.out.println(method.getName() + "结束代理");
        return result;
    }
}
