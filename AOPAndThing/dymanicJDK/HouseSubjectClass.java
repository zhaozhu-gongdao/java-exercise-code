package com.example.aoptest.demos.dymanicJDK;

public class HouseSubjectClass {
    public void rentHouse(){
        System.out.println("房东出租房子");
    }

    public void saleHouse(){
        System.out.println("房东出售房子");
    }
}
