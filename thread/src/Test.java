import static java.lang.Thread.sleep;

public class Test {
    public static void main1(String[] args) {
        MyThread thread = new MyThread();
        thread.start();

        while (true){
            try{
                System.out.println("这是main线程");
                sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    public static void main2(String[] args) {
        MyRunnable runnable = new MyRunnable();
        Thread t = new Thread(runnable);
        t.start();

        System.out.println("这里是主线程");
    }

    public static void main3(String[] args) {
        Thread thread = new Thread(){
            @Override
            public void run() {
                System.out.println("基于匿名内部类 + Thread创建线程");
            }
        };
        thread.start();
        System.out.println("这是main主线程");
    }

    public static void main4(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("实现Runnable接口，重写run方法，基于匿名内部类");
            }
        });

        thread.start();
        System.out.println("这是main主线程");
    }

    public static void main5(String[] args) {
        Thread thread = new Thread(() -> {
            while (true){
                System.out.println("打印线程");
            }
        }, "myThread");
        thread.setDaemon(true);
        thread.start();
    }


    public static void main6(String[] args) throws InterruptedException {
        boolean isQuit = false;
        Thread thread = new Thread(() -> {
            while(!isQuit){
                try {
                    Thread.sleep(1000);
                    System.out.println("这是自定义创建的线程");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        thread.start();
        Thread.sleep(3000);
    }

    public static void main7(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            while(!Thread.currentThread().isInterrupted()){
                System.out.println("这是自定义创建的线程");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        });

        thread.start();
        Thread.sleep(3000);
        thread.interrupt();
    }

    public static void main9(String[] args) throws InterruptedException {
        Thread thread = new Thread(()->{
            try {
                Thread.sleep(3000);
                System.out.println("自定义线程执行结束");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        thread.start();
        thread.join();
        System.out.println("main主线程执行完毕");
    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(()->{
            System.out.println("测试出TERMINATED的线程状态");
        });
        thread.start();
        thread.join();
        System.out.println(thread.getState());
    }
}
