public class ThreadSociety {
    static class Counter{
        Object locker = new Object();
        int num = 0;
        public void increase(){
            synchronized (locker){
                num++;
            }
        }
        public void increase2(){
            synchronized (locker){
                num++;
            }
        }
    }
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();
        
        Thread t1 = new Thread(()->{
            for (int i = 0; i < 100000; i++){
                counter.increase();
            }
        });
        Thread t2 = new Thread(()->{
            for (int i = 0; i < 100000; i++){
                counter.increase2();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();

        System.out.println(counter.num);
    }
}


