import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String str = in.nextLine();

            int[] arr = new int[127];

            StringBuilder result = fun(str);
            System.out.println(result);

        }
    }

    public static StringBuilder fun(String str){
        int[] arr = new int[127];
        StringBuilder result = new StringBuilder();

        for(int i = 0; i < str.length(); i++){
            char ch = str.charAt((i));

            if (arr[ch] == 0){
                result.append(ch);
                arr[ch]++;
            }
        }
        return result;
    }


    public static void merge(int A[], int m, int B[], int n) {
        int k = A.length - 1;
        int i = m - 1;
        int j = n - 1;

        if (A[i] < B[j]){
            A[k] = B[j];
            j--;
            k--;
        }else{
            A[k] = A[i];
            i--;
            k--;
        }
    }
}