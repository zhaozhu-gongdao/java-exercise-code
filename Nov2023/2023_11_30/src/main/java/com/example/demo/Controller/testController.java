
package com.example.demo.Controller;
public class testController{
    public static void main(String[] args) {
        //moveZeroes(new int[]{1, 2, 0, 3});
        duplicateZeros(new int[]{1, 0, 2, 3, 0, 4, 5, 0});
    }

    public static int[] moveZeroes (int[] nums) {
        int cur = 0;
        int dest = -1;

        for(; cur < nums.length;cur++){
            if (nums[cur] != 0){
                dest++;

                int tmp = nums[cur];
                nums[cur] = nums[dest];
                nums[dest] = tmp;
            }
        }

        return nums;
    }


    public static void duplicateZeros(int[] arr) {
        int dest = -1;
        int cur = 0;

        for (; cur < arr.length; cur++){
            if (arr[cur] == 0){
                dest += 2;
            }else {
                dest += 1;
            }

            if (dest >= arr.length - 1){
                break;
            }
        }

        //处理边界问题
        if (dest == arr.length){
            arr[dest - 1] = 0;
            dest -= 2;
            cur--;
        }


        //从后往前放
        for (; cur >= 0; cur--){
            if (arr[cur] == 0){
                arr[dest] = 0;
                arr[dest - 1] = 0;
                dest -= 2;
            }else{
                arr[dest] = arr[cur];
                dest--;
            }
        }
    }

    public static boolean isHappy(int n) {
        double slow = 0;
        double fast = -1;

        while(slow != fast){
            slow = Math.pow(n, 2);
            fast = Math.pow(Math.pow(n, 2), 2);
        }

        if (slow == 1){
            return true;
        }else {
            return false;
        }
    }
}

