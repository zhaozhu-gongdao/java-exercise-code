package com.example.demo.service;

import com.example.demo.dao.BookDao;
import com.example.demo.model.BookInfo;

import java.util.List;

public class BookService {
    public List<BookInfo> getBookList(){
        BookDao bookDao = new BookDao();
        List<BookInfo> books = bookDao.mockData();

        for (BookInfo book:books){
            if (book.getStatus() % 2 == 0){
                book.setStatusCN("可借阅");
            }else {
                book.setStatusCN("不可借阅");
            }
        }

        return books;
    }
}
