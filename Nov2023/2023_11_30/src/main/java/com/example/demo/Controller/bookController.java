package com.example.demo.Controller;

import com.example.demo.model.BookInfo;
import com.example.demo.service.BookService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/book")
public class bookController {
    @RequestMapping("/getList")
    public List<BookInfo> getList(){
        BookService bookService = new BookService();
        List<BookInfo> books = bookService.getBookList();

        return books;
    }

}
