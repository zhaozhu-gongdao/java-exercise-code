package com.example.demo.Controller;

import jakarta.servlet.http.HttpSession;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    @RequestMapping("login")
    public boolean login(String name, String password, HttpSession session){
        if (!StringUtils.hasLength(name) || !StringUtils.hasLength(password)){
            return false;
        }

        if ("admin".equals(name) && "123456".equals(password)){
            session.setAttribute("username", name);
            return true;
        }

        return false;
    }
}
