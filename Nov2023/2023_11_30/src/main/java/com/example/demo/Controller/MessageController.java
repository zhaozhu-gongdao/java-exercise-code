package com.example.demo.Controller;

import com.example.demo.model.MessageInfo;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("message")
public class MessageController {
    private List<MessageInfo> messageInfos = new ArrayList<>();

    @RequestMapping("/getList")
    public List<MessageInfo> getList(){
        return messageInfos;
    }

   @RequestMapping("/publish")
    public boolean publish(MessageInfo messageInfo) {
        if (StringUtils.hasLength(messageInfo.getFrom()) && StringUtils.hasLength(messageInfo.getTo())
            && StringUtils.hasLength(messageInfo.getMessage())){
            messageInfos.add(messageInfo);
            return true;
        }
        return false;
    }


}
