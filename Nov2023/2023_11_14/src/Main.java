class Solution {
//    public int maxArea(int[] height) {
//        int left = 0;
//        int right = height.length - 1;
//        int res = 0;
//
//        while (left < right){
//            //height 取最小值
//            int he = height[left] > height[right]?height[right]:height[left];
//
//            int area = he * (right - left);
//
//            if (height[left] > height[right]){
//                right--;
//            }else{
//                left++;
//            }
//
//            if (res < area){
//                res = area;
//            }
//        }
//
//        return res;
//    }

    public int maxArea(int[] height){
        int left = 0;
        int right = height.length - 1;
        int ret = 0;

        while (left < right){
            int v = Math.min(height[left], height[right]) * (right - left);
            ret = Math.max(ret, v);
            if (height[left] < height[right]){
                left++;
            }else{
                right--;
            }
        }

        return ret;
    }
}

public class Main {
    public static void main(String[] args) {
        Solution test = new Solution();
        int result = test.maxArea(new int[]{2, 3, 4, 5, 18, 17, 6});
        System.out.println(result);
    }
}
