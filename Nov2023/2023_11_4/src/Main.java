public class Main {
    public static void main(String[] args) {
        int[] arr = new int[]{1,0,2,3,0,4,5,0};

        Solution test = new Solution();
        test.duplicateZeros(arr);
    }

}


class Solution {
    public void duplicateZeros(int[] arr) {
        //寻找原数组中最后结束的那个数
        int cur = -1;
        int dest = 0;

        for (; cur < arr.length - 1; dest++){
            if (arr[dest] != 0){
                cur += 1;
            }else{
                cur += 2;
            }
        }

        //处理边界问题
        if (cur == arr.length){
            arr[cur-1] = 0;
            dest--;
            cur -= 2;
        }

        //此时dest指向的事结束的数字
        for (;dest >= 0; dest--){
            if (arr[dest] == 0){
                arr[cur] = 0;
                arr[--cur] = 0;
            }else{
                arr[cur] = arr[dest];
            }
        }
    }
}