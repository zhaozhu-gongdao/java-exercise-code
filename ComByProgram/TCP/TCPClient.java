package TCP;


import com.sun.deploy.util.SessionState;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Struct;
import java.util.Scanner;

public class TCPClient {
    private String serverIP;
    private int serverPort;
    private Socket socket = null;

    public TCPClient(String serverIP, int serverPort) throws IOException {
        socket = new Socket(serverIP, serverPort);
    }

    public void start() throws IOException {
        System.out.println("TCP客户端启动！");
        Scanner scanner = new Scanner(System.in);

        try(InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(outputStream);
            Scanner scannerConsole = new Scanner(inputStream)){
            while (true){
                System.out.print("-----> ");
                String request = scanner.next();
                writer.println(request);
                writer.flush();

                String response = scannerConsole.next();
                System.out.println("服务器响应：" + response);
            }
        }

    }

    public static void main(String[] args) throws IOException {
        TCPClient client = new TCPClient("127.0.0.1", 9091);
        client.start();
    }
}
