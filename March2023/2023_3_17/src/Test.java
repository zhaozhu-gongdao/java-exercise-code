/*
public class Test {
    public static void main(String[] args) {
        System.out.println(1+2);
        System.out.println(3-4);
        System.out.println(5*6);
        System.out.println(8/4);
        System.out.println(8/5);

        int a = 10;
        int b = 20;
        int c = a > b? 1 :2.0;
    }
}
*/

   //成员前写法统一为public，此处的方法不带static
/*
class Dog{
   public String name;
   public String color;     //成员变量

   public void barks(){
      System.out.println(name + "汪汪汪");
   }

   public void wag(){
      System.out.println(name + "摇尾巴");
   }
}

public class Test{
   public static void main(String[] args) {
      Dog dog = new Dog();   //通过new实例化对象
                            //类型定义了一个变量来接收，存的是接收值(对象)的地址，所以dog是个引用变量/引用

      dog.name = "小白";
      dog.color = "白";
      dog.barks();
      dog.wag();
   }

   public Test() {
   }
}*/

/*class Date{
   public int year;
   public int month;
   public int day;

   public void setDay(int y, int m, int d){
      year = y;
      month = m;
      day = d;
   }
}
public class Test{
   public static void main(String[] args) {
      Date d1 = new Date();

   }
}*/

/*public class Test {
   public int year;
   public int month;
   public int day;

   public void setDay(int y, int m, int d){
      year = y;
      month = m;
      day = d;
   }
   public void printDate(){
      System.out.println(year + "/" + month + "/" + day);
   }
   public static void main(String[] args) {
// 构造三个日期类型的对象 d1 d2 d3
      Test d1 = new Test();
      Test d2 = new Test();
      Test d3 = new Test();
// 对d1，d2，d3的日期设置
      d1.setDay(2020,9,15);
      d2.setDay(2020,9,16);d3.setDay(2020,9,17);
// 打印日期中的内容
      d1.printDate();
      d2.printDate();
      d3.printDate();
   }
}*/


/*public class Test {
    public static void main(String[] args) {
*//*        int num = 10;
        if (num % 2 == 0){
            System.out.println("偶数");
        }
        else{
            System.out.println("奇数");
        }*//*


 *//*       int year = 2000;
        if ((year % 4 == 0) && (year % 100 != 0)){
            System.out.println("闰年");
        }else if(year % 400 == 0){
            System.out.println("闰年");
        }else{
            System.out.println("不是闰年");
        }*//*

        int i = 1;
        while(i < 11){
            System.out.print(i+" ");
            i++;
        }
    }
}*/

import java.util.Scanner;
public class Test{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入你的名字：");
        String name = sc.nextLine();

        System.out.println("请输入你的年龄");
        int age = sc.nextInt();

        System.out.println("请输入你的工资");
        float salary = sc.nextFloat();
/*        Scanner scan = new Scanner(System.in);
        System.out.println("输入一个整数：");
        int a = scan.nextInt();
        System.out.println(a);
        System.out.println("输入你的姓名：");
        String name = scan.nextLine();//遇到空格不会结束的
        System.out.println(name);
        System.out.println("输入一个小数：");
        double d = scan.nextDouble();
        System.out.println(d);*/
    }
}
