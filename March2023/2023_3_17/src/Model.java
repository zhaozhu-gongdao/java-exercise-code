import java.util.Random;
import java.util.Scanner;

/**
 * @Author 12629
 * @Description：
 */
public class Model {
    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j+"*"+i +"="+i*j+" ");
            }
            System.out.println();
        }
    }
    public static void main13(String[] args) {
        int num = 7;
        int i = 2;
        for (; i <= Math.sqrt(num); i++) {
            if(num % i == 0) {
                System.out.println("不是素数！");
                break;
            }
        }
        if(i > Math.sqrt(num)) {
            System.out.println("是素数！");
        }
    }

    public static void main12(String[] args) {
        int num = 8;
        int i = 2;
        for (; i <= num/2; i++) {
            if(num % i == 0) {
                System.out.println("不是素数！");
                break;
            }
        }
        if(i > num/2) {
            System.out.println("是素数！");
        }
    }
    public static void main11(String[] args) {
        int num = 7;
        int i = 2;
        for (; i < num; i++) {
            if(num % i == 0) {
                System.out.println("不是素数！");
                break;
            }
        }
        if(i == num) {
            System.out.println("是素数！");
        }
    }
    public static void main10(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random random = new Random();
        //int randNum = random.nextInt(100)+50;//[50-150)
        //int randNum = random.nextInt(50)+50;//[50-100)      [50,100]
        int randNum = random.nextInt(100);//[0-100) --> [50-100)
        System.out.println("作弊："+randNum);
        while (true) {
            int n = scan.nextInt();
            if(n < randNum) {
                System.out.println("你猜小了！");
            }else if(n == randNum) {
                System.out.println("你猜对了！");
                break;
            }else {
                System.out.println("你猜大了！");
            }
        }

    }
    public static void main9(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNextInt()) {
            int a = scan.nextInt();
            System.out.println(a);
        }


        System.out.println("输入一个整数：");
        int a = scan.nextInt();
        System.out.println(a);
        scan.nextLine();//回车给消掉
        System.out.println("输入你的姓名：");
        String name = scan.nextLine();//遇到空格不会结束的
        System.out.println(name);
        System.out.println("输入一个小数：");
        double d = scan.nextDouble();
        System.out.println(d);

    }


    public static void main8(String[] args) {
        System.out.println("输出且换行");
        System.out.print("输出不换行");
        System.out.printf("%s\n","格式化输出！");
    }
    public static void main7(String[] args) {
        int x = 0;
        do {
            System.out.println(x);
        }while (x != 0);
    }
    public static void main6(String[] args) {
        int sum = 0;
        for (int x = 1; x <= 5; x++) {
            int ret = 1;
            for (int i = 1; i <= x; i++) {
                ret *= i;
            }
            //System.out.println(ret);
            sum += ret;
        }
        System.out.println(sum);
    }
    public static void main5(String[] args) {
       /* int a = 1;
        while (a <= 2) {
            while (a <= 5) {
                a++;
            }
        }
        a--;
        System.out.println(a);*/
        /*while (true) {
            System.out.println("fsdfsa");
        }*/
       /* int i = 1;
        while (i <= 10) {
            if(i % 3 == 0) {
                System.out.println(i);
                i++;
                continue;
            }
            i++;
        }*/
        int i = 1;
        while (i <= 100) {
            if(i % 30 != 0) {
                i++;
                continue;
            }
            System.out.println(i);
            i++;
        }
    }
    public static void main4(String[] args) {
        int x = 1;
        int sum = 0;
        while (x < 16) {
            int i = 1;
            int ret = 1;
            while (i <= x) {
                ret = i * ret;
                i++;
            }
            //System.out.println(ret);
            sum += ret;
            x++;
        }
        System.out.println(sum);
    }

}