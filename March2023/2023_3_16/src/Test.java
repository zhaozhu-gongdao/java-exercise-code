/*
public class Test {
    public static void main1(String[] args) {
        System.out.println("hello world");
        System.out.println(100);
        System.out.println(3.14);
        System.out.println('A');
        System.out.println(true);
        System.out.println(false);
    }

    public static void main(String[] args) {
        //通过数据类型，定义各种变量
        int a = 10;      //a是变量名或者说是标识符
        double b = 3.14;
        char c = 'A';
        boolean d = true;

        //可以在一行定义多个相同类型的变量
        int a1 = 0,a2 = 20,a3  = 30;
        System.out.println(a1);
        System.out.println(a2);
        System.out.println(a3);

        //打印
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);

        //修改赋值
        a = 100;
        System.out.print("改变后的 a =");
        System.out.println(a);
    }
}
*/

/*public class Test{
    public static void main(String[] args) {
        //初始化
        int a = 0;
        System.out.println(a);

        int b;
        System.out.println(b);       //会报错，java当中，定义在方法当中的变量必须初始化才能使用

        //最大值
        System.out.println(Integer.MIN_VALUE);    //int类型的最小值
        System.out.println(Integer.MAX_VALUE);    //int类型的最大值，所以创建变量时要时刻注意取值范围
                                                  //Integer是包装类，相当于是int类型的plus版

        int b = 123456789010;           //会报错，b的范围超过了int的最大值
    }
}*/

/*
public class Test{
    public static void main(String[] args) {
        int a = 10;
        long b = 10L;      //为了区分int和long，最好在long变量初始值之后加L

        System.out.println(Long.MIN_VALUE);      //-2^63     Long是long类型的包装类
        System.out.println(Long.MAX_VALUE);      //2^63-1
    }
}*/

/*
public class Test {
    public static void main(String[] args) {
        short a = 10;
        System.out.println(a);

        System.out.println(Short.MIN_VALUE);    //-2^15    -32768    Short是short类型的包装类
        System.out.println(Short.MAX_VALUE);    ////2^15-1     32767
    }
}
*/


/*
public class Test{
    public static void main(String[] args) {
        byte a = 10;
        System.out.println(a);

        System.out.println(Byte.MIN_VALUE);    //-128   Byte是byte类型的包装类
        System.out.println(Byte.MAX_VALUE);    //127


        byte b = 128;        //在java当中，当给定的数据超过了当前数据类型所表示的范围之后，会报错
        System.out.println(b);      //会报错
    }
}*/


/*public class Test{
    public static void main(String[] args) {
        double a = 3.14;
        System.out.println(a);   //3.14

        int b = 1;
        int c = 2;
        System.out.println(b/c);   //0   原理：都是整型，是无法储存小数的

        double d = 1;
        double e = 2;
        System.out.println(d/e);   //0.5   上个代码的改良

        double num = 1.1;
        System.out.println(num * num);   //1.2100000000000002
                                  //原理：小数在计算机中，没有一个完全的表示，只能精确到某一位
    }
}*/


/*
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            double inputN = scanner.nextDouble();
            double tmpN=0;
            if (inputN<0) {
                System.out.println(Double.NaN);
            }else {
                while(tmpN*tmpN<inputN&&(Math.abs(tmpN*tmpN-inputN)>0.0001)) {
                    tmpN+=0.0001;
                }
                System.out.printf("%.6f\n",tmpN);
            }
        }
        scanner.close();
    }

}
*/


/*public class Test{
    public static void main(String[] args) {
        float a = 1.0f;     //两种初始化方法，可以加f或F
        float b = 1.0F;
        System.out.println(a);
        System.out.println(b);
    }
}*/


/*public class Test{
    public static void main(String[] args) {
        char a = 'A';    //字母
        char b = '1';    //数字
        char c = '牛';    //中文

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}*/

/*public class Test{
    public static void main(String[] args) {
        boolean a = true;    //布尔类型只有两种取值，true表示真，false表示假
        boolean b = false;

        a = a+b;          //会报错，不存在非表示真，0表示假的情况，所以boolean不能和int相互转换
        System.out.println(a);     //true
        System.out.println(b);     //false


            public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(i == j || (i+j) == n-1) {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
    }
}*/

/*public class Test{
    public static void main(String[] args) {
        System.out.println(1024);    //整型默认情况下是int
        System.out.println(3.14);    //浮点型默认情况下是double

        int a = 100;
        long b = 10L;
        b = a;     //a和b都是整型，a的范围小，b的范围大，当将a赋值给b时，编译器会自动将a提升为long类型，然后赋值
        a = b;     //编译报错，long的范围比int范围大，会有数据丢失，不安全

        float f = 3.14F;
        double d = 5.12;

        d = f;     //编译器会将f转换为double，然后进行赋值
        f = d;     //double表示数据范围大，直接将float交给double会有数据丢失，不安全

        byte b1 = 100;  //编译通过，100没有超过byte的范围，编译器隐式将100转换为byte
        byte b2 = 257;  //编译失败，257超过了byte的数据范围，有数据丢失
    }
}*/

/*public class Test{
    public static void main(String[] args) {
        int a = 10;
        long b = 100L;
        b = a; // int-->long，数据范围由小到大，隐式转换
        a = (int)b; // long-->int, 数据范围由大到小，需要强转，否则编译失败

        float f = 3.14F;
        double d = 5.12;
        d = f; // float-->double，数据范围由小到大，隐式转换
        f = (float)d; // double-->float, 数据范围由大到小，需要强转，否则编译失败

        a = d; // 报错，类型不兼容
        a = (int)d; // int没有double表示的数据范围大，需要强转，小数点之后全部丢弃

        byte b1 = 100; // 100默认为int，没有超过byte范围，隐式转换
        byte b2 = (byte)257; // 257默认为int，超过byte范围，需要显示转换，否则报错

        boolean flag = true;
        a = flag; // 编译失败：类型不兼容
        flag = a; // 编译失败：类型不兼容
    }
}*/


/*
public class Test{
    public static void main(String[] args) {
        int a = 100;
        long b = 10L;

        int c = a + b;   //编译报错，a+b->int+long->long+long 赋值给int时会丢失数据
        long d = a + b;  //编译成功，a+b->int+long->long+long 赋值给long不会丢失数据
    }
}*/


/*
public class Test{
    public static void main(String[] args) {
        byte a = 10;
        byte b = 20;   //编译报错，虽然a和b都是byte，但是计算a+b会先将a和b都提升成int再进行计算
        byte c = a + b;    //因此得到的结果也是int，这时赋值给byte就会出现数据的丢失
        System.out.println(c);

        //修改
        byte a1 = 10;
        byte b1 = 20;
        byte c1 = (byte)(a1+b1);
        System.out.println(c1);

    }
}*/


/*
public class Test{
    public static void main(String[] args) {
        String s1 = "hello ";
        String s2 = "world";
        System.out.println(s1+s2);
    }
}*/

/*
public class Test{
    public static void main(String[] args) {
        int a = 10;
        int b = 20;      //都是字符串类型
        System.out.println(a+b+"hello");   //30hello
        System.out.println("hello"+a+b);   //hello1020
        System.out.println("hello"+(a+b));  //hello30
    }
}*/


/*
public class Test{
    public static void main(String[] args) {
        int num = 10;
        String str = String.valueOf(num);
        System.out.println(str);
    }
}*/


/*
public class Test{
    public static void main(String[] args) {
        String str = "100";
        int num = Integer.parseInt(str);
        System.out.println(num);    //100
    }
}*/

/*public class Test{
    public static void main(String[] args) {
        int a = 0;
        long b = 10L;
        a = a + b;    //报错
        a +=b;        //不报错
    }
}*/


public class Test{
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        System.out.println(a==b);   //false
        System.out.println(a!=b);   //true
        System.out.println(a<b);    //true
        System.out.println(a>b);    //false
        System.out.println(a<=b);   //true
        System.out.println(a>=b);   //false
    }
}