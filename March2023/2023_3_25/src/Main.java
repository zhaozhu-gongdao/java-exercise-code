import java.util.Arrays;
import java.util.Scanner;

//public class Main {
    //调整数组顺序使得奇数位于偶数之前。调整之后，不关心大小顺序。
/*    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};

        int left = 0;
        int right = arr.length - 1;
        while(left < right){
            while ((arr[left] % 2) != 0){
                left ++;
            }
            while ((arr[right] % 2) == 0){
                right --;
            }

            if (left < right){
                int tmp = 0;
                tmp = arr[left];
                arr[left] = arr[right];
                arr[right] = tmp;
            }
        }

        System.out.println(Arrays.toString(arr));
    }*/


    //给定一个有序整型数组, 实现二分查找
/*    public static int bubberSearch(int[] arr, int k){
        int left = 0;
        int right = arr.length - 1;

        while (left <= right){
            int middle = (left + right) / 2;
            if (arr[middle] > k){
                right = middle - 1;
            }else if (arr[middle] < k){
                left = middle + 1;
            }else{
                return middle;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int[]arr = {1,2,3,4,5,6};
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()){
            int k = scanner.nextInt();
            int ret = bubberSearch(arr,k);
            if (ret != -1){
                System.out.println("该数在下标" + ret + "处");
            }else{
                System.out.println("查无此数");
            }
        }
    }*/

/*    public static int search(int[]arr, int k){
        int len = 0;
        int right = arr.length - 1;

        while (len <= right){
            int middle = (len + right) / 2;

            if (arr[middle] < k){
                len = middle + 1;
            }else if(arr[middle] > k) {
                right = middle - 1;
            }else{
                return middle;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int[]arr = {1,2,3,4,5,6,7};

        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int index = search(arr,k);
        if(index != -1){
            System.out.println("在第" + (index + 1)+ "个位置");
        }else{
            System.out.println("查无此数");
        }
    }*/


    //给定一个整型数组, 实现冒泡排序(升序排序)
/*    public static void fun(int[]arr){
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]){
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {4,3,7,8,0,46,23,86};
        fun(arr);
        System.out.println(Arrays.toString(arr));
    }*/

/*    public static int[] fun(int[]sum, int target){
        for (int i = 0; i < sum.length - 1; i++) {
            for (int j = i + 1; j < sum.length; j++) {
                if (sum[i] + sum[j] == target){
                    return new int[]{i,j};
                }
            }
        }

        return null;
    }

    public static void main(String[] args) {
        //输入：nums = [2,7,11,15], target = 9
        //
        //输出：[0,1]
        //
        //解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
        int[] nums = {2,7,11,15};
        int[]ret = fun(nums,9);
        System.out.println(Arrays.toString(ret));
    }*/

/*    public static int[] fun(int[]arr){
        int ret[] = new int[arr.length];;

        for (int i = 0; i < arr.length - 1; i++) {
            int count = 1;
            int num = 0;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]){
                    count++;
                }

                if (count > 1){
                    break;
                }
            }
            if (count == 1){
                ret[num] = arr[i];
                num++;
            }
        }

        return ret;
    }

    public static void main(String[] args) {
        //给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
        int[] arr = {3,3,6,6,1,78,4,4};
        int[] ret = fun(arr);
        System.out.println(Arrays.toString(ret));
    }*/

/*
    public static int fun(int[]arr){
        int sum = arr[0];
        for (int i = 1; i < arr.length; i++) {
            sum ^= arr[i];
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 2, 2, 9};
        int ret = fun(arr);
        System.out.println(ret);
    }
}
*/

/*public class Main{
    public static void main(String[] args) {
        int[] arr = {1, 1, 2, 2, 2, 9};
        Arrays.sort(arr);
        System.out.println(arr[arr.length / 2]);
    }
}*/


//给你一个整数数组 arr，请你判断数组中是否存在连续三个元素都是奇数的情况：如果存在，请返回 true ；否则，返回 false 。
public class Main{
    public static boolean fun(int[]arr){

        for (int i = 0; i < arr.length - 2; i++) {
            int count = 0;
            for (int j = 0; j < 3; j++) {
                if (arr[i + j] % 2 == 1){
                    count++;
                }else{
                    break;
                }
            }

            if (count == 3){
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        int[] arr = {1,4,5,4,7,9,3,10};
        System.out.println(fun(arr));

        int[] arr2 = {0,0,0,0,0};
        System.out.println(fun(arr2));
    }
}
