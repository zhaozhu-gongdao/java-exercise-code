
/*class WashMachine{
    public String name;
    public int price;

    public void setTime(){
        System.out.println("设置时间");
    }
}

public class Main {
    public static void main(String[] args) {
        WashMachine washMachine = new WashMachine();
        washMachine.name = "小天鹅";
    }
}*/


class Date{
    public int year;
    public int month;
    public int day;

    public void setDate(int year, int month, int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public void printf(){
        System.out.println("年：" + this.year + "   月：" + this.month + "   日：" + this.day);
    }
}

public class Main{
    public static void main(String[] args) {
        Date date1 = new Date();
        Date date2 = new Date();
        Date date3 = new Date();

        date1.setDate(2022,3,1);
        date2.setDate(2023,4,6);
        date3.setDate(2060,9,9);

        date1.printf();
        date2.printf();
        date3.printf();
    }
}