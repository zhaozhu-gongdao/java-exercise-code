import java.util.Scanner;

public class Main {

    //写一个方法来计算闰年
/*    public static void isLeapYear(int year){
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0){
            System.out.println("是闰年");
        }else{
            System.out.println("不是闰年");
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        isLeapYear(scanner.nextInt());
    }*/

/*    public static void calAdd (int a, int b){
        System.out.println(a + b);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        calAdd(scanner.nextInt(), scanner.nextInt());
    }*/


    //计算1-5的阶乘
/*    public static void fac(){
        int sum = 0;
        int ret = 0;

        for (int i = 1; i <= 5; i++) {
            ret = 1;
            for (int j = 1; j <= i ; j++) {
                ret *= j;
            }
            sum += ret;
        }

        System.out.println(sum);
    }

    public static void main(String[] args) {
        fac();
    }*/

/*    public static void add(int a, int b){
        System.out.println(a + b);
    }

    public static void add(double a, double b){
        System.out.println(a + b);
    }
    public static void main(String[] args) {
        add(5,3);
        add(0.5,0.3);
    }*/

    //计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值
/*    public static void main(String[] args) {
        double sum = 0;
        int flag = 1;

        for (int i = 1; i < 101; i++) {
            sum +=flag * (1.0 / i);
            flag = -flag;
        }
        System.out.println(sum);
    }*/

/*        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int len = scanner.nextInt();

            for (int i = 0; i < len; i++){
                for (int j = 0; j < len; j++){
                    if (i == j || i == (len - 1 - j)){
                        System.out.print("*");
                    }else{
                        System.out.print(" ");
                    }
                }
                System.out.println("");
            }
        }*/

    //创建方法求两个数的最大值max2，随后再写一个求3个数的最大值的函数max3。
    //
    //​      要求：在max3这个函数中，调用max2函数，来实现3个数的最大值计算
/*    public static void max(int a, int b){
        if (a > b){
            System.out.println(a);
        }else{
            System.out.println(b);
        }
    }

    public static void max(int a, int b, int c){
        int max = a;
        if (max < b){
            max = b;
        }else if(max < c){
            max = c;
        }
        System.out.println(max);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        max(scanner.nextInt(),scanner.nextInt());
        max(scanner.nextInt(),scanner.nextInt(),scanner.nextInt());
    }*/


    //求 N 的阶乘
/*        public static void fac(int n){
        int sum = 0;
        int ret = 0;

        for (int i = 1; i <= n; i++) {
            ret = 1;
            for (int j = 1; j <= i ; j++) {
                ret *= j;
            }
        }

        System.out.println(ret);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        fac(scanner.nextInt());
    }*/

    //求1！+2！+3！+4！+........+n!的和
/*    public static void fac(int n){
        int sum = 0;
        int ret = 0;

        for (int i = 1; i <= n; i++) {
            ret = 1;
            for (int j = 1; j <= i ; j++) {
                ret *= j;
            }
            sum += ret;
        }

        System.out.println(sum);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        fac(scanner.nextInt());
    }*/


    //在同一个类中,分别定义求两个整数的方法 和 三个小数之和的方法。 并执行代码，求出结果
/*    public static void add(int a, int b){
        System.out.println(a + b);
    }

    public static void add(float a, float b, float c){
        System.out.println(a + b +c);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        add(scanner.nextInt(),scanner.nextInt());
        add(scanner.nextFloat(),scanner.nextFloat(),scanner.nextFloat());
    }*/


    //在同一个类中定义多个方法：要求不仅可以求2个整数的最大值，还可以求3个小数的最大值？
/*    public static void max(int a, int b){
        if (a > b){
            System.out.println(a);
        }else{
            System.out.println(b);
        }
    }

    public static void max(float a, float b, float c){
        float max = a;
        if (max < b){
            max = b;
        }else if(max < c){
            max = c;
        }
        System.out.println(max);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        max(scanner.nextInt(),scanner.nextInt());
        max(scanner.nextFloat(),scanner.nextFloat(),scanner.nextFloat());
    }*/


    //用递归求n的阶乘
/*    public static int fac(int n){
        if (n == 1){
            return 1;
        }else{
            return n * fac(n - 1);
        }
    }
    public static void main(String[] args) {
        System.out.println(fac(5));
    }*/



    //按照顺序打印数字的每一位
/*    public static int print(int n){
        if (n < 10){
            return n;
        }else{
            System.out.print(n % 10 + " ");
            return print(n / 10);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        print(scanner.nextInt());
    }*/


/*    public static void print(int n){
        if (n < 10){
            System.out.print(n + " ");;
        }else{
            print(n / 10);
            System.out.print(n % 10 + " ");
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        print(scanner.nextInt());
    }*/

    //递归求1加到10的和
/*    public static int add(int n){
        int sum = 0;
        if (n == 10){
            return n;
        }else{
            return sum += n + add(n + 1);
        }

    }
    public static void main(String[] args) {
        System.out.println(add(1));
    }*/


    //输入一个非负整数，返回它的组成数字之和
/*    public static int sum(int n){
        if(n < 10){
            return n;
        }else{
            return n % 10 +sum(n/10);
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(sum(scanner.nextInt()));
    }*/


    // 利用递归求斐波那契数列
/*    public static int fib(int n){
        if (n == 1 || n == 2){
            return 1;
        }else{
            return fac(n-1)+fac(n-2);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(fib(scanner.nextInt()));
    }*/


    //斐波那契数列的第n项。(迭代实现)
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = 1;
        int b = 1;
        int c = 1;
        int num = scanner.nextInt();
        if (num > 2){
            for (int i = 0; i < num - 2; i++){
                int tmp = 0;
                c = a + b;
                a = b;
                b = c;
            }
        }
        System.out.println(c);
    }
}

