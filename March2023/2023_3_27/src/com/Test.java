package com;

class Stu{
    private String name;
    private int age;
    public static String classRoom = "301";

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
public class Test {
    public static void main(String[] args) {
        System.out.println(Stu.classRoom);
    }
}
