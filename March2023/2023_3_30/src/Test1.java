/*
import java.util.Scanner;

public class Test1 {

    public static void main(String[] args) {
        Person p = new Person();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int age = scanner.nextInt();
            p.setAge(age);
            System.out.println(p.getAge());
        }
    }

}

class Person {

    private int age;

    public void setAge(int age){
        this.age = age;
    }

    public int getAge(){
        if(this.age < 0){
            return 0;
        }else if (this.age > 200){
            return 200;
        }else{
            return this.age;
        }
    }


}*/



/*
import java.util.Scanner;

public class Test1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            Sub sub = new Sub(x, y);
            sub.calculate();
        }
    }

}

class Base {

    private int x;
    private int y;

    public Base(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void calculate() {
        System.out.println(getX() * getY());
    }

}

class Sub extends Base {

    public Sub(int x, int y) {
        super(x, y);
    }

    public void calculate() {
        if (getY() == 0){
            System.out.println("Error");
        }else{
            System.out.println(super.getX() / super.getY());
        }
    }

}*/


/*import java.util.Scanner;

public class Test1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            Sub sub = new Sub(x, y);
            System.out.println(sub.sum());
        }
    }

}

class Base {

    private int x;
    private int y;

    public Base(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public final int getY() {
        return y;
    }

    public final int sum() {
        return getX() + getY();
    }

}

class Sub extends Base {

    public Sub(int x, int y) {
        super(x, y);
    }

    @Override
    public int getX() {
        return super.getX();
    }
}*/

/*
abstract class Father{   //抽象类
    abstract public void func();    //抽象方法
}
public class Test1{
    public static void main(String[] args) {
        //Father father = new Father();     会报错
    }
}*/
