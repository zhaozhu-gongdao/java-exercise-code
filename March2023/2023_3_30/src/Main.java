
/*class Father{
    public void eat(){
        System.out.println("吃饭");
    }
}

class Son extends  Father{
    public void eat(){
        System.out.println("正在吃黄焖鸡米饭");
    }
}

public class Main {
    public static void main(String[] args) {
        Father person = new Son();
        person.eat();
    }
}*/


/*
class Father{
    public void eat(Father person){
        System.out.println("吃饭");
    }
}

class Son extends  Father{
    public void eat(Father person){
        System.out.println("正在吃黄焖鸡米饭");
    }
}

public class Main {
    public static void main(String[] args) {
        Son person = new Son();
        person.eat(person);
    }
}*/

/*class Father{
    public void eat(Father person){
        System.out.println("吃饭");
    }
}

class Son extends  Father{
    public Father eat(Son person){
        System.out.println("正在吃黄焖鸡米饭");
        return person;
    }
}

public class Main {
    public static void main(String[] args) {
        Son person = new Son();
        person.eat(person);
    }
}*/


class Animal{
    String name;
    int age;

    public Animal(String name) {
        this.name = name;
    }
}

class Dog extends  Animal{
    public Dog(String name) {
        super(name);
    }

    public void bark(){
        System.out.println(this.name + "在汪汪叫");
    }
}

class Cat extends  Animal{
    public Cat(String name) {
        super(name);
    }

    public void mimi(){
        System.out.println(this.name + "在喵喵叫");
    }
}
public class Main{
    public static void main(String[] args) {
        Animal animal = new Dog("旺财");
        Dog dog = (Dog) animal;
        dog.bark();

        if (animal instanceof Cat){
            Animal animal2 = new Dog("旺财");
            Cat cat = (Cat) animal;
            cat.mimi();
        }
    }
}