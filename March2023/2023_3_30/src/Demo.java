


/*interface Ishape{    //接口名推荐以I来开头
    public abstract void draw();    //public abstract这块是灰色，即接口中的方法默认是public abstract的

    //接口当中有不能实现的方法，两种例外
    public static void eat(){          //静态方法可以有具体的实现
        System.out.println("中午吃什么？");
    }

    default public void drink(){     //这个方法被 default 关键词修饰，表示这个方法默认就是这样的
        System.out.println("中午喝什么？");
    }



    int a = 1;
    public static int b = 1;     //public static 是灰色
    public static final int c = 1;   //public static final 是灰色，
                                    // 即接口当中的成员变量默认是public static final修饰的
}

public class Demo {
    public static void main(String[] args) {
        //Ishape shape = new Ishape();       会报错，接口不能通过关键词new来初始化
    }
}*/




interface IShape{
    void draw();
    public static void eat(){

    }
}


class Cycle implements IShape {    //类和接口之间，使用关键词implements进行关联
    //重写父类的方法
    //当一个类实现一个接口之后，这个类必须重写这个接口当中的抽象方法
    @Override
    public void draw() {
        System.out.println("○");
    }


    //当接口当中，存在default方法，可以选择重写，也可以选择不重写，具体看业务需求
    public void eat(){
        System.out.println("在重写这个default修饰的方法");
    }

}

class Rect implements IShape {
    @Override
    public void draw() {
        System.out.println("▭");
    }
}
public class Demo{

    //可以实现多态
    public static void drawMap(IShape iShape){
        iShape.draw();
    }
    public static void main(String[] args) {
        IShape iShape = new Cycle();    //接口虽然不能实例化自己，但可以实例化其子类，这个过程也是向上转型

        drawMap(new Cycle());
        drawMap(new Rect());
    }
}
