package Demo;


interface IFlying{
    void fly();
}

interface ISwimming{
    void swim();
}

interface IRunning{
    void run();
}

abstract class Animal{
    String name;
    int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract void eat();
}

class Dog extends Animal implements ISwimming, IRunning{   //先继承类，再关联接口，不能改变顺序
    public Dog(String name, int age) {          //这里接口的意义，不是所有的动物都会飞
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println(this.name + "正在吃狗粮");
    }

    @Override
    public void swim() {
        System.out.println(this.name + "正在河里游泳");
    }

    @Override
    public void run() {
        System.out.println(this.name + "正在遛人");
    }
}

class Bird extends Animal implements IFlying, IRunning{
    public Bird(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println(this.name + "正在吃鸟粮");
    }

    @Override
    public void fly() {
        System.out.println(this.name + "正在吃天空飞翔");

    }

    @Override
    public void run() {
        System.out.println(this.name + "正在吃地上蹦蹦跳跳");
    }
}

public class Main {

    public static void testAnimal(Animal animal){
        animal.eat();
    }

    public static void testRun(IRunning run){
        run.run();
    }
    public static void main(String[] args) {
        testAnimal(new Bird("小鸟",1));
        testRun(new Dog("小狗",2));
    }
}
