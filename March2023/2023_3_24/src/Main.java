import java.util.Arrays;
import java.util.Scanner;

public class Main {
    //数组的查找
 /*   public static int search(int[]arr, int k){
        boolean flag = false;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == k){
                flag = true;
                return i;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int[]arr = {1,2,3,4,5,6,7};

        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int index = search(arr,k);
        if(index != -1){
            System.out.println("在第" + (index + 1) + "个位置");
        }else{
            System.out.println("查无此数");
        }
    }*/

/*    public static String myTostring(int[] arr){
        if (arr == null){
            return null;
        }

        String ret = "{";
        for (int i = 0; i < arr.length; i++) {
            ret += arr[i];

            if(i != arr.length - 1){
                ret += ",";
            }
        }

        ret += "}";
        return ret;
    }

    public static void main(String[] args) {
        int[]arr = {1,2,3,4,5,6,7};
        System.out.println(myTostring(arr));
    }*/

/*    public static int search(int[]arr, int k){
        int len = 0;
        int right = arr.length - 1;

        while (len <= right){
            int middle = (len + right) / 2;

            if (arr[middle] < k){
                len = middle + 1;
            }else if(arr[middle] > k) {
                right = middle - 1;
            }else{
                return middle;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int[]arr = {1,2,3,4,5,6,7};

        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int index = search(arr,k);
        if(index != -1){
            System.out.println("在第" + (index + 1)+ "个位置");
        }else{
            System.out.println("查无此数");
        }
    }*/


/*    public static void main(String[] args) {
        int[]arr = {4,6,2,7,29,56,32,1};
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.binarySearch(arr, 29));
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.binarySearch(arr, 29));
    }*/


/*    public static void main(String[] args) {
        int[]arr = {4,6,2,7,29,56,32,1};
        bubberSelect(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static int[] bubberSelect(int[]arr){
        for (int i = 0; i < arr.length - 1; i++) {
            boolean flag = false;
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]){
                    int tmp = 0;
                    tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;

                    flag = true;
                }
            }

            if (flag == false){
                return arr;
            }
        }

        return arr;
    }*/


/*    public static void main(String[] args) {
        int[]arr = {4,6,2,7,29,56,32,1};
        myCopyOf(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static int[] myCopyOf(int[] arr){
        int [] arr2 = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            arr2[i] = arr[i];
        }
        return arr2;
    }*/


/*    public static void main(String[] args) {
        int[]arr = {4,6,2,7,29,56,32,1};
        System.out.println(Arrays.toString(arr));
        int[]ret = (Arrays.copyOf(arr, arr.length));
        System.out.println(Arrays.toString(ret));
    }*/


    //copyOfrange           arraycopy
/*    public static void main(String[] args) {
        int[]arr = {4,6,2,7,29,56,32,1};
        int[] ret = Arrays.copyOfRange(arr, 6, 8);  //[6, 8)
        System.out.println(Arrays.toString(ret));
    }*/

/*    public static void main(String[] args) {
        int[] arr = {4,6,2,7,29,56,32,1};
        int[] ret = new int[arr.length];
        System.arraycopy(arr,2, ret, 0,5);
        System.out.println(Arrays.toString(ret));
    }*/


    //数组逆序
/*    public static void reverseArray(int[] array) {
        int i = 0;
        int j = array.length-1;
        while (i < j) {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
            i++;
            j--;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
        reverseArray((arr));
        System.out.println(Arrays.toString(arr));
    }*/


/*public static void main(String[] args) {
    int[][] arr = {{1,2,3},{4,5,6}};
    int[][] arr2 = new int[2][3];
    int[][] arr3 = new int[][]{{1,2,3},{4,5,6}};

    for (int i = 0; i < arr.length; i++) {
        for (int j = 0; j < arr[0].length; j++) {
            System.out.print(arr[i][j] + " ");
        }
        System.out.println();
    }

    for (int[]tmp:arr) {
        for (int x:tmp ) {
            System.out.print(x + " ");
        }
        System.out.println();
    }

    System.out.println(Arrays.deepToString(arr));
}*/


/*    public static void main(String[] args) {
        int[][] arr = new int[2][];
        arr[0] = new int[5];
        arr[1] = new int[]{1,2,3};

        //arr[1] = {1,2,3};
    }*/


    //创建一个 int 类型的数组, 元素个数为 100, 并把每个元素依次设置为 1 - 100
/*    public static void main(String[] args) {
        int[]arr = new int[100];

        for (int i = 0; i < 100; i++) {
            arr[i] = i + 1;
        }

        System.out.println(Arrays.toString(arr));
    }*/

    //实现一个方法 printArray, 以数组为参数, 循环访问数组中的每个元素, 打印每个元素的值.
/*    public static  void printArray(int[] arr){
        for (int x:arr) {
            System.out.print(x + " ");
        }
    }
    
    public static void main(String[] args) {
        int [] arr = {1,2,3,4,5,6};
        printArray(arr);
    }*/


    //实现一个方法 transform, 以数组为参数, 循环将数组中的每个元素 乘以 2 ,
    // 并设置到对应的数组元素上. 例如 原数组为 {1, 2, 3}, 修改之后为 {2, 4, 6}
/*    public static int[] transform(int[] arr){
        for (int i = 0; i < arr.length; i++) {
            arr[i] *= 2;
        }
        return arr;
    }

    public static void main(String[] args) {
        int [] arr = {1,2,3,4,5,6};
        int[]ret = transform(arr);
        System.out.println(Arrays.toString(ret));
    }*/

    //实现一个方法 sum, 以数组为参数, 求数组所有元素之和.
/*    public static int sum(int[]arr){
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }

        return sum;
    }

    public static void main(String[] args) {
        int [] arr = {1,2,3,4,5,6};
        System.out.println(sum(arr));
    }*/

    //实现一个方法 avg, 以数组为参数, 求数组中所有元素的平均值(注意方法的返回值类型).
/*    public static double avg(int[]arr){
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }

        return sum / arr.length;
    }

    public static void main(String[] args) {
        int [] arr = {1,2,3,4,5,6};
        System.out.println(avg(arr));
    }*/

    public static void main(String[] args) {
        int[] arr = new int[10];
        System.out.println("最初值：" + Arrays.toString(arr));
        Arrays.fill(arr,1);
        System.out.println("全部初始化：" + Arrays.toString(arr));
        Arrays.fill(arr,0,5,4);    //[0, 5)
        System.out.println("局部初始化：" + Arrays.toString(arr));
    }
}
