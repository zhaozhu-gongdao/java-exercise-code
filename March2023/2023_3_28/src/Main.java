import java.util.Scanner;
/*

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            Data data = new Data(x, y);
            System.out.println(data.getX() + data.getY());
        }
    }

}

class Data {

    private int x;
    private int y;

    public Data(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}*/



/*class Stu{
    private String name;
    public int age = 20;


    public Stu(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Stu{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

public class Main{
    public static void main(String[] args) {
        Stu stu = new Stu("aaaa",19);
        System.out.println(stu);
    }
}*/

/*
class Aminal{
    public String name = "父类成员";
    public int age = 19;

    public void eat(){
        System.out.println(this.name + "正在吃饭");
    }
}

class Dog extends Aminal{
    private String name = "子类成员";
    private String color;
    public int age = 20;

    public void bark(){
       */
/* System.out.println(this.name + " 正在汪汪叫");*//*

        System.out.println("调用的是： " + name);
        System.out.println("调用的是： " + super.name);

    }
}

class Cat extends Aminal{
    public void bark(){
        System.out.println(this.name + " 正在喵喵叫");
    }
}
public class Main{
    public static void main(String[] args) {
        Dog dog = new Dog();
*/
/*        Cat cat = new Cat();*//*

        dog.bark();
*/
/*        dog.name = "旺财";
        cat.name = "咪咪";
        dog.bark();
        cat.bark();*//*

    }
}*/

class Father{
    public String name;
    public int age;

    public void methodA(){
        System.out.println("调用的是父类中的methodA");
    }

    public void methodB(int a){
        System.out.println("调用的是父类中的methodB");
    }
}

class Son extends Father{
    public void methodA(){
        System.out.println("调用的是子类中的methodA");
    }

    public void methodB(){
        System.out.println("调用的是子类中的methodB");
    }

    public void methodC(){
        super.methodA();
    }
}

public class Main {
    public static void main(String[] args) {
        Son son = new Son();
        son.methodB(5);
        son.methodB();
        son.methodA();
        son.methodC();
    }
}



/*
class Father{
    public String name = "父类方法";
    public int age;
}

class Son extends Father{
    public String name = "子类方法";
    public void methodA(){
        System.out.println("调用的是：" + name);
        System.out.println("调用的是：" + super.name);
    }

}

public class Main {
    public static void main(String[] args) {
        Son son = new Son();
        son.methodA();
    }
}*/
