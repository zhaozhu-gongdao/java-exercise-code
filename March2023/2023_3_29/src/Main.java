/*import java.util.Scanner;

class Father{
    public String name;
    public int age;

    public Father(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

class Son extends Father{
    public Son() {
        super("aaa", 19);
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
    }
}*/



/*class Father{
    public String name;
    public int age;

    static{
        System.out.println("这是Father的静态代码块");
    }

    {
        System.out.println("这是Father的构造代码块");
    }

    public Father() {
        System.out.println("这是Father的构造方法");
    }

}

class Son extends Father{
    static{
        System.out.println("这是Son的静态代码块");
    }

    {
        System.out.println("这是Father的构造代码块");
    }

    public Son(){
        System.out.println("这是Son的构造方法");
    }
}

public class Main{
    public static void main(String[] args) {
        Son son1 = new Son();
        System.out.println("===============");
        Son son2 = new Son();
    }
}*/

/*final class Father{

}
*//*class Son extends Father{     无法继承，会报错

}*//*
public class Main{
    public static void main(String[] args) {
        final int a = 0;
        //a = 100;      //会报错
    }
}*/


/*public class Main{
    public static void main(String[] args) {
        System.out.println("  A");
        System.out.println("A   A");
        System.out.println("  A");
    }
}*/


/*
import java.util.Scanner;

public class Main{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        for (int i = 0; i < 3; i++) {
            int num = scanner.nextInt();
            sum += num;
        }
        System.out.println("sum=" + sum);
    }
}*/

/*import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            int z = scanner.nextInt();
            Sub sub = new Sub(x, y, z);
            System.out.println(sub.calculate());
        }
    }

}

class Base {

    private int x;
    private int y;

    public Base(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}

class Sub extends Base {

    private int z;

    public Sub(int x, int y, int z) {
        super(x,y);
        this.z = z;
    }

    public int getZ() {
        return z;
    }

    public int calculate() {
        return super.getX() * super.getY() * this.getZ();
    }

}*/


import javax.swing.*;
/*import java.util.Scanner;

public class Main{
    public static int[] money(int money){
        int hundred = money / 100;
        money %= 100;
        int fifty = money / 50;
        money %= 50;
        int twenty = money / 20;
        money %= 20;
        int ten = money / 10;
        money %= 10;
        int five = money / 5;
        money %= 5;
        int two = money / 2;
        money %= 2;
        int one = money / 1;
        money %= 1;

        return new int[]{hundred, fifty, twenty, ten, five, two, one};
    }

    public static void print(int[]arr){
        System.out.format("100元:%3d张\n", arr[0]);
        System.out.format(" 50元:%3d张\n", arr[1]);
        System.out.format(" 20元:%3d张\n", arr[2]);
        System.out.format(" 10元:%3d张\n", arr[3]);
        System.out.format("  5元:%3d张\n", arr[4]);
        System.out.format("  2元:%3d张\n", arr[5]);
        System.out.format("  1元:%3d张\n", arr[6]);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int money = scanner.nextInt();

        int[] arr = money(money);

        print(arr);
    }
}*/


class Father{

    public  void eat(){
        System.out.println("正在吃饭");
    }
}

class Son extends Father{

    @Override
    public  void eat(){
        System.out.println("正在吃黄焖鸡米饭");
    }
}
public class Main {
    public static void main(String[] args) {
        Son son = new Son();
        son.eat();
    }
}
