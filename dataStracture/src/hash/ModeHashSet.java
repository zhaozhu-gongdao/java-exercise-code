package hash;

public class ModeHashSet {
    //Node相当于Entry
    static class Node {
        private int key;
        private int value;
        private Node next;

        public Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    public Node[] array;
    public int usedSize;

    //默认的负载因子为0.75
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    public ModeHashSet() {
        this.array = new Node[10];
    }
}

