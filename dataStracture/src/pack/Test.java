package pack;

public class Test {
    public static void main1(String[] args) {
        int a = 10;
        Integer i = a; //自动装箱
        Integer b = Integer.valueOf(a);
        Integer d = new Integer(a); //显示装箱
    }

    public static void main(String[] args) {
        Integer in = 10;
        int e = in;

        int i = in.intValue();
        float f = in.floatValue();

        Integer a = 127;
        Integer b = 127;

        Integer c = 128;
        Integer d = 128;
    }
}
