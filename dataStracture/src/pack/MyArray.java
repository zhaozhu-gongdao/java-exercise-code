package pack;
//private Object[] array = new Object[10];
public class MyArray<T> {
    private T[] array = (T[]) new Object[10];

    public T getVal(int pos) {
        return array[pos];
    }

    public void setVal(int pos, T object){
        array[pos] = object;
    }

    public T[] objects = (T[])new Object[10];

    public T[] getArray() {
        return objects;
    }
}

class Test2{

    public static void main1(String[] args) {
        MyArray array = new MyArray();
        array.setVal(0, "123");
        array.setVal(1, 123);
        int secondArg = (int)array.getVal(1);
    }

    public static void main2(String[] args) {
        MyArray<Integer> array = new MyArray<>();
        array.setVal(0,1);
        System.out.println(array.getVal(0));
    }

    public static void main3(String[] args) {
        MyArray<String> array = new MyArray<>();
        array.setVal(0,"aaa");
        System.out.println(array.getVal(0));
    }

    public static void main(String[] args) {
        MyArray array = new MyArray<>();
        array.setVal(0,"aaa");
        array.setVal(1,123);
        int val = (int)array.getVal(1);
    }
}


