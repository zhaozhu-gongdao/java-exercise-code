package queue;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

//循环队列
public class MyCircularQueue {
    private int[] elem;
    private int head;//队头下标
    private int last;//队尾下标

    public MyCircularQueue(int k) {
        this.elem = new int[k+1];
    }

    //入队
    public boolean enQueue(int value) {
        if(isFull()) {
            return false;
        }
        elem[last] = value;
        last = (last+1) % elem.length;
        return true;
    }

    //出队
    public boolean deQueue() {
        if(isEmpty()) {
            return false;
        }
        head = (head+1) % elem.length;
        return true;
    }

    //得到队头元素
    public int Front() {
        if(isEmpty()) {
            return -1;
        }
        return elem[head];
    }
    //得到队尾元素
    public int Rear() {
        if(isEmpty()) {
            return -1;
        }
        int index = (last == 0) ? elem.length-1 : last-1;
        return elem[index];
    }

    public boolean isEmpty() {
        return last == head;
    }

    public boolean isFull() {
        return (last+1) % elem.length == head;
    }

    public static void main(String[] args) {
        Deque<Integer> deque = new LinkedList<>();
        deque.offer(1);

        Deque<Integer> stack = new LinkedList<>();
        stack.push(2);

        Deque<Integer> stack2 = new ArrayDeque<>();
    }
}
