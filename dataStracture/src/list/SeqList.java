package list;

import java.util.Arrays;

public class SeqList {
    private int[] elem;
    private int usedSize;
    private static final int DEFAULT_CAPACITY = 5;

    public SeqList() {
        this.elem = new int[DEFAULT_CAPACITY];
    }

    // 打印顺序表，注意：该方法并不是顺序表中的方法，而是为了方便看测试结果给出的
    public void display() {
        for (int i = 0; i < this.usedSize; i++) {
            System.out.print(this.elem[i] +" ");
        }
        System.out.println();
    }

    // 新增元素,默认在数据 最后新增
    public void add(int data) {
        //首先得判断满的情况
        if(isFull()) {
            resize();
        }
        this.elem[usedSize] = data;
        usedSize++;
    }

    public boolean isFull() {
        return usedSize == elem.length;
    }

    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if(elem[i] == toFind) {
                return true;
            }
        }
        return false;
    }

    // 查找某个元素对应的位置 下标
    public int indexOf(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if(elem[i] == toFind) {
                return i;
            }
        }
        return -1;
    }

    // 获取 pos 位置的元素
    public int get(int pos) {
        if(!checkPos(pos)) {
            //这个是自定义异常
            throw new PosOutBoundsException("get 获取数据时，位置不合法！");
        }
        return elem[pos];
    }

    // 获取顺序表长度
    public int size() {
        return this.usedSize;
    }

    // 给 pos 位置的元素设为 value【更新的意思 】
    public void set(int pos, int value) {
        if(!checkPos(pos)) {
            //这个是自定义异常
            throw new PosOutBoundsException("set 数据时，位置不合法！");
        }
        this.elem[pos] = value;
    }

    private boolean checkPos(int pos) {
        if(pos < 0 || pos >= usedSize) {
            return false;
        }
        return true;
    }

    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        if(pos < 0 || pos > this.usedSize) {
            //这个是自定义异常
            throw new PosOutBoundsException("add 元素的时候，pos位置不合法！");
        }
        if(isFull()) {
            resize();
        }
        //挪数据
        for (int i = this.usedSize-1; i >= pos ; i--) {
            this.elem[i+1] = this.elem[i];
        }
        //存数据
        this.elem[pos] = data;
        this.usedSize++;
    }

    private void resize() {
        elem = Arrays.copyOf(elem,2*elem.length);
    }

    //删除第一次出现的关键字key
    public void remove(int toRemove) {
        if(isEmpty()) {
            return;
        }
        int index = indexOf(toRemove);
        if(index == -1) {
            return;//没有你要删除的数字
        }
        for (int i = index; i < usedSize-1; i++) {
            this.elem[i] = this.elem[i+1];
        }
        usedSize--;
        //elem[index] = null;
    }

    public boolean isEmpty() {
        return usedSize == 0;
    }

    // 清空顺序表
    public void clear() {
        usedSize = 0;
    }

}
