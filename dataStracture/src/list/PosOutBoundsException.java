package list;

public class PosOutBoundsException extends RuntimeException{
    public PosOutBoundsException() {
    }

    public PosOutBoundsException(String message) {
        super(message);
    }
}
