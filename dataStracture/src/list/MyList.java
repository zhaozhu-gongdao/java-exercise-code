package list;

import java.util.*;

public class MyList {
    public static void main(String[] args) {
//        List<Integer> stack = new Stack<>();
//        List<Integer> arrayList = new ArrayList<>();
//        List<Integer> linkedList = new LinkedList<>();
//
//        ArrayList<Integer> arrayList1 = new ArrayList<>();
//        List<Integer> arrayList2 = new ArrayList<>();

//        ArrayList<String> arrayList = new ArrayList<>();
//        arrayList.add("aaa");
//        arrayList.add("bbb");
//        arrayList.add("ccc");
//        arrayList.subList(1,3);
//        List<String> list = new ArrayList<>(arrayList);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("aaa");
        System.out.println(arrayList);

        System.out.println("=====");

        for (int i = 0; i < arrayList.size(); i++) {
            System.out.print(arrayList.get(i)+" ");
        }
        System.out.println();

        System.out.println("=====");
        for(String x : arrayList) {
            System.out.print(x+" ");
        }
        System.out.println();

        System.out.println("=====");

        Iterator<String> it = arrayList.listIterator();
        while (it.hasNext()) {
            System.out.print(it.next()+" ");
        }
        System.out.println();
    }
}
