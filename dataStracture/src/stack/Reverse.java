package stack;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

public class Reverse {
    ListNode head = null;
    static class ListNode{
        int value;
        ListNode next;

        public ListNode(int value) {
            this.value = value;
        }
    }
    //通过递归方式打印链表
    public void reverseByRecurision(ListNode head){
        if (head == null){
            return;
        }

        if (head.next == null) {
            System.out.println(head.value);
            return;
        }
        reverseByRecurision(head.next);
        System.out.println(head.value);
    }

    public void reverseByStack(ListNode head){
        Stack<ListNode> stack = new Stack<>();

        ListNode cur = head;
        while (cur != null){
            stack.push(cur);
            cur = cur.next;
        }

        while (!stack.empty()){
            ListNode tmp = stack.pop();
            System.out.println(tmp.value);
        }
    }

    public ListNode initList(){
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);
        ListNode node6 = new ListNode(6);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;
        return node1;
    }
    public static void main1(String[] args) {
        Reverse test = new Reverse();
//        test.reverseByRecurision(test.initList());
        test.reverseByStack(test.initList());
    }

    public static void main(String[] args) {
        Deque<Integer> stack = new LinkedList<>();
        stack.push(10);
    }
}
