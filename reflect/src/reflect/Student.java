package reflect;

public class Student {
    public String name = "张三";
    private Integer age = 18;
    public Student() {
    }

    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    private void function(String str){
        System.out.println(str);
    }
}
