package lambda;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class TestLambda {
    public static void main1(String[] args) {
        Test t = new Test() {
            @Override
            public void test() {
                System.out.println("使用匿名内部类的方式");
            }
        };

        t.test();
    }

    public static void main2(String[] args) {
        Test t = () -> System.out.println("使用lambda表达式");
        t.test();
    }

    public static void main3(String[] args) {
        int arg = 10;
        //arg = 100;
        Test t = new Test() {
            @Override
            public void test() {
                System.out.println("捕获到的变量arg值为: " + arg);
            }
        };

        t.test();
    }

    public static void main4(String[] args) {
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>((x,y)->{return x.compareTo(y);});

        PriorityQueue<Integer> maxHeap2 = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
    }

    public static void main5(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");

        list.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.print(s + " ");
            }
        });

        list.forEach(s -> System.out.print(s + " "));
    }

    public static void main6(String[] args) {
        HashMap<String,Integer> map = new HashMap<>();
        map.put("a",1);
        map.put("b",2);
        map.put("c",3);

        map.forEach(new BiConsumer<String, Integer>() {
            @Override
            public void accept(String s, Integer integer) {
                System.out.println("key "+ s +" value: "+integer);
            }
        });

        map.forEach((k,v) -> System.out.println("key: "+ k +" value: "+v));
    }

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("c");
        list.add("b");
        list.add("a");

        list.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });

        list.sort(((o1, o2) -> o1.compareTo(o2)));
    }
    //list.forEach(x -> System.out.print(x + " "));
}
