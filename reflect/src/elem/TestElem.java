package elem;

public enum TestElem {
    RED(0, "红色"), GREEN(0, "绿色"),
    BLUE(0, "蓝色"), WHITE(0, "白色");

    public int ordinal;
    public String color;
    TestElem(int ordinal, String color) {
        this.ordinal = ordinal;
        this.color = color;
    }

    public static void main(String[] args) {
        TestElem[] testElems = TestElem.values();
        for (int i = 0; i < testElems.length; i++){
            System.out.println(testElems[i] + ", " + testElems[i].ordinal());
        }

        System.out.println("=======");

        TestElem elem = TestElem.valueOf("BLUE");
        System.out.println(elem);

        System.out.println("=======");
        System.out.println(GREEN.compareTo(WHITE));
    }
}
