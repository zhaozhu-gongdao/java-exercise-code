package combine;


import elem.TestElem;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Test {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<?> c1 = Class.forName("elem.TestElem");
        Constructor<?> constructor = c1.getDeclaredConstructor(int.class, String.class);
        constructor.setAccessible(true);
        TestElem elem = (TestElem) constructor.newInstance("青色", 0, 0, "青色");
        System.out.println(elem);
    }
}
