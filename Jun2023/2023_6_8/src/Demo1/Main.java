package Demo1;

import java.util.*;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String[] types = {"Student","Teacher"};
        int seed=sc.nextInt();
        Random r = new Random(seed);//输入种子seed，并用其初始化Random对象
        int n=sc.nextInt();//输入对象生成数量n
        Person[] p = new Person[n];
        String[] strr = new String[n];
        for(int i=0;i<n;i++)//以下是生成n个对象的循环
        {
            String str =types[r.nextInt(2)];
            //使用random.nextInt(2)从types数组中随机选取元素x并输出
            strr[i]=str;///
            //根据x选择输入相应的参数
            if(str.equals("Student"))
            {
                String name=sc.next();
                int year=sc.nextInt();
                int month=sc.nextInt();
                int dayOfMonth=sc.nextInt();
                int enrollmentYear=sc.nextInt();
                String clazzName=sc.next();
                Student s = new Student(i,name,year,month,dayOfMonth,enrollmentYear,clazzName);
                p[i]=s;
            }
            else if(str.equals("Teacher"))
            {
                String name=sc.next();
                int year=sc.nextInt();
                int month=sc.nextInt();
                int dayOfMonth=sc.nextInt();
                String department=sc.next();
                double salary=sc.nextDouble();
                Teacher t = new Teacher(i,name,year,month,dayOfMonth,department,salary);
                p[i]=t;
            }

        }
        for(String f:strr)
        {
            System.out.println(f);
        }

        for(Person x:p)
        {
            System.out.println(x.getPersonNumber());
        }
        //以下几行输出验证信息，请直接复制粘贴到你的代码中
        System.out.println(Student.class.getSuperclass());
        System.out.println(Teacher.class.getSuperclass());
        try {
            System.out.println(Person.class.getDeclaredMethod("getPersonNumber", null));
            System.out.println(Student.class.getDeclaredMethod("getPersonNumber", null));
            System.out.println(Teacher.class.getDeclaredMethod("getPersonNumber", null));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
abstract class Person
{
    int id;
    String name;
    LocalDate birthDate;
    int year;
    int month;
    int dayOfMonth;

    public Person(int id,String name,int year,int month,int dayOfMonth)
    {
        this.id=id;
        this.name=name;
        this.year=year;
        this.month=month;
        this.dayOfMonth=dayOfMonth;
        this.birthDate=LocalDate.of(this.year,this.month,this.dayOfMonth);

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }


    public String toString() {
        return "Person [id=" + id + ", name=" + name + ", birthDate=" + birthDate +  "]";
    }
    //抽象方法
    public abstract String getPersonNumber();

}

class Student extends Person
{
    int enrollmentYear;
    String clazzName;
    public Student(int id,String name,int year,int month,int dayOfMonth,int enrollmentYear,String clazzName)
    {
        super(id,name,year,month,dayOfMonth);//使用super复用Person的构造函数
        this.enrollmentYear=enrollmentYear;
        this.clazzName=clazzName;

    }
    public String getPersonNumber() {
        return "Student-"+this.enrollmentYear+"-"+this.name;
    }

}
class Teacher extends Person
{
    String department;
    double salary;

    public Teacher(int id,String name,int year,int month,int dayOfMonth,String department,double salary)
    {
        super(id,name,year,month,dayOfMonth);
        this.department=department;
        this.salary=salary;
    }
    public String getPersonNumber() {

        return "Teacher-"+this.department+"-"+this.name;
    }
}

