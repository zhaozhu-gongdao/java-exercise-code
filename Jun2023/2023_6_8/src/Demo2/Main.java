package Demo2;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner =  new Scanner(System.in);
        String str = scanner.nextLine();
        String[] newStr = str.split("-",3);
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();

        for (int i = 0; i < Integer.parseInt(newStr[0]); i++) {
            String checkStr = scanner.nextLine();

            String[] checkStrArray = checkStr.split("-", 4);
            if (checkStrArray[0].equals(newStr[1]) && checkStrArray[1].compareToIgnoreCase(newStr[2]) == 0){
                list2.add(checkStr);
            }

            if (checkStrArray[0].equals(newStr[1])){
                list1.add(checkStr);
            }
        }

        System.out.println(list1);
        System.out.println(list2);

        for (int i = 0; i < list2.size() - 1; i++) {
            CompareScore compareScore = new CompareScore();
            for (int j = i + 1; j < list2.size(); j++) {
                String[] o1 = list2.get(i).split("-", 4);
                String[] o2 = list2.get(j).split("-", 4);

                if (compareScore.compare(o1,o2) < 0 ){
                    String tmp = list2.get(i);
                    list2.set(i,list2.get(j));
                    list2.set(j, tmp);
                }
            }
        }
        System.out.println(list2);
    }
}

class CompareScore implements Comparator<String[]>{
    @Override
    public int compare(String[] o1, String[] o2) {
        int oneScore = Integer.parseInt(o1[2]);
        int twoScore = Integer.parseInt(o2[2]);
        return oneScore - twoScore;
    }
}
