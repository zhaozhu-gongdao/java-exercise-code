package cn.itcast.service;

import cn.itcast.domian.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    void addUser(User user);

    User findUserById(String id);

    void updateUser(User user);

    void deleteUser(String id);

    void delSelectedUser(String[] uids);

    User login(User user);
}
