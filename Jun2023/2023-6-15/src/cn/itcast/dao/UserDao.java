package cn.itcast.dao;

import cn.itcast.domian.User;

import java.util.List;

public interface UserDao {
    List<User> findAll();

    void add(User user);

    User findById(int parseInt);

    void update(User user);

    void delete(int parseInt);

    User findUserByUsernameAndPassword(String username, String password);
}
