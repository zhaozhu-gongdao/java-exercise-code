package Demo1;

import java.util.Arrays;

public class PriorityQueue {
    public int[] elem;
    public int usedSize;

    public PriorityQueue() {
        this.elem = new int[10];
    }

    /**
     * 建堆的时间复杂度：
     *
     * @param array
     */
    public void createHeap(int[] array) {
        for (int i = 0; i < array.length; i++) {
            elem[i] = array[i];
            usedSize++;
        }

        for (int parent = (usedSize-1-1)/2; parent >= 0 ; parent--) {
            shiftDown(parent,usedSize);
        }
    }

    /**
     *
     * @param parent 是每棵子树的根节点的下标
     * @param len  是每棵子树调整结束的结束条件
     * 向下调整的时间复杂度：O(logn)
     */
    private void shiftDown(int parent,int len) {
        int child = 2 * parent+1;

        while (child < len) {
            if(child + 1 < len && elem[child] < elem[child+1]) {
                child = child + 1;
            }
            if(elem[child] > elem[parent]) {
                int tmp = elem[child];
                elem[child] = elem[parent];
                elem[parent] = tmp;
                parent = child;
                child = 2 * parent+1;
            }else {
                break;
            }
        }
    }


    /**
     * 入队：仍然要保持是大根堆
     * @param val
     */
    public void push(int val) {
        if (isFull()){
            elem = Arrays.copyOf(elem, elem.length * 2);
        }

        elem[usedSize] = val;
        usedSize++;
        shiftUp(usedSize - 1);
    }

    private void shiftUp(int child) {
        int parent = (child - 1) / 2;

        while (child > 0){
            if (elem[child] > elem[parent]){
                int tmp = elem[child];
                elem[child] = elem[parent];
                elem[parent] = tmp;
                child = parent;
                parent = (child - 1) / 2;
            }else{
                break;
            }
        }
    }

    public boolean isFull() {
        if (usedSize == elem.length){
            return true;
        }
        return false;
    }

    /**
     * 出队【删除】：每次删除的都是优先级高的元素
     * 仍然要保持是大根堆
     */
    public void pollHeap() {
        if (isEmpty()){
            return;
        }

        int tmp = elem[0];
        elem[0] = elem[usedSize - 1];
        elem[usedSize- 1] = tmp;
        usedSize--;
        shiftDown(0,usedSize);
    }

    public boolean isEmpty() {
        if (usedSize == 0){
            return true;
        }
        return false;
    }

    /**
     * 获取堆顶元素
     * @return
     */
    public int peekHeap() {
        if (isEmpty()){
            return -1;
        }

        return elem[0];
    }
}


class Main2{
    public static void main(String[] args) {
        PriorityQueue test2 = new PriorityQueue();
        test2.createHeap(new int[]{1,3,5,7,2,4,6,8});
        System.out.println(test2.peekHeap());
        test2.pollHeap();
        System.out.println(test2.peekHeap());

    }
}