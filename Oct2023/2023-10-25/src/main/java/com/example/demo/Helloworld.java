package com.example.demo;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.net.CookieStore;
import java.util.List;

@RestController
public class Helloworld {
    @RequestMapping(value = "sayHello", method = RequestMethod.GET)
    public String text() {
        return "hello world";
    }

    @RequestMapping(value = "demo1", method = RequestMethod.PUT)
    public String demo1(String name) {
        return "接收到的name:" + name;
    }

    @RequestMapping("demo2")
    public String demo2(String name, Integer age) {
        return "接受到的name:" + name + "，age:" + age;
    }

    @RequestMapping("demo3")
    public String demo3(Person person) {
        return person.toString();
    }

    @RequestMapping("demo4")
    public String demo4(@RequestParam(value = "name", required = false) String username) {
        return "接收到的name:" + username;
    }

    @RequestMapping("/demo5")
    public String demo5(String[] str) {
        return "接收到的数组：" + str.toString() + ",长度是：" + str.length;
    }

    @RequestMapping("/demo6")
    public String m7(@RequestParam(required = false, defaultValue = "zhangsan,lisi,wangwu,zhaoliu") List<String> listParam) {
        return "接收到的参数listParam:" + listParam + ",长度:" + listParam.size();
    }

    @RequestMapping("/demo7")
    public String demo7(Person person) {
        System.out.println("11111111");
        return person.toString();
    }

    @RequestMapping("/demo8/{name}/{wendiage}")
    public String demo8(@PathVariable String name, @PathVariable("wendiage") Integer age) {
        return "解析的name:" + name + ", age:" + age;
    }

    @RequestMapping("/demo9")
    public String demo9(@RequestPart MultipartFile file) throws IOException {
        System.out.println(file.getOriginalFilename());
        file.transferTo(new File("D:/else/" + file.getOriginalFilename()));
        return "success";
    }

    @RequestMapping("/demo10")
    public String demo10(HttpServletRequest request, HttpServletResponse response){
        Cookie[] cookies = request.getCookies();

        StringBuilder builder = new StringBuilder();

        if (cookies != null){
            for (Cookie ck : cookies){
                builder.append(ck.getName() + ":" + ck.getValue());
            }
        }

        return "Cookie信息：" + builder;
    }

    @RequestMapping("/demo11")
    public String demo11(HttpServletRequest request){
        HttpSession session = request.getSession();

        if (session != null){
            session.setAttribute("username","zhangsan");
        }

        return "success";
    }


    @RequestMapping("/getSess")
    public String sess(HttpServletRequest request) {
        // 如果 session 不存在, 不会⾃动创建
        HttpSession session = request.getSession(false);
        String username = null;
        if (session != null && session.getAttribute("username") != null) {
            username = (String) session.getAttribute("username");
        }
        return "username：" + username;
    }
}
