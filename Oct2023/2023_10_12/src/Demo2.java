import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Demo2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int year = 0;
        int month = 0;
        int day = 0;

        int monthDay[] = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30,31};
        while (in.hasNext()){
            year = in.nextInt();
            month = in.nextInt();
            day = in.nextInt();

            int dayNum = 0;
            for(int i = 0; i < month; i++){
                dayNum += monthDay[i];
            }

            dayNum += day;

            //判断是否是闰年
            if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0){
                if (month > 2){
                    dayNum++;
                }
            }

            System.out.println(dayNum);
        }
    }
}