//import java.util.Scanner;
//
//public class Demo1 {
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        int n = scanner.nextInt();
//
//        scanner.nextLine();
//
//        String[] arr = new String[n];
//
//        //输入字符串
//        for (int i = 0; i < n; i++) {
//            arr[i] = scanner.nextLine();
//        }
//
//        boolean isLength = false;
//        boolean isNum = false;
//
//        if (isLengths(arr)){
//            isLength = true;
//        }
//
//        if (isNums(arr)){
//            isNum = true;
//        }
//
//        if (isLength == true && isNum == true){
//            System.out.println("both");
//        }else if (isLength == false &&isNum != true){
//            System.out.println("lengths");
//        }else{
//            System.out.println("none");
//        }
//    }
//
//    //判断是否长度排序
//    public static boolean isLengths(String[] arr){
//        for (int i = 0; i < arr.length - 1; i++) {
//            if (arr[i].length() > arr[i + 1].length()){
//                return false;
//            }
//        }
//        return true;
//    }
//
//
//    //判断字典序
//    public static boolean isNums(String[] arr){
//        int k = 0;
//        for (int i = 0; i < arr.length - 1; i++) {
//            if (arr[i].compareTo(arr[i + 1]) > 0){
//                return true;
//            }
//        }
//
//        return false;
//    }
//}


import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Demo1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int a = in.nextInt();
            int b = in.nextInt();

            System.out.println(test(a,b));
        }
    }

    private static int test(int a, int b) {
        int m = a;
        int n = b;
        int t = m % n;

        while (t != 0){
            m = n;
            n = t;
            t = m % n;
        }

        return a * b / n;
    }
}
