package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Helloworld {
    @RequestMapping(value = "sayHello",method = RequestMethod.GET)
    public String text(){
        return "hello";
    }

    @RequestMapping(value = "demo1",method = RequestMethod.PUT)
    public String demo1(String name){
        return "接收到的name:" + name;
    }

    @RequestMapping("demo2")
    public String demo2(String name, Integer age){
        return "接受到的name:" + name + "，age:" + age;
    }

    @RequestMapping("demo3")
    public String demo3(Person person){
        return person.toString();
    }

    @RequestMapping("demo4")
    public String demo4(@RequestParam(value = "name",required = false) String username){
        return "接收到的name:" + username;
    }
}
